/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include "parse.h"

#include "graph.h"

void mate__export_graph_as_html_canvas(struct mate__Graph *graph,
									   const char *file_path) {

	const double VERTEX_RADIUS = 12.0;
	const int VERTEX_LABEL_FONT_SIZE = 10;
	const int EDGE_LABEL_FONT_SIZE = 10;
	const int META_LABEL_FONT_SIZE = 12;
	const int META_LABEL_Y_OFFSET = 15;

	FILE *f = fopen(file_path, "w");
	fprintf(f, "<!DOCTYPE html>\n");
	fprintf(f, "<html>\n");
	fprintf(f, "\t<body>\n");
	fprintf(f, "\t\t<canvas id=\"canvas\" width=\"500\" height=\"500\" "
			   "style=\"border:1px solid\"></canvas>\n");
	fprintf(f, "\t<script>\n");
	fprintf(f, "\t\tvar c = document.getElementById(\"canvas\");\n");

	fprintf(f, "\t\tvar ctx = c.getContext(\"2d\");\n");
	fprintf(f, "\t\tctx.fillStyle = \"#00FF00\";\n");

	/* --- plot edges --- */
	struct mate__List_iterator it_e = mate__create_list_iterator(graph->e);
	struct mate__Graph_Edge *e;
	fprintf(f, "\t\tctx.font = \"%dpx Arial\";\n", VERTEX_LABEL_FONT_SIZE);
	while ((e = mate__get_next_list_iterator_item(&it_e)) != NULL) {
		double x1 = e->v0->coord.x + 0.5;
		double y1 = e->v0->coord.y + 0.5;
		double x2 = e->v1->coord.x + 0.5;
		double y2 = e->v1->coord.y + 0.5;
		double cx = x1 + (x2 - x1) / 2.0;
		double cy = y1 + (y2 - y1) / 2.0;
		/* line */
		fprintf(f, "\t\t// edge '%s'\n", e->label);
		fprintf(f, "\t\tctx.beginPath();\n");
		fprintf(f, "\t\tctx.moveTo(%f,%f);\n", x1, y1);
		fprintf(f, "\t\tctx.lineTo(%f,%f);\n", x2, y2);
		fprintf(f, "\t\tctx.strokeStyle = \"#000000\";\n");
		fprintf(f, "\t\tctx.stroke();\n");
		/* label */
		fprintf(f, "\t\tctx.fillStyle = \"blue\";\n");
		fprintf(f, "\t\tctx.textAlign = \"center\";\n");
		fprintf(f, "\t\tctx.textBaseline=\"top\";\n");
		fprintf(f, "\t\tctx.fillText(\"%s\", %f, %f);\n", e->label, cx, cy);
	}

	/* --- plot vertices --- */
	struct mate__List_iterator it_v = mate__create_list_iterator(graph->v);
	struct mate__Graph_Vertex *v;
	fprintf(f, "\t\tctx.font = \"%dpx Arial\";\n", EDGE_LABEL_FONT_SIZE);
	while ((v = mate__get_next_list_iterator_item(&it_v)) != NULL) {
		double x = v->coord.x + 0.5;
		double y = v->coord.y + 0.5;
		fprintf(f, "\t\t// vertex '%s'\n", v->label);
		/* circle */
		fprintf(f, "\t\tctx.beginPath();\n");
		fprintf(f, "\t\tctx.arc(%f,%f,%f,0,6.283185);\n", x, y, VERTEX_RADIUS);
		fprintf(f, "\t\tctx.fillStyle = \"white\";\n");
		fprintf(f, "\t\tctx.fill();\n");
		fprintf(f, "\t\tctx.lineWidth = 1.0;\n");
		fprintf(f, "\t\tctx.strokeStyle = '#000000';\n");
		fprintf(f, "\t\tctx.stroke();\n");
		/* label */
		fprintf(f, "\t\tctx.fillStyle = \"blue\";\n");
		fprintf(f, "\t\tctx.textAlign = \"center\";\n");
		fprintf(f, "\t\tctx.textBaseline=\"middle\";\n");
		fprintf(f, "\t\tctx.fillText(\"%s\", %f, %f);\n", v->label, x, y);
	}

	/* --- plot metadata --- */
	const int n = mate__get_graph_order(graph);
	const int m = mate__get_graph_size(graph);
	const int k = mate__get_graph_component_number(graph);
	const bool is_tree = mate__is_tree(graph);
	const bool is_forest = mate__is_forest(graph);

	fprintf(f, "\t\tctx.font = \"%dpx Arial\";\n", META_LABEL_FONT_SIZE);
	fprintf(f, "\t\tctx.fillStyle = \"blue\";\n");
	fprintf(f, "\t\tctx.textAlign = \"left\";\n");
	fprintf(f, "\t\tctx.textBaseline=\"middle\";\n");
	double x = 5.5;
	double y = 400.5;
	fprintf(f, "\t\tctx.fillText(\"n=%d\", %f, %f);\n", n, x, y);
	y += META_LABEL_Y_OFFSET;
	fprintf(f, "\t\tctx.fillText(\"m=%d\", %f, %f);\n", m, x, y);
	y += META_LABEL_Y_OFFSET;
	fprintf(f, "\t\tctx.fillText(\"k=%d\", %f, %f);\n", k, x, y);
	y += META_LABEL_Y_OFFSET;
	fprintf(f, "\t\tctx.fillText(\"tree=%s\", %f, %f);\n",
			is_tree ? "yes" : "no", x, y);
	y += META_LABEL_Y_OFFSET;
	fprintf(f, "\t\tctx.fillText(\"forest=%s\", %f, %f);\n",
			is_forest ? "yes" : "no", x, y);
	y += META_LABEL_Y_OFFSET;

	fprintf(f, "\t</script>\n");
	fprintf(f, "\t</body>\n");
	fprintf(f, "</html>\n");

	fclose(f);
}

bool mate__export_graph(struct mate__Graph *graph, const char *file_path) {
	FILE *f = fopen(file_path, "w");
	if (f == NULL)
		return false; // TODO: error msg
	/* --- get graph order and size --- */
	const int n = mate__get_graph_order(graph);
	const int m = mate__get_graph_size(graph);
	/* --- write metadata --- */
	fprintf(f, "g {\n");
	fprintf(f, "\tundirected;\n"); // TODO
	fprintf(f, "}\n\n");
	/* --- write vertices --- */
	mate__set_graph_vertex_indices(graph);
	fprintf(f, "v(vec3 coord");
	for (int i = 0; i < graph->num_v_int_attr; i++)
		fprintf(f, ", int %s", graph->v_int_attr_labels[i]);
	for (int i = 0; i < graph->num_v_double_attr; i++)
		fprintf(f, ", double %s", graph->v_double_attr_labels[i]);
	fprintf(f, ") : %d {\n", n);
	struct mate__List_iterator it_v = mate__create_list_iterator(graph->v);
	struct mate__Graph_Vertex *v;
	int i = 0;
	while ((v = mate__get_next_list_iterator_item(&it_v)) != NULL) {
		fprintf(f, "\tv%d: \"%s\", %f, %f, %f", i, v->label, v->coord.x,
				v->coord.y, v->coord.z);
		for (int i = 0; i < graph->num_v_int_attr; i++)
			fprintf(f, ", %d", v->int_attr[i]);
		for (int i = 0; i < graph->num_v_double_attr; i++)
			fprintf(f, ", %f", v->double_attr[i]);
		fprintf(f, ";\n");
		i++;
	}
	fprintf(f, "}\n\n");
	/* --- write edges --- */
	fprintf(f, "e(");
	bool first = true;
	for (int i = 0; i < graph->num_e_int_attr; i++) {
		fprintf(f, "%sint %s", first ? "" : ", ", graph->e_int_attr_labels[i]);
		first = false;
	}
	for (int i = 0; i < graph->num_e_double_attr; i++) {
		fprintf(f, "%sdouble %s", first ? "" : ", ",
				graph->e_double_attr_labels[i]);
		first = false;
	}
	fprintf(f, ") : %d {\n", m);
	struct mate__List_iterator it_e = mate__create_list_iterator(graph->e);
	struct mate__Graph_Edge *e;
	i = 0;
	while ((e = mate__get_next_list_iterator_item(&it_e)) != NULL) {
		fprintf(f, "\te%d: v%d, v%d, \"%s\"", i, e->v0->index, e->v1->index,
				e->label);
		for (int i = 0; i < graph->num_e_int_attr; i++)
			fprintf(f, ", %d", e->int_attr[i]);
		for (int i = 0; i < graph->num_e_double_attr; i++)
			fprintf(f, ", %f", e->double_attr[i]);
		fprintf(f, ";\n");
		i++;
	}
	fprintf(f, "}\n\n");
	/* --- close file --- */
	fclose(f);
	return true;
}

struct mate__Graph_Attribute_Desc {
	char type[64];
	char id[64];
	int dest_array_idx;
};

struct mate__Graph *mate__import_graph(const char *file_path) {
	struct mate__Graph *graph =
		mate__create_graph_without_attributes(""); // TODO: label
	int graph_order = 0, graph_size = 0;

	int v_coord_dimension = 0;

	struct mate__List *v_attr_list = mate__create_list();
	struct mate__List *e_attr_list = mate__create_list();

	struct mate__Graph_Vertex **vertices = NULL;

	// TODO: delete lists!

	int vertex_idx = 0;
	int edge_idx = 0;

	FILE *f = fopen(file_path, "r");
	if (f == NULL)
		return NULL; // TODO: error msg
	/* --- read line by line --- */
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	enum { m_global, m_meta, m_vertices, m_edges } mode = m_global;
	while ((read = getline(&line, &len, f)) != -1) {
		if ((int)read == 1)
			continue; /* skip empty line "\n" */
		const int n = (int)read;
		// printf("%s\n", line); // TODO: remove this
		// printf("%d\n", n); // TODO: remove this
		if (line[0] == '}')
			mode = m_global;
		char block_type;
		switch (mode) {
		case m_global:
			block_type = line[0];
			switch (block_type) {
			case 'g': /* header declaration of graph metadata */
				mode = m_meta;
				break;
			case 'v': /* header declaration of vertices and edges */
			case 'e': /* header declaration of vertices and edges */
				if (block_type == 'v')
					mode = m_vertices;
				else
					mode = m_edges;
				struct mate__List *token_list =
					mate__tokenize(line, "(,):{"); // TODO: mate__del_tokenlist
				mate__print_tokenlist(token_list); // TODO: remove this
				const int token_list_len = mate__get_list_length(token_list);
				char *tk_str;
				if (token_list_len < 1 ||
					strcmp(tk_str = ((struct mate__Token *)mate__get_list_item(
										 token_list, 0))
										->str,
						   "v") != 0) {
					printf("Error: mate__import_graph(..): expecting 'v', but "
						   "got '%s'\n",
						   tk_str);
					return NULL;
				}
				if (token_list_len < 2 ||
					strcmp(tk_str = ((struct mate__Token *)mate__get_list_item(
										 token_list, 1))
										->str,
						   "(") != 0) {
					printf("Error: mate__import_graph(..): expecting '(' after "
						   "'v', but got '%s'\n",
						   tk_str);
					return NULL;
				}

				// TODO: implementation
				ASSERT(false);
			}
		}
	}
	/*
	xxxx




			enum {
			  m_attr_init,
			  m_attr_type,
			  m_attr_id,
			  m_attr_count
			} m_attr_mode = m_attr_init;
			char type[64], id[64], count[64];
			type[0] = '\0';
			id[0] = '\0';
			int tmp;
			int num_v_int_attr = 0;
			int num_v_double_attr = 0;
			int num_e_int_attr = 0;
			int num_e_double_attr = 0;
			for (int i = 0; i < n; i++) {
			  char ch = line[i];
			  switch (m_attr_mode) {
			  case m_attr_init:
				if (ch == '(')
				  m_attr_mode = m_attr_type;
				else if (ch == ':') {
				  m_attr_mode = m_attr_count;
				  count[0] = '\0';
				}
				break;
			  case m_attr_count:
				if (ch == '{') {
				  m_attr_mode = m_attr_init;
				  if (block_type == 'v') {
					graph_order = atoi(count);
					// printf("----- n := %d (%s) -----\n", graph_order, count);
				  } else {
					graph_size = atoi(count);
					// printf("----- m := %d (%s) -----\n", graph_size, count);
				  }
				} else if (ch != ' ') {
				  tmp = strlen(count);
				  count[tmp] = ch;
				  count[tmp + 1] = '\0';
				}
				break;
			  case m_attr_type:
				if (ch == ' ') {
				  m_attr_mode = m_attr_id;
				  id[0] = '\0';
				} else if (ch == ')') {
				  m_attr_mode = m_attr_init;
				} else {
				  tmp = strlen(type);
				  type[tmp] = ch;
				  type[tmp + 1] = '\0';
				}
				break;
			  case m_attr_id:
				if (ch == ',' || ch == ' ' || ch == ')') {
				  if (ch == ',') {
					m_attr_mode = m_attr_type;
				  } else {
					m_attr_mode = m_attr_init;
				  }
				  / * ----- add attribute to list --- * /
				  if (strcmp(type, "vec3") ==
					  0) { // TODO: also support other dimensions!
					v_coord_dimension = 3;
					printf("PARSED VEC3\n");
				  } else if (strcmp(type, "int") == 0 ||
							 strcmp(type, "double") == 0) {
					struct mate__Graph_Attribute_Desc *dag = ALLOC(
						struct mate__Graph_Attribute_Desc, 1); // TODO: clear
	mem strcpy(dag->type, type); strcpy(dag->id, id); printf("----- push %s %s
	%d -----\n", dag->type, dag->id, dag->dest_array_idx); ASSERT(false); if
	(block_type == 'v') { dag->dest_array_idx = strcmp(type,"int")==0 ?
	num_v_int_attr ++ : num_v_double_attr ++; mate__push_to_list(v_attr_list,
	dag); } else { dag->dest_array_idx = strcmp(type,"int")==0 ? num_e_int_attr
	++ : num_e_double_attr ++; mate__push_to_list(e_attr_list, dag);
					}
					type[0] = '\0';
				  } else {
					printf("----- '%s' -----\n", type);
					ASSERT(false);
					return NULL; // TODO: error message + clear memory
				  }
				} else {
				  tmp = strlen(id);
				  id[tmp] = ch;
				  id[tmp + 1] = '\0';
				}
				break;
			  }
			}
			/ * --- add attributes to graph --- * /
			char *label;
			int i;
			if (block_type == 'v') { / * vertex attributes * /
			  int num_int = 0;
			  int num_double = 0;
			  struct mate__List_iterator it =
				  mate__create_list_iterator(v_attr_list);
			  struct mate__Graph_Attribute_Desc *gad;
			  while ((gad = mate__get_next_list_iterator_item(&it)) != NULL) {
				if (strcmp(gad->type, "int") == 0)
				  num_int ++;
				else if (strcmp(gad->type, "double") == 0)
				  num_double ++;
				else
				  ASSERT(false);
			  }

			  printf("abc %d %d\n", num_int, num_double);

			  graph->num_v_int_attr = num_int;
			  graph->v_int_attr_labels = ALLOC(char *, graph->num_v_int_attr);
			  graph->num_v_double_attr = num_double;
			  graph->v_double_attr_labels = ALLOC(char *,
	graph->num_v_double_attr); it = mate__create_list_iterator(v_attr_list); int
	i_int = 0, i_double = 0; while ((gad =
	mate__get_next_list_iterator_item(&it)) != NULL) { if (strcmp(gad->type,
	"int") == 0) { graph->v_int_attr_labels[i_int] = strdup(gad->id); i_int ++;
				} else if (strcmp(gad->type, "double") == 0) {
				  graph->v_double_attr_labels[i_double] = strdup(gad->id);
				  i_double ++;
				} else {
				  ASSERT(false);
				}
			  }
			} else { / * edge attributes * /
			  int num_int = 0;
			  int num_double = 0;
			  struct mate__List_iterator it =
				  mate__create_list_iterator(e_attr_list);
			  struct mate__Graph_Attribute_Desc *gad;
			  while ((gad = mate__get_next_list_iterator_item(&it)) != NULL) {
				if (strcmp(gad->type, "int") == 0)
				  num_int ++;
				else if (strcmp(gad->type, "double") == 0)
				  num_double ++;
				else
				  ASSERT(false);
			  }
			  graph->num_e_int_attr = num_int;
			  graph->e_int_attr_labels = ALLOC(char *, graph->num_e_int_attr);
			  graph->num_e_double_attr = num_double;
			  graph->e_double_attr_labels = ALLOC(char *,
	graph->num_e_double_attr); it = mate__create_list_iterator(e_attr_list); int
	i_int = 0, i_double = 0; while ((gad =
	mate__get_next_list_iterator_item(&it)) != NULL) { if (strcmp(gad->type,
	"int") == 0) { graph->e_int_attr_labels[i_int] = strdup(gad->id); i_int++;
				} else if (strcmp(gad->type, "double") == 0) {
				  graph->e_double_attr_labels[i_double] = strdup(gad->id);
				  i_double++;
				} else {
				  ASSERT(false);
				}
			  }
			}
			mate__print_graph_metadata(graph); // TODO: remove this line
			printf("########################\n");
			break;
		  }
		  break;
		case m_meta:
		  if (strcmp(line, "\tundirected;\n") == 0) {
			// TODO
			printf("graph is undirected\n");
		  }
		  break;
		case m_vertices:
		  if (vertices == NULL)
			vertices = ALLOC(struct mate__Graph_Vertex *, graph_order);
		  vertices[vertex_idx] = mate__add_vertex_to_graph(graph, "");
		  // printf("--- graph order: %d ---\n", graph_order);
		  // printf("--- graph size: %d ---\n", graph_size);
		  enum { v_label, v_ident, v_coord, v_attrib } v_mode = v_label;
		  char label[64], id[64], value[64];
		  int coord_idx, attr_idx;
		  int tmp;
		  bool within_str = false;
		  label[0] = '\0';
		  id[0] = '\0';
		  for (int i = 0; i < n; i++) {
			char ch = line[i];
			switch (v_mode) {
			case v_label:
			  if (ch == '\t' || ch == ' ') {
				/ * do nothing * /
			  } else if (ch == ':') {
				v_mode = v_ident;
				printf("***** label '%s' *****\n", label);
			  } else {
				tmp = strlen(label);
				label[tmp] = ch;
				label[tmp + 1] = '\0';
			  }
			  break;
			case v_ident:
			  if (ch == ' ' || ch == '\t') {
				/ * do nothing * /
			  } else if (ch == '"') {
				within_str = !within_str;
			  } else if (ch == ',' || ch == ';') {
				if (v_coord_dimension > 0) {
				  coord_idx = 0;
				  v_mode = v_coord;
				  value[0] = '\0';
				} else {
				  v_mode = v_attrib;
				  attr_idx = 0;
				}
				printf("***** id '%s' *****\n", id);
			  } else if (within_str) {
				tmp = strlen(id);
				id[tmp] = ch;
				id[tmp + 1] = '\0';
			  }
			  break;
			case v_coord:
			  if (ch == ' ' || ch == '\t') {
				/ * do nothing * /
			  } else if (ch == ',' || ch == ';') {
				printf("COORD: '%f'\n", atof(value));
				switch (coord_idx) {
				case 0:
				  vertices[vertex_idx]->coord.x = atof(value);
				  break;
				case 1:
				  vertices[vertex_idx]->coord.y = atof(value);
				  break;
				case 2:
				  vertices[vertex_idx]->coord.z = atof(value);
				  break;
				default:
				  assert(false); // TODO: unimplemented
				}
				value[0] = '\0';
				if (coord_idx < v_coord_dimension - 1) {
				  v_mode = v_coord;
				  coord_idx ++;
				} else {
				  v_mode = v_attrib;
				  attr_idx = 0;
				}
			  } else {
				tmp = strlen(value);
				value[tmp] = ch;
				value[tmp + 1] = '\0';
			  }
			  break;
			case v_attrib:
			  if (ch == ' ' || ch == '\t') {
				/ * do nothing * /
			  } else if (ch == ',' || ch == ';') {
				value[0] = '\0';
				printf("##### %d %d\n", attr_idx,
	mate__get_list_length(v_attr_list)); ASSERT( attr_idx <
	mate__get_list_length(v_attr_list) ); struct mate__Graph_Attribute_Desc *dag
	= mate__get_list_item(v_attr_list, attr_idx); if(strcmp(dag->type, "int") ==
	0) vertices[vertex_idx]->int_attr[dag->dest_array_idx] = atof(value); else
	if(strcmp(dag->type, "double") == 0)
				  vertices[vertex_idx]->double_attr[dag->dest_array_idx] =
	atof(value); else ASSERT(false); attr_idx ++; } else { tmp = strlen(value);
				value[tmp] = ch;
				value[tmp + 1] = '\0';
			  }
			}
		  }
		  vertex_idx++;
		  break;
		case m_edges:
		  break;
		}
	  }
	  / * --- free temporary memory --- * /
	  free(vertices);
	  / * --- close file --- * /
	  fclose(f);
	  return graph;*/
}
