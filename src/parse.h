/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== PARSER ============================================================= */

#ifndef __PARSE__H
#define __PARSE__H

#include "list.h"

/* ----- characters --------------------------------------------------------- */

#define IS_CHAR_NUMBER(CH) ((CH) >= '0' && (CH) <= '9')
#define IS_CHAR_NUMBER_1(CH) ((CH) >= '1' && (CH) <= '9')
#define IS_CHAR_UPPERCASE_LETTER(CH) ((CH) >= 'A' && (CH) <= 'Z')
#define IS_CHAR_LOWERCASE_LETTER(CH) ((CH) >= 'a' && (CH) <= 'z')
#define IS_CHAR_LETTER(CH)                                                     \
	(IS_CHAR_UPPERCASE_LETTER(CH) || IS_CHAR_LOWERCASE_LETTER(CH))

/* ----- 1-token parser ----------------------------------------------------- */

/**
 * mate__is_identifier - check if given string is an identifier
 * @str:           string to be checked
 */
bool mate__is_identifier(const char *str);

/**
 * mate__is_integer - check if given string is an integer constant
 * @str:           string to be checked
 */
bool mate__is_integer(const char *str);

/**
 * mate__is_double - check if given string is an double constant
 * @str:           string to be checked
 */
bool mate__is_double(const char *str);

/* ----- tokenizer ---------------------------------------------------------- */

struct mate__Token {
	char *str;
	int pos;
};

/**
 * mate__tokenize - tokenize string into list of tokens
 * @input:         string to tokenize
 * @delimiters:    delimiters
 */
struct mate__List *mate__tokenize(const char *input, const char *delimiters);

/**
 * mate__del_tokenlist - frees memory of token list
 * @list:          token list to be erased
 */
void mate__del_tokenlist(struct mate__List *list);

/**
 * mate__print_tokenlist - print tokenlist to stdout
 * @list:          token list to be printed
 */
void mate__print_tokenlist(struct mate__List *list);

/* ----- parser ------------------------------------------------------------- */

// TODO
// 'v' '(' [ 'vec3' @1 'coord' ',' ] [ ('int' @2 |'double' @2 ) id @3  { ','
// ('int' @4 |'double' @4 ) id @5 } ] ')' ':' int @6 '{'
// void mate__parse(const str grammar, struct mate__List *tokens)

#endif
