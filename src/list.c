/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdbool.h>

#include "common.h"

#include "list.h"

struct mate__List *mate__create_list(void) {
	struct mate__List *list = ALLOC(struct mate__List, 1);
	list->first = NULL;
	list->last = NULL;
	list->len = 0;
	return list;
}

void mate__clear_list(struct mate__List *list, bool free_data,
					  void (*free_function)(void *ptr)) {
	struct mate__List_item *item = list->first, *next;
	while (item != NULL) {
		next = item->next;
		if (free_data && item->data != NULL) {
			// TODO: free function
			free(item->data);
		}
		free(item);
		item = next;
	}
	list->first = NULL;
	list->last = NULL;
	list->len = 0;
}

void mate__delete_list(struct mate__List *list, bool free_data,
					   void (*free_function)(void *ptr)) {
	mate__clear_list(list, free_data, free_function);
	free(list);
}

struct mate__List_item *mate__push_to_list(struct mate__List *list,
										   void *data) {
	struct mate__List_item *item = ALLOC(struct mate__List_item, 1);
	item->data = data;
	item->prev = list->last;
	if (item->prev != NULL)
		item->prev->next = item;
	item->next = NULL;
	if (list->first == NULL)
		list->first = item;
	list->last = item;
	list->len++;
	return item;
}

void mate__push_list_to_list(struct mate__List *list, struct mate__List *rhs) {
	void *data;
	mate__iterate_list(it, data, rhs) {
		mate__push_to_list(list, data);
	}
}

struct mate__List_item *mate__insert_to_list_begin(struct mate__List *list, void *data) {
	struct mate__List_item *item = ALLOC(struct mate__List_item, 1);
	item->data = data;
	item->prev = NULL;
	item->next = list->first;
	if(item->next != NULL)
		item->next->prev = item;
	list->first = item;
	if(list->last == NULL)
		list->last = item;
	list->len ++;
	return item;
	//TODO: tests !!!!!
}

void *mate__pop_from_list(struct mate__List *list) {
	struct mate__List_item *item = list->last;
	if (item == NULL)
		return NULL;
	void *data = item->data;
	list->last = item->prev;
	if (list->last == NULL)
		list->first = NULL;
	else
		list->last->next = NULL;
	free(item);
	list->len--;
	return data;
}

void *mate__get_list_item(struct mate__List *list, int index) {
	if (index < 0)
		mate__error("mate__get_list_item(): invalid index");
	if (index < list->len / 2) {
		/* search forward */
		int i = 0;
		struct mate__List_item *item = list->first;
		while (item != NULL) {
			if (i == index)
				return item->data;
			i++;
			item = item->next;
		}
	} else {
		/* search backward */
		int i = list->len - 1;
		struct mate__List_item *item = list->last;
		while (item != NULL) {
			if (i == index)
				return item->data;
			i--;
			item = item->prev;
		}
	}
	mate__error("mate__get_list_item(): invalid index");
	return NULL;
}

// TODO: tests
void *mate__get_list_item__rev(struct mate__List *list, int index) {
	if(index >= 0)
		return mate__get_list_item(list, index);
	else
		return mate__get_list_item(list, list->len + index);
	return NULL;
}

void *mate__get_first_list_item(struct mate__List *list) {
	if (list->first == NULL)
		return NULL;
	return list->first->data;
}

void *mate__get_last_list_item(struct mate__List *list) {
	if (list->last == NULL)
		return NULL;
	return list->last->data;
}

void remove_item(struct mate__List *list, struct mate__List_item *item) {
	if (item == NULL)
		return;
	if (item->prev != NULL)
		item->prev->next = item->next;
	if (item->next != NULL)
		item->next->prev = item->prev;
	if (item == list->first)
		list->first = item->next;
	if (item == list->last)
		list->last = item->prev;
	list->len--;
	free(item);
}

void *mate__delete_list_item(struct mate__List *list, int index) {
	if (index < 0)
		return NULL;
	int i = 0;
	struct mate__List_item *item = list->first;
	while (item != NULL) {
		if (i == index) {
			void *data = item->data;
			remove_item(list, item);
			return data;
		}
		i++;
		item = item->next;
	}
	return NULL;
}

void mate__delete_list_item_by_data_pointer(struct mate__List *list,
											void *data) {
	/* --- first try to delete last item (faster for e.g. MST algorithm) --- */
	if (list->last != NULL && list->last->data == data) {
		remove_item(list, list->last);
		return;
	}
	/* --- find item and delete --- */
	struct mate__List_item *item = list->first;
	while (item != NULL) {
		if (item->data == data) {
			remove_item(list, item);
			return;
		}
		item = item->next;
	}
}

int mate__calc_list_length(struct mate__List *list) {
	int size = 0;
	struct mate__List_item *item = list->first;
	while (item != NULL) {
		size++;
		item = item->next;
	}
	list->len = size;
	return size;
}

int mate__get_list_length(struct mate__List *list) { return list->len; }

struct mate__List_iterator mate__create_list_iterator(struct mate__List *list) {
	struct mate__List_iterator iterator;
	iterator.list = list;
	iterator.current = NULL;
	iterator.init_forward = true;
	iterator.init_backward = false;
	return iterator;
}

void mate__set_list_iterator_to_front(struct mate__List_iterator *iterator) {
	iterator->current = NULL;
	iterator->init_forward = true;
	iterator->init_backward = false;
}

void mate__set_list_iterator_to_back(struct mate__List_iterator *iterator) {
	iterator->current = NULL;
	iterator->init_forward = false;
	iterator->init_backward = true;
}

void *
mate__get_current_list_iterator_item(struct mate__List_iterator *iterator) {
	return iterator->current == NULL ? NULL : iterator->current->data;
}

void *mate__get_next_list_iterator_item(struct mate__List_iterator *iterator) {
	if (iterator->init_forward) {
		iterator->init_forward = false;
		iterator->current = iterator->list->first;
	} else if (iterator->init_backward) {
		iterator->current = NULL;
	} else if (iterator->current != NULL) {
		iterator->current = iterator->current->next;
	}
	return iterator->current == NULL ? NULL : iterator->current->data;
}

void *mate__get_prev_list_iterator_item(struct mate__List_iterator *iterator) {
	if (iterator->init_forward) {
		iterator->current = NULL;
	} else if (iterator->init_backward) {
		iterator->init_backward = false;
		iterator->current = iterator->list->last;
	} else if (iterator->current != NULL) {
		iterator->current = iterator->current->prev;
	}
	return iterator->current == NULL ? NULL : iterator->current->data;
}

void mate__delete_current_list_iterator_item(
	struct mate__List_iterator *iterator) {
	remove_item(iterator->list, iterator->current);
}
