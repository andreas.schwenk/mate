/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "common.h"

#include "hash.h"

int mate__standard_hash_function(char *str, int n) {
	/* based on:
	 * http://cseweb.ucsd.edu/~kube/cls/100/Lectures/lec16/lec16-14.html */
	uint64_t h = 0;
	const int str_len = strlen(str);
	for (int i = 0; i < str_len; i++) {
		char c = str[i];
		h = (uint64_t)31 * h + (uint64_t)c;
	}
	int res = (int)(h % (uint64_t)n);
	// printf("*** %s %d\n", str, res);
	return res;
}

struct mate__Hashtable *
mate__create_hashtable(const int n, int (*hash_function)(char *str, int n)) {
	struct mate__Hashtable *ht = ALLOC(struct mate__Hashtable, 1);
	ht->n = n;
	ht->hash_function = hash_function;
	ht->buckets = ALLOC(struct mate__List *, n);
	for (int i = 0; i < n; i++)
		ht->buckets[i] = NULL;
	return ht;
}

void mate__delete_hashtable(struct mate__Hashtable *ht) {
	// TODO
}

struct mate__Hashtable_Item *
mate__insert_into_hashtable(struct mate__Hashtable *ht, char *id, void *data) {
	struct mate__Hashtable_Item *item = ALLOC(struct mate__Hashtable_Item, 1);
	item->id = strdup(id);
	item->data = data;
	int idx = ht->hash_function(id, ht->n);
	// printf("--- %s %d\n", id, idx);
	if (ht->buckets[idx] == NULL)
		ht->buckets[idx] = mate__create_list();
	mate__push_to_list(ht->buckets[idx], item);
	return item;
}

void *mate__get_hashtable_item(struct mate__Hashtable *ht, char *id) {
	int idx = ht->hash_function(id, ht->n);
	struct mate__List *bucket = ht->buckets[idx];
	if (bucket == NULL)
		return NULL;
	struct mate__List_iterator iterator = mate__create_list_iterator(bucket);
	struct mate__Hashtable_Item *item;
	while ((item = mate__get_next_list_iterator_item(&iterator)) != NULL) {
		if (strcmp(id, item->id) == 0)
			return item->data;
	}
	return NULL;
}

double mate__get_hashtable_load_factor(struct mate__Hashtable *ht) {
	double num_occupied = 0.0;
	for (int i = 0; i < ht->n; i++)
		if (ht->buckets[i] != NULL)
			num_occupied += mate__get_list_length(ht->buckets[i]) > 0 ? 1 : 0;
	return num_occupied / (double)ht->n;
}

int mate__get_hashtable_length(struct mate__Hashtable *ht) {
	int n = 0;
	for (int i = 0; i < ht->n; i++)
		n += ht->buckets[i] == NULL ? 0 : mate__get_list_length(ht->buckets[i]);
	return n;
}

struct mate__Hashtable_Iterator
mate__create_hashtable_iterator(struct mate__Hashtable *ht) {
	struct mate__Hashtable_Iterator iterator;
	iterator.first = true;
	iterator.last = false;
	iterator.bucket_idx = 0;
	iterator.hashtable = ht;
	return iterator;
}

void *mate__get_current_hashtable_iterator_item(
	struct mate__Hashtable_Iterator *iterator) {
	ASSERT(false); // TODO: unimplemented
}

void *mate__get_next_hashtable_iterator_item(
	struct mate__Hashtable_Iterator *iterator) {
	if (iterator->last || iterator->bucket_idx >= iterator->hashtable->n)
		return NULL;
	if (iterator->first)
		iterator->first = false;
	else {
		void *data =
			mate__get_next_list_iterator_item(&iterator->list_iterator);
		if (data != NULL)
			return ((struct mate__Hashtable_Item *)data)->data;
		else
			iterator->bucket_idx++;
	}
	for (; iterator->bucket_idx < iterator->hashtable->n;
		 iterator->bucket_idx++) {
		struct mate__List *bucket =
			iterator->hashtable->buckets[iterator->bucket_idx];
		if (bucket != NULL) {
			iterator->list_iterator = mate__create_list_iterator(bucket);
			return ((struct mate__Hashtable_Item *)
						mate__get_next_list_iterator_item(
							&iterator->list_iterator))
				->data;
		}
	}
	iterator->last = true;
	return NULL;
}
