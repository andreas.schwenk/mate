/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== LIST DATA STRUCTURE ================================================ */

#ifndef __LIST__H
#define __LIST__H

#include <stdbool.h>

#include "common.h"

/* ----- list --------------------------------------------------------------- */

struct mate__List_item {
	struct mate__List_item *prev;
	struct mate__List_item *next;
	void *data;
};

struct mate__List {
	struct mate__List_item *first;
	struct mate__List_item *last;
	int len;
};

/**
 * mate__create_list - create a new list
 * @return:        pointer to new list
 */
struct mate__List *mate__create_list(void);

/**
 * mate__clear_list - delete elements of list
 * @list:          list to be cleared
 * @free_data:     free memory of stored data. Note that complex data types
 * could point to memory that is NOT deleted automatically.
 * TODO: free_function
 */
void mate__clear_list(struct mate__List *list, bool free_data,
					  void (*free_function)(void *ptr));

/**
 * mate__delete_list - delete elements and list
 * @list:          list to be erased
 * @free_data:     free memory of stored data. Note that complex data types
 * could point to memory that is NOT deleted automatically.
 * TODO: free_function
 */
void mate__delete_list(struct mate__List *list, bool free_data,
					   void (*free_function)(void *ptr));

/**
 * mate__push_to_list - pushes data on top of list
 * @return:        new list item
 * @list:          destination list
 * @data:          data to push onto list
 */
struct mate__List_item *mate__push_to_list(struct mate__List *list, void *data);

// TODO
void mate__push_list_to_list(struct mate__List *list, struct mate__List *rhs);

// TODO
struct mate__List_item *mate__insert_to_list_begin(struct mate__List *list, void *data);

/**
 * mate__pop_from_list - pop data from list
 * @return:        data of list item. memory must be freed by caller
 * @list:          source list
 */
void *mate__pop_from_list(struct mate__List *list);

/**
 * mate__get_list_item - get item from list at given index
 * @return:        data of list item or NULL, if index does not exist
 * @index:         list index
 */
void *mate__get_list_item(struct mate__List *list, int index);

// TODO: allows negative index for reverse counting
void *mate__get_list_item__rev(struct mate__List *list, int index);

/**
 * mate__get_first_list_item - get first item from list
 * @return:        data of first list item or NULL, if list is empty
 */
void *mate__get_first_list_item(struct mate__List *list);

/**
 * mate__get_last_list_item - get last item from list
 * @return:        data of last list item or NULL, if list is empty
 */
void *mate__get_last_list_item(struct mate__List *list);

/**
 * mate__delete_list_item - erase item from list at given index
 * @return:        data of list item. caller must delete data
 * @index:         list index
 */
void *mate__delete_list_item(struct mate__List *list, int index);

// TODO
void mate__delete_list_item_by_data_pointer(struct mate__List *list,
											void *data);

/**
 * mate__calc_list_length - calculates the number of items in list
 * @return:        list size
 * @list:          source list
 */
int mate__calc_list_length(struct mate__List *list);

/**
 * mate__get_list_length - gets the buffered number of items in list
 * @return:        list size
 * @list:          source list
 */
int mate__get_list_length(struct mate__List *list);

/* ----- list iterator ------------------------------------------------------ */

#define mate__iterate_list(ITERATOR_NAME, ITEMNAME, LIST)                      \
	struct mate__List_iterator ITERATOR_NAME =                                 \
		mate__create_list_iterator(LIST);                                      \
	while ((ITEMNAME = mate__get_next_list_iterator_item(&ITERATOR_NAME)) !=   \
		   NULL)

#define mate__iterate_list_backwards(ITERATOR_NAME, ITEMNAME, LIST)            \
	struct mate__List_iterator ITERATOR_NAME =                                 \
		mate__create_list_iterator(LIST);                                      \
	mate__set_list_iterator_to_back(&ITERATOR_NAME);                           \
	while ((ITEMNAME = mate__get_prev_list_iterator_item(&ITERATOR_NAME)) !=   \
		   NULL)

struct mate__List_iterator {
	bool init_forward;
	bool init_backward;
	struct mate__List *list;
	struct mate__List_item *current;
};

/**
 * mate__create_list_iterator - creates a list iterator. User must call
 * mate__list_iterator__get_next to get first data element.
 * @return:        list iterator
 * @list:          list
 */
struct mate__List_iterator mate__create_list_iterator(struct mate__List *list);

// TODO
void mate__set_list_iterator_to_front(struct mate__List_iterator *iterator);

// TODO
void mate__set_list_iterator_to_back(struct mate__List_iterator *iterator);

/**
 * mate__get_current_list_iterator_item - get current data element from list by
 * iterator.
 * @return:        data pointer to current element
 * @list:          list
 */
void *
mate__get_current_list_iterator_item(struct mate__List_iterator *iterator);

/**
 * mate__get_next_list_iterator_item - get next data element from list by
 * iterator.
 * @return:        data pointer to next element
 * @list:          list
 */
void *mate__get_next_list_iterator_item(struct mate__List_iterator *iterator);

// TODO
void *mate__get_prev_list_iterator_item(struct mate__List_iterator *iterator);

//
void mate__delete_current_list_iterator_item(
	struct mate__List_iterator *iterator);

/* -------------------------------------------------------------------------- */

#endif
