/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== GRAPH ============================================================== */

#ifndef __GRAPH__H
#define __GRAPH__H

#include <stdbool.h>

#include "la.h"
#include "list.h"

/* ----- graph algorithms --------------------------------------------------- */

struct mate__Graph;

// TODO
struct mate__Graph_Vertex {
	int index;
	struct mate__Graph *graph;
	char *label;
	struct mate__Vector3 coord;
	struct mate__List *e; /* adjacent edges */
	int *int_attr;
	double *double_attr;
};

// TODO
struct mate__Graph_Edge {
	struct mate__Graph *graph;
	char *label;
	struct mate__Graph_Vertex *v0;
	struct mate__Graph_Vertex *v1;
	int *int_attr;
	double *double_attr;
};

// TODO: documentation
struct mate__Graph {
	char *label;
	struct mate__List *v;
	struct mate__List *e;
	int num_v_int_attr;
	char **v_int_attr_labels;
	int num_v_double_attr;
	char **v_double_attr_labels;
	int num_e_int_attr;
	char **e_int_attr_labels;
	int num_e_double_attr;
	char **e_double_attr_labels;
};

// TODO
struct mate__Graph *mate__create_graph_without_attributes(char *label);

// TODO
struct mate__Graph *
mate__create_graph(char *label, const int num_v_int_attr,
				   char *v_int_attr_labels[], const int num_v_double_attr,
				   char *v_double_attr_labels[], const int num_e_int_attr,
				   char *e_int_attr_labels[], const int num_e_double_attr,
				   char *e_double_attr_labels[]);

// TODO
void mate__print_graph_metadata(struct mate__Graph *graph);

// TODO
void mate__delete_graph(struct mate__Graph *graph);

// TODO
void mate__export_graph_as_html_canvas(struct mate__Graph *graph,
									   const char *file_path);

// TODO
bool mate__export_graph(struct mate__Graph *graph, const char *file_path);

// TODO
struct mate__Graph *mate__import_graph(const char *file_path);

// TODO
int mate__get_graph_order(struct mate__Graph *graph);

// TODO
int mate__get_graph_size(struct mate__Graph *graph);

// TODO
void mate__add_complete_graph(struct mate__Graph *graph, int n, double cx,
							  double cy, double radius);

// TODO
void mate__add_circle_graph(struct mate__Graph *graph, int n, double cx,
							double cy, double radius);

// TODO.  weight_attr_label -> then only true / false
bool mate__add_graph_from_adjacency_matrix(struct mate__Graph *graph,
										   struct mate__Vector3 *coordinates,
										   struct mate__Matrix *adj_mat,
										   char *weight_attr_label);

// TODO
bool mate__is_adjacent(struct mate__Graph *graph, struct mate__Graph_Vertex *v0,
					   struct mate__Graph_Vertex *v1);

// TODO
struct mate__Matrix *mate__get_adjacency_matrix(struct mate__Graph *graph,
												char *weight_attr_label);

// TODO
struct mate__Vector3 *mate__get_coordinate_vector(struct mate__Graph *graph);

// TODO
struct mate__Graph_Vertex *mate__add_vertex_to_graph(struct mate__Graph *graph,
													 char *label);

// TODO
void mate__insert_vertex_to_graph(struct mate__Graph *graph,
								  struct mate__Graph_Vertex *vertex);

// TODO
struct mate__Graph_Edge *mate__add_edge_to_graph(struct mate__Graph *graph,
												 char *label,
												 struct mate__Graph_Vertex *v0,
												 struct mate__Graph_Vertex *v1);

// TODO
void mate__insert_edge_to_graph(struct mate__Graph *graph,
								struct mate__Graph_Edge *edge);

// TODO
void mate__remove_edge_from_graph(struct mate__Graph *graph,
								  struct mate__Graph_Edge *edge);

// TODO
bool mate__set_vertex_attribute__int(struct mate__Graph_Vertex *v,
									 const char *attr_label, int value);

// TODO
bool mate__set_vertex_attribute__double(struct mate__Graph_Vertex *v,
										const char *attr_label, double value);

// TODO
bool mate__set_edge_attribute__int(struct mate__Graph_Edge *v,
								   const char *attr_label, int value);

// TODO
bool mate__set_edge_attribute__double(struct mate__Graph_Edge *v,
									  const char *attr_label, double value);

// TODO
int mate__get_edge_attribute_index__double(struct mate__Graph *graph,
										   char *attr_label);

// TODO
int mate__get_graph_component_number(struct mate__Graph *graph);

// TODO
bool mate__is_tree(struct mate__Graph *graph);

// TODO
bool mate__is_forest(struct mate__Graph *graph);

// TODO
bool mate__graph_mst(struct mate__Graph *graph, char *weight_attr_label);

// TODO
void mate__set_graph_vertex_indices(struct mate__Graph *graph);

/* -------------------------------------------------------------------------- */

#endif
