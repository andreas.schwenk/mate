/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#include "graph.h"

void mate__set_graph_vertex_indices(struct mate__Graph *graph) {
	struct mate__List_iterator it = mate__create_list_iterator(graph->v);
	struct mate__Graph_Vertex *v;
	int i = 0;
	while ((v = mate__get_next_list_iterator_item(&it)) != NULL) {
		v->index = i++;
	}
}

struct mate__Graph *mate__create_graph_without_attributes(char *label) {
	struct mate__Graph *graph = ALLOC(struct mate__Graph, 1);
	graph->label = strdup(label);
	graph->v = mate__create_list();
	graph->e = mate__create_list();
	graph->num_v_int_attr = 0;
	graph->v_int_attr_labels = NULL;
	graph->num_v_double_attr = 0;
	graph->v_double_attr_labels = NULL;
	graph->num_e_int_attr = 0;
	graph->e_int_attr_labels = NULL;
	graph->num_e_double_attr = 0;
	graph->e_double_attr_labels = NULL;
	return graph;
}

struct mate__Graph *
mate__create_graph(char *label, const int num_v_int_attr,
				   char *v_int_attr_labels[], const int num_v_double_attr,
				   char *v_double_attr_labels[], const int num_e_int_attr,
				   char *e_int_attr_labels[], const int num_e_double_attr,
				   char *e_double_attr_labels[]) {

	struct mate__Graph *graph = mate__create_graph_without_attributes(label);

	graph->num_v_int_attr = num_v_int_attr;
	graph->v_int_attr_labels = ALLOC(char *, num_v_int_attr);
	for (int i = 0; i < num_v_int_attr; i++)
		graph->v_int_attr_labels[i] = strdup(v_int_attr_labels[i]);

	graph->num_v_double_attr = num_v_double_attr;
	graph->v_double_attr_labels = ALLOC(char *, num_v_double_attr);
	for (int i = 0; i < num_v_double_attr; i++)
		graph->v_double_attr_labels[i] = strdup(v_double_attr_labels[i]);

	graph->num_e_int_attr = num_e_int_attr;
	graph->e_int_attr_labels = ALLOC(char *, num_e_int_attr);
	for (int i = 0; i < num_e_int_attr; i++)
		graph->e_int_attr_labels[i] = strdup(e_int_attr_labels[i]);

	graph->num_e_double_attr = num_e_double_attr;
	graph->e_double_attr_labels = ALLOC(char *, num_e_double_attr);
	for (int i = 0; i < num_e_double_attr; i++)
		graph->e_double_attr_labels[i] = strdup(e_double_attr_labels[i]);

	return graph;
}

void mate__delete_graph(struct mate__Graph *graph) {
	// TODO
}

void mate__print_graph_metadata(struct mate__Graph *graph) {
	printf("---------- graph '%s' ----------\n", graph->label);
	printf("-- vertex integer attributes: --\n");
	for (int i = 0; i < graph->num_v_int_attr; i++)
		printf("ATTRIBUTE %02d: '%s'\n", i, graph->v_int_attr_labels[i]);
	printf("-- vertex double attributes: --\n");
	for (int i = 0; i < graph->num_v_double_attr; i++)
		printf("ATTRIBUTE %02d: '%s'\n", i, graph->v_double_attr_labels[i]);
	printf("-- edge integer attributes: --\n");
	for (int i = 0; i < graph->num_e_int_attr; i++)
		printf("ATTRIBUTE %02d: '%s'\n", i, graph->e_int_attr_labels[i]);
	printf("-- edge double attributes: --\n");
	for (int i = 0; i < graph->num_e_double_attr; i++)
		printf("ATTRIBUTE %02d: '%s'\n", i, graph->e_double_attr_labels[i]);
	printf("---------- end of graph '%s' ----------\n", graph->label);
}

int mate__get_graph_order(struct mate__Graph *graph) {
	return mate__get_list_length(graph->v);
}

int mate__get_graph_size(struct mate__Graph *graph) {
	return mate__get_list_length(graph->e);
}

void mate__add_complete_graph(struct mate__Graph *graph, int n, double cx,
							  double cy, double radius) {
	char label[64];
	const int old_order = mate__get_graph_order(graph);
	struct mate__Graph_Vertex **v = ALLOC(struct mate__Graph_Vertex *, n);
	for (int i = 0; i < n; i++) {
		sprintf(label, "v_%d", old_order + i);
		v[i] = mate__add_vertex_to_graph(graph, label);
		v[i]->coord.x = cx + radius * cos((double)i / (double)n * 2.0 * M_PI);
		v[i]->coord.y = cy + radius * sin((double)i / (double)n * 2.0 * M_PI);
	}
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			if (i == j)
				continue;
			sprintf(label, "e_%d_%d", i, j);
			mate__add_edge_to_graph(graph, label, v[i], v[j]);
		}
	}
}

void mate__add_circle_graph(struct mate__Graph *graph, int n, double cx,
							double cy, double radius) {
	char label[64];
	const int old_order = mate__get_graph_order(graph);
	struct mate__Graph_Vertex **v = ALLOC(struct mate__Graph_Vertex *, n);
	for (int i = 0; i < n; i++) {
		sprintf(label, "v_%d", old_order + i);
		v[i] = mate__add_vertex_to_graph(graph, label);
		v[i]->index = old_order + i;
		v[i]->coord.x = cx + radius * cos((double)i / (double)n * 2.0 * M_PI);
		v[i]->coord.y = cy + radius * sin((double)i / (double)n * 2.0 * M_PI);
	}
	for (int i = 0; i < n; i++) {
		sprintf(label, "e_%d_%d", v[i]->index, v[(i + 1) % n]->index);
		mate__add_edge_to_graph(graph, label, v[i], v[(i + 1) % n]);
	}
	free(v);
}

bool mate__add_graph_from_adjacency_matrix(struct mate__Graph *graph,
										   struct mate__Vector3 *coordinates,
										   struct mate__Matrix *adj_mat,
										   char *weight_attr_label) {
	if (adj_mat->M != adj_mat->N)
		return false; // TODO: error message
	char label[64];
	const int old_order = mate__get_graph_order(graph);
	const int n = adj_mat->M;
	struct mate__Graph_Vertex **v = ALLOC(struct mate__Graph_Vertex *, n);
	for (int i = 0; i < n; i++) {
		sprintf(label, "v_%d", old_order + i);
		v[i] = mate__add_vertex_to_graph(graph, label);
		v[i]->index = old_order + i;
		if (coordinates != NULL) {
			v[i]->coord.x = coordinates[i].x;
			v[i]->coord.y = coordinates[i].y;
			v[i]->coord.z = coordinates[i].z;
		}
	}
	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			double value = mate__get_matrix_element(adj_mat, i, j);
			if (fabs(value) > 1e-6) { // TODO: constant value
				// TODO: set edge
				sprintf(label, "e_%d_%d", v[i]->index, v[j]->index);
				mate__add_edge_to_graph(graph, label, v[i], v[j]);
			}
		}
	}
	free(v);
	return true;
}

bool mate__is_adjacent(struct mate__Graph *graph, struct mate__Graph_Vertex *v0,
					   struct mate__Graph_Vertex *v1) {
	struct mate__List_iterator it = mate__create_list_iterator(v0->e);
	struct mate__Graph_Edge *e;
	while ((e = mate__get_next_list_iterator_item(&it)) != NULL) {
		struct mate__Graph_Vertex *neighbor = e->v0 == v0 ? e->v1 : e->v0;
		if (neighbor == v1)
			return true;
	}
	return false;
}

struct mate__Matrix *mate__get_adjacency_matrix(struct mate__Graph *graph,
												char *weight_attr_label) {
	const int n = mate__get_graph_order(graph);
	// TODO: weight_attr_label
	struct mate__Matrix *mat = mate__create_matrix(n, n);
	int i = 0, j = 0;
	struct mate__Graph_Vertex *vi, *vj;
	struct mate__List_iterator it1 = mate__create_list_iterator(graph->v);
	while ((vi = mate__get_next_list_iterator_item(&it1)) != NULL) {
		struct mate__List_iterator it2 = mate__create_list_iterator(graph->v);
		while ((vj = mate__get_next_list_iterator_item(&it2)) != NULL) {
			mate__set_matrix_element(
				mat, i, j, mate__is_adjacent(graph, vi, vj) ? 1.0 : 0.0);
			j++;
		}
		i++;
	}
	return mat;
}

struct mate__Vector3 *mate__get_coordinate_vector(struct mate__Graph *graph) {
	const int n = mate__get_graph_order(graph);
	struct mate__Vector3 *coord_vec = ALLOC(struct mate__Vector3, n);
	struct mate__Graph_Vertex *v;
	struct mate__List_iterator it = mate__create_list_iterator(graph->v);
	int i = 0;
	while ((v = mate__get_next_list_iterator_item(&it)) != NULL) {
		coord_vec[i].x = v->coord.x;
		coord_vec[i].y = v->coord.y;
		coord_vec[i].z = v->coord.z;
		i++;
	}
	return coord_vec;
}

struct mate__Graph_Vertex *mate__add_vertex_to_graph(struct mate__Graph *graph,
													 char *label) {
	struct mate__Graph_Vertex *v = ALLOC(struct mate__Graph_Vertex, 1);
	v->graph = graph;
	v->label = strdup(label);
	mate__set_vector3(&v->coord, 0.0, 0.0, 0.0);
	v->e = mate__create_list();
	if (graph->num_v_int_attr > 0) {
		v->int_attr = ALLOC(int, graph->num_v_int_attr);
		for (int i = 0; i < graph->num_v_int_attr; i++)
			v->int_attr[i] = 0;
	} else {
		v->int_attr = NULL;
	}
	if (graph->num_v_double_attr > 0) {
		v->double_attr = ALLOC(double, graph->num_v_double_attr);
		for (int i = 0; i < graph->num_v_double_attr; i++)
			v->double_attr[i] = 0.0;
	} else {
		v->double_attr = NULL;
	}
	mate__insert_vertex_to_graph(graph, v);
	return v;
}

void mate__insert_vertex_to_graph(struct mate__Graph *graph,
								  struct mate__Graph_Vertex *vertex) {
	vertex->graph = graph;
	mate__push_to_list(graph->v, vertex);
}

struct mate__Graph_Edge *
mate__add_edge_to_graph(struct mate__Graph *graph, char *label,
						struct mate__Graph_Vertex *v0,
						struct mate__Graph_Vertex *v1) {
	struct mate__Graph_Edge *e = ALLOC(struct mate__Graph_Edge, 1);
	e->graph = graph;
	e->label = strdup(label);
	e->v0 = v0;
	e->v1 = v1;
	if (graph->num_e_int_attr > 0) {
		e->int_attr = ALLOC(int, graph->num_e_int_attr);
		for (int i = 0; i < graph->num_e_int_attr; i++)
			e->int_attr[i] = 0;
	} else {
		e->int_attr = NULL;
	}
	if (graph->num_e_double_attr > 0) {
		e->double_attr = ALLOC(double, graph->num_e_double_attr);
		for (int i = 0; i < graph->num_e_double_attr; i++)
			e->double_attr[i] = 0.0;
	} else {
		e->double_attr = NULL;
	}
	mate__insert_edge_to_graph(graph, e);
	return e;
}

void mate__insert_edge_to_graph(struct mate__Graph *graph,
								struct mate__Graph_Edge *edge) {
	edge->graph = graph;
	mate__push_to_list(graph->e, edge);
	mate__push_to_list(edge->v0->e, edge);
	mate__push_to_list(edge->v1->e, edge);
}

void mate__remove_edge_from_graph(struct mate__Graph *graph,
								  struct mate__Graph_Edge *edge) {
	mate__delete_list_item_by_data_pointer(edge->v0->e, edge);
	mate__delete_list_item_by_data_pointer(edge->v1->e, edge);
	mate__delete_list_item_by_data_pointer(graph->e, edge);
}

bool mate__set_vertex_attribute__int(struct mate__Graph_Vertex *v,
									 const char *attr_label, int value) {
	for (int i = 0; i < v->graph->num_v_int_attr; i++) {
		if (strcmp(v->graph->v_int_attr_labels[i], attr_label) == 0) {
			v->int_attr[i] = value;
			return true;
		}
	}
	return false;
}

bool mate__set_vertex_attribute__double(struct mate__Graph_Vertex *v,
										const char *attr_label, double value) {
	for (int i = 0; i < v->graph->num_v_double_attr; i++) {
		if (strcmp(v->graph->v_double_attr_labels[i], attr_label) == 0) {
			v->double_attr[i] = value;
			return true;
		}
	}
	return false;
}

bool mate__set_edge_attribute__int(struct mate__Graph_Edge *e,
								   const char *attr_label, int value) {
	for (int i = 0; i < e->graph->num_e_int_attr; i++) {
		if (strcmp(e->graph->e_int_attr_labels[i], attr_label) == 0) {
			e->int_attr[i] = value;
			return true;
		}
	}
	return false;
}

bool mate__set_edge_attribute__double(struct mate__Graph_Edge *e,
									  const char *attr_label, double value) {
	for (int i = 0; i < e->graph->num_e_double_attr; i++) {
		if (strcmp(e->graph->e_double_attr_labels[i], attr_label) == 0) {
			e->double_attr[i] = value;
			return true;
		}
	}
	return false;
}

int mate__get_edge_attribute_index__double(struct mate__Graph *graph,
										   char *attr_label) {
	for (int i = 0; i < graph->num_e_double_attr; i++) {
		if (strcmp(graph->e_double_attr_labels[i], attr_label) == 0)
			return i;
	}
	return -1;
}

void mate__flood_graph(struct mate__Graph_Vertex *v, bool *visited_flag) {
	if (visited_flag[v->index] == true)
		return;
	visited_flag[v->index] = true;
	struct mate__List_iterator it = mate__create_list_iterator(v->e);
	struct mate__Graph_Edge *e;
	while ((e = mate__get_next_list_iterator_item(&it)) != NULL) {
		struct mate__Graph_Vertex *neighbor = v == e->v0 ? e->v1 : e->v0;
		mate__flood_graph(neighbor, visited_flag);
	}
}

int mate__get_graph_component_number(struct mate__Graph *graph) {
	int k = 0; /* number of components */
	/* --- for each vertex: set index and visited-flag --- */
	mate__set_graph_vertex_indices(graph);
	const int n = mate__get_graph_order(graph);
	bool *visited_flag = ALLOC(bool, n);
	memset(visited_flag, 0, sizeof(bool) * n);
	/* --- for each vertex: visit all reachable vertices --- */
	struct mate__List_iterator it = mate__create_list_iterator(graph->v);
	struct mate__Graph_Vertex *v;
	int i = 0;
	while ((v = mate__get_next_list_iterator_item(&it)) != NULL) {
		if (visited_flag[v->index] == true)
			continue;
		mate__flood_graph(v, visited_flag);
		k++; /* if vertex was not yet visited, we have a new component */
	}
	/* --- free memory and return --- */
	free(visited_flag);
	return k;
}

bool mate__is_tree(struct mate__Graph *graph) {
	const int n = mate__get_graph_order(graph);
	const int m = mate__get_graph_size(graph);
	const int k = mate__get_graph_component_number(graph);
	if (k != 1)
		return false;
	return n - m == 1;
}

bool mate__is_forest(struct mate__Graph *graph) {
	const int n = mate__get_graph_order(graph);
	const int m = mate__get_graph_size(graph);
	const int k = mate__get_graph_component_number(graph);
	return n - m == k;
}

struct mst_Edge_Ref {
	struct mate__Graph_Edge *e;
	double weight;
};

int mst_cmp_fct(const void *a, const void *b) {
	return ((struct mst_Edge_Ref *)a)->weight -
		   ((struct mst_Edge_Ref *)b)->weight;
}

bool mate__graph_mst(struct mate__Graph *graph, char *weight_attr_label) {
	int weight_attr_idx =
		mate__get_edge_attribute_index__double(graph, weight_attr_label);
	if (weight_attr_idx < 0)
		return false; // TODO: error message

	const int n = mate__get_graph_order(graph);
	const int m = mate__get_graph_size(graph);

	/* --- sort edges by weight --- */
	/* (a) create array of edge references */
	struct mst_Edge_Ref *edge_list = ALLOC(struct mst_Edge_Ref, m);
	struct mate__List_iterator it = mate__create_list_iterator(graph->e);
	struct mate__Graph_Edge *e;
	int i = 0;
	while ((e = mate__get_next_list_iterator_item(&it)) != NULL) {
		edge_list[i].e = e;
		edge_list[i].weight = e->double_attr[weight_attr_idx];
		i++;
	}
	ASSERT(i == m);
	/* (b) sort array */
	qsort(edge_list, m, sizeof(struct mst_Edge_Ref), mst_cmp_fct);

	/* --- remove all edges from graph --- */
	/* (a) clear list */
	mate__clear_list(graph->e, false, NULL);
	/* (b) remove adjacent edges from vertices */
	struct mate__List_iterator it2 = mate__create_list_iterator(graph->v);
	struct mate__Graph_Vertex *v;
	while ((v = mate__get_next_list_iterator_item(&it2)) != NULL) {
		mate__clear_list(v->e, false, NULL);
	}

	/* --- insert edges to graph in order of sorted list --- */
	for (int i = 0; i < m; i++) {
		struct mst_Edge_Ref *edge_ref = &edge_list[i];
		mate__insert_edge_to_graph(graph, edge_ref->e);
		if (mate__is_forest(graph) == false) {
			mate__remove_edge_from_graph(graph, edge_ref->e);
		}
	}

	// TODO: free memory of non-used edges!

	free(edge_list);

	return true;
}
