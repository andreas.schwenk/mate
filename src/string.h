/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== STRING DATA STRUCTURE ============================================== */

#ifndef __STRING__H
#define __STRING__H

#include <stdbool.h>

#include "list.h"

/* ----- C string ----------------------------------------------------------- */

/* TODO: documentation */
struct mate__List* mate__split_c_string(const char *str, const char *delimiters, const char *keep_delimiters);

/* ----- mate string -------------------------------------------------------- */

struct mate__String {
	char *data;
	int n_used; /* including EOS */
	int n_allocated;
};

/**
 * mate__create_string - create a new string
 * @return:        pointer to new string
 */
struct mate__String *mate__create_string(void);

/**
 * mate__delete_string - delete a string
 * @return:        pointer to new string
 */
void mate__delete_string(struct mate__String *str);

// TODO
void mate__clear_string(struct mate__String *str);

// TODO
int mate__get_string_length(struct mate__String *str);

// TODO
int mate__get_string_line_count(struct mate__String *str);

// TODO
void mate__substring(struct mate__String *str, int start);

// TODO: end is NOT including
void mate__substring_with_end(struct mate__String *str, int start, int end);

/* TODO: documentation */
void mate__append_cstr_to_string(struct mate__String *str, const char *str2);

/* TODO: documentation */
void mate__append_string_to_string(struct mate__String *str,
								   const struct mate__String *str2);

/* TODO: documentation */
void mate__append_int_to_string(struct mate__String *str, const int64_t val);

/* TODO: documentation */
void mate__append_int_list_to_string(struct mate__String *str, struct mate__List *int_list);

/* TODO: documentation */
void mate__append_double_to_string(struct mate__String *str, const double val,
								   int num_decimal_places);

/* TODO: documentation */
void mate__append_double_list_to_string(struct mate__String *str, struct mate__List *double_list, int num_decimal_places);

/* TODO: documentation */
void mate__append_indent_to_string(struct mate__String *str, int indent);

/* TODO: documentation */
void mate__indent_string(struct mate__String *str, int indent);

/* TODO: documentation */
bool mate__string_equalto_string(struct mate__String *str1, struct mate__String *str2);

/* TODO: documentation */
bool mate__string_equalto_c_string(struct mate__String *str, const char *c_str);

/* TODO: documentation */
bool mate__C_string_equalto_c_string(const char *c_str1, const char *c_str2);

/* TODO: documentation */
bool mate__string_starts_with(struct mate__String *haystack, const char *needle);

/* TODO: documentation */
bool mate__string_ends_with(struct mate__String *haystack, const char *needle);

/* TODO: documentation */
bool mate__string_contains(struct mate__String *haystack, const char *needle);

/* TODO: documentation */
void mate__substitute_substring(struct mate__String *haystack, const char *needle, const char *replacement);

/* -------------------------------------------------------------------------- */

#endif
