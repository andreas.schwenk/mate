/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== CLUSTER := CLUSTERING ALGORITHMS =================================== */

#ifndef __CLUSTER__H
#define __CLUSTER__H

/* ----- k-Means ------------------------------------------------------------ */

/**
 * mate__cluster_kmeans_2d - clustering of a 2D data set via k-Means
 * @n:             number of input data points
 * @x:             x values of input data
 * @y:             y values of input data
 * @k:             number of clusters
 * @kx:            x values of resulting clusters (must be reserved at client)
 * @ky:            y values of resulting clusters (must be reserved at client)
 * @MAX_IT:        maxinum number of iterations
 * @EPSILON:       precision. Only assured, if number of iterations less MAX_IT
 */
int mate__cluster_kmeans_2d(const int n, double *x, double *y, const int k,
							double *kx, double *ky, const int MAX_IT,
							const double EPSILON);

/* -------------------------------------------------------------------------- */

#endif
