/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "common.h"

char *strdup_(const char *src) {
	const int n = (int)strlen(src);
	char *dst = ALLOC(char, n + 1);
	strcpy(dst, src);
	return dst;
}

int64_t mate__pow_int(int64_t base, int64_t exponent) {
	int64_t res = 1;
	while (exponent) {
		if (exponent & 0x01)
			res *= base;
		exponent /= 2;
		base *= base;
	}
	return res;
}

void mate__error(const char *msg) {
	fprintf(stderr, "error: %s\n", msg);
	fflush(stderr);
	exit(EXIT_FAILURE);
}
