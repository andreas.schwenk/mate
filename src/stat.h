/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== STAT := Statistical Algorithms ===================================== */

#ifndef __STAT__H
#define __STAT__H

#include <stdbool.h>

/* ----- basic functions ---------------------------------------------------- */

/**
 * mate__stat_sum - gets the sum of elements from a given array
 * @return:        sum i from 0 to N-1 of x
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_sum(double *x, const int N);

/**
 * mate__stat_sum - gets the sum of elements from a given array
 * @return:        sum i from 0 to N-1 of |x|
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_sum_abs(double *x, const int N);

/**
 * mate__stat_min - gets the minimum value from a given array
 * @return:        minimum value
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_min(double *x, const int N);

/**
 * mate__stat_min - gets the index of the minimum value from a given array
 * @return:        first index of a minimum of x
 * @x:             input array
 * @N:             number of elements of x
 */
int mate__stat_min_idx(double *x, const int N);

/**
 * mate__stat_max - gets the maximum value from a given array
 * @return:        maximum value
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_max(double *x, const int N);

/**
 * mate__stat_max - gets the index of the maximum value from a given array
 * @return:        first index of a maximum of x
 * @x:             input array
 * @N:             number of elements of x
 */
int mate__stat_max_idx(double *x, const int N);

/**
 * mate__stat_avrg - calculates the average value from a given array
 * @return:        average
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_avrg(double *x, const int N);

/**
 * mate__stat_stdev - calculates the standard deviation of a given array
 * @return:        standard deviation
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_stdev(double *x, const int N);

/**
 * mate__stat_median - calculates the median of a given array
 * @return:        median
 * @x:             input array
 * @N:             number of elements of x
 */
double mate__stat_median(double *x, const int N);

/**
 * mate__stat_covariance - calculates the covariance of two given arrays
 * @return:        covariance
 * @x:             first input array
 * yx:             second input array
 * @N:             number of elements of x and y
 */
double mate__stat_covariance(double *x, double *y, const int N);

/**
 * mate__stat_corr_coeff - calculates the correlation coefficient of two given
 * arrays
 * @return:        correlation coefficienct
 * @x:             first input array
 * yx:             second input array
 * @N:             number of elements of x and y
 */
double mate__stat_corr_coeff(double *x, double *y, const int N);

/* -------------------------------------------------------------------------- */

#endif
