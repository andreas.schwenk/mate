/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

#include "common.h"

int mate__cluster_kmeans_2d(const int n, double *x, double *y, const int k,
							double *kx, double *ky, const int MAX_IT,
							const double EPSILON) {
	int *m = ALLOC(int, n); // mapping: data point i -> cluster k
	int *numk = ALLOC(int, k);

	double *kx_old = ALLOC(double, k);
	double *ky_old = ALLOC(double, k);

	// --- set initial cluster positions ---
	for (int i = 0; i < k; i++) {
		int idx = rand() % n;
		kx[i] = x[idx];
		ky[i] = y[idx];
	}

	for (int z = 0; z < MAX_IT; z++) {
		// --- map data point to nearest cluster ---
		for (int i = 0; i < n; i++) {
			double mindist = 1e16;
			m[i] = 0;
			for (int j = 0; j < k; j++) {
				double dx = x[i] - kx[j];
				double dy = y[i] - ky[j];
				double dist = sqrt(dx * dx + dy * dy);
				if (dist < mindist) {
					m[i] = j;
					mindist = dist;
				}
			}
		}
		// --- recalculate centers ---
		for (int i = 0; i < k; i++) {
			kx_old[i] = kx[i];
			ky_old[i] = ky[i];
			kx[i] = 0.0;
			ky[i] = 0.0;
			numk[i] = 0;
		}
		for (int i = 0; i < n; i++) {
			int k = m[i];
			numk[k]++;
			kx[k] += x[i];
			ky[k] += y[i];
		}
		for (int i = 0; i < k; i++) {
			kx[i] /= (double)numk[i];
			ky[i] /= (double)numk[i];
		}
		// --- stop criterion ---
		bool stop = true;
		for (int i = 0; i < k; i++) {
			if (fabs(kx[i] - kx_old[i]) > EPSILON ||
				fabs(ky[i] - ky_old[i]) > EPSILON) {
				stop = false;
				break;
			}
		}
		if (stop)
			break;
	}

	free(kx_old);
	free(ky_old);

	free(m);
	free(numk);

	return 0;
}
