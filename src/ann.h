// TODO: switch to "C"

/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#ifndef ARTIFICIALNEURALNETWORK_H
#define ARTIFICIALNEURALNETWORK_H

struct Neuron {
	double *weights;
	double y;
	double delta;   // used in learning process (backtracking algorithm)
	double wChange; // used in learning process for the momentum term
					// (backtracking algorithm)
};

struct Layer {
	Neuron *neurons;
};

class ArtificialNeuralNetwork {
  public:
	ArtificialNeuralNetwork(int numberInputs, int numberOutputs,
							int numberHiddenLayers,
							int numberNeuronsInHiddenLayer, bool weightsGiven,
							double *initialWeights);

	~ArtificialNeuralNetwork();

	void run(double *inputValues, double *outputValues);

	void backpropagationAlgorithm(double *trainingInput, double *trainingOutput,
								  int numberOfTrainigsSets, double learningRate,
								  double momentum, int numEpochs,
								  bool showOutput);

	void showWeights(bool showMetadata);

  private:
	double getRandomNumber();
	void initWeights(bool randomized, double *startValues);
	void calculate();

	Neuron *inputs;
	Neuron *outputs;
	Layer *layers;

	int numberInputs;
	int numberOutputs;
	int numberHiddenLayers;
	int numberNeuronsInHiddenLayer;
};

#endif /* ARTIFICIALNEURALNETWORK_H */
