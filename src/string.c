/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#include "string.h"

/* ----- C string functions ------------------------------------------------- */

struct mate__List* mate__split_c_string(const char *str, const char *delimiters, const char *keep_delimiters) {
	struct mate__List* tokens = mate__create_list(); /* C-string */
	const int n = (int)strlen(str);
	const int n2 = (int)strlen(delimiters);
	const int n3 = (int)strlen(keep_delimiters);
	char tmp[2048];
	int tmp_k = 0;
	for(int i=0; i<n; i++) {
		char ch = str[i];
		bool is_delimiter = false;
		for(int j=0; j<n2; j++) {
			if(ch == delimiters[j]) {
				is_delimiter = true;
				break;
			}
		}
		if(is_delimiter) {
			tmp[tmp_k] = '\0';
			if(tmp_k > 0) {
				char *s = strdup_(tmp);
				mate__push_to_list(tokens, s);
				tmp_k = 0;
			}
			for(int j=0; j<n3; j++) {
				if(ch == keep_delimiters[j]) {
					char *s = ALLOC(char, 2);
					s[0] = ch;
					s[1] = '\0';
					mate__push_to_list(tokens, s);
					break;
				}
			}
		} else {
			tmp[tmp_k ++] = ch;
			ASSERT(tmp_k < 2048);
		}
	}
	if(tmp_k > 0) {
		char *s = strdup_(tmp);
		mate__push_to_list(tokens, s);
	}
	return tokens;
}

/* ----- mate string functions ---------------------------------------------- */

struct mate__String *mate__create_string(void) {
	struct mate__String *str;
	str = ALLOC(struct mate__String, 1);
	str->data = ALLOC(char, 64);
	str->data[0] = '\0';
	str->n_used = 1;
	str->n_allocated = 64;
	return str;
}

void mate__delete_string(struct mate__String *str) {
	DEALLOC(str->data);
	DEALLOC(str);
}

void mate__clear_string(struct mate__String *str) {
	str->data[0] = '\0';
	str->n_used = 1;
}

int mate__get_string_length(struct mate__String *str) {
	return str->n_used - 1;
}

int mate__get_string_line_count(struct mate__String *str) {
	int cnt = 1;
	int n = mate__get_string_length(str);
	for (int i = 0; i < n; i++) {
		if (str->data[i] == '\n')
			cnt++;
	}
	return cnt;
}

void mate__substring(struct mate__String *str, int start) {
	int end = mate__get_string_length(str);
	mate__substring_with_end(str, start, end);
}

void mate__substring_with_end(struct mate__String *str, int start, int end) {
	if (start < 0)
		start = 0;
	if (end >= mate__get_string_length(str))
		end = mate__get_string_length(str);
	char *tmp = ALLOC(char, end - start + 1);
	for (int i = start; i < end; i++)
		tmp[i - start] = str->data[i];
	tmp[end - start] = '\0';
	mate__clear_string(str);
	mate__append_cstr_to_string(str, tmp);
	free(tmp);
}

void mate__append_cstr_to_string(struct mate__String *str, const char *str2) {
	int n2 = strlen(str2);
	int n = str->n_used + n2;
	if (n > str->n_allocated) {
		str->data = REALLOC(str->data, char, n);
		str->n_allocated = n;
	}
	int start = str->n_used - 1;
	str->n_used = n;
	for (int i = 0; i < n2; i++)
		str->data[start + i] = str2[i];
	str->data[start + n2] = '\0';
}

void mate__append_string_to_string(struct mate__String *str,
								   const struct mate__String *str2) {
	mate__append_cstr_to_string(str, str2->data);
}

void mate__append_int_to_string(struct mate__String *str, const int64_t val) {
	char tmp[128];
	sprintf(tmp, "%lld", val);
	mate__append_cstr_to_string(str, tmp);
}

void mate__append_int_list_to_string(struct mate__String *str, struct mate__List *int_list) {
	mate__append_cstr_to_string(str, "[");
	char tmp[128];
	bool first = true;
	int *v;
	mate__iterate_list(it, v, int_list) {
		if(!first)
			mate__append_cstr_to_string(str, ", ");
		sprintf(tmp, "%d", (int)(*v));
		mate__append_cstr_to_string(str, tmp);
		first = false;
	}
	mate__append_cstr_to_string(str, "]");
}

void mate__append_double_to_string(struct mate__String *str, const double val,
								   int num_decimal_places) {
	char tmp[128];
	sprintf(tmp, "%.*f", num_decimal_places, val);
	mate__append_cstr_to_string(str, tmp);
}

void mate__append_double_list_to_string(struct mate__String *str, struct mate__List *double_list, int num_decimal_places) {
	mate__append_cstr_to_string(str, "[");
	char tmp[128];
	bool first = true;
	double *v;
	mate__iterate_list(it, v, double_list) {
		if(!first)
			mate__append_cstr_to_string(str, ", ");
		sprintf(tmp, "%.*f", num_decimal_places, *v);
		mate__append_cstr_to_string(str, tmp);
		first = false;
	}
	mate__append_cstr_to_string(str, "]");
}

void mate__append_indent_to_string(struct mate__String *str, int indent) {
	ASSERT(indent >= 0 && indent < 16);
	char tmp[16];
	for (int i = 0; i < indent; i++)
		tmp[i] = '\t';
	tmp[indent] = '\0';
	mate__append_cstr_to_string(str, tmp);
}

void mate__indent_string(struct mate__String *str, int indent) {
	ASSERT(indent >= 0 && indent < 16);
	int lines = mate__get_string_line_count(str);
	int n_orig = mate__get_string_length(str);
	char *tmp = ALLOC(char, n_orig + lines * indent + 1);

	int i, j, k = 0;
	for (i = 0; i < n_orig; i++) {
		char ch = str->data[i];
		if (i == 0 || ch == '\n') {
			if (ch == '\n')
				tmp[k++] = '\n';
			if (i != n_orig - 1) {
				for (j = 0; j < indent; j++)
					tmp[k++] = '\t';
			}
			if (i == 0)
				tmp[k++] = ch;
		} else
			tmp[k++] = ch;
	}
	tmp[k] = '\0';

	mate__clear_string(str);
	mate__append_cstr_to_string(str, tmp);
	free(tmp);
}

bool mate__string_equalto_string(struct mate__String *str1, struct mate__String *str2) {
	return strcmp(str1->data, str2->data) == 0;
}

bool mate__string_equalto_c_string(struct mate__String *str, const char *c_str) {
	return strcmp(str->data, c_str) == 0;
}

bool mate__C_string_equalto_c_string(const char *c_str1, const char *c_str2) {
	return strcmp(c_str1, c_str2) == 0;
}

bool mate__string_starts_with(struct mate__String *haystack, const char *needle) {
	int n_haystack = mate__get_string_length(haystack);
	int n_needle = strlen(needle);
	if(n_haystack==0 || n_needle==0 || n_needle > n_haystack)
		return false;
	for(int i=0; i<n_needle; i++)
		if(haystack->data[i] != needle[i])
			return false;
	return true;
}

bool mate__string_ends_with(struct mate__String *haystack, const char *needle) {
	int n_haystack = mate__get_string_length(haystack);
	int n_needle = strlen(needle);
	if(n_haystack==0 || n_needle==0 || n_needle > n_haystack)
		return false;
	for(int i=0; i<n_needle; i++)
		if(haystack->data[n_haystack-n_needle+i] != needle[i])
			return false;
	return true;
}

bool mate__string_contains(struct mate__String *haystack, const char *needle) {
	int n_haystack = mate__get_string_length(haystack);
	int n_needle = strlen(needle);
	if(n_haystack==0 || n_needle==0 || n_needle > n_haystack)
		return false;
	for(int i=0; i<=n_haystack-n_needle; i++) {
		for(int j=0; j<n_needle; j++) {
			if(haystack->data[i+j] != needle[j])
				break;
			if(j==n_needle-1)
				return true;
		}
	}
	return false;
}

void mate__substitute_substring(struct mate__String *haystack, const char *needle, const char *replacement) {
	// TODO: replace ALL found substrings (from right to left!!)
	int n_haystack = mate__get_string_length(haystack);
	int n_needle = strlen(needle);
	int n_replacement = strlen(replacement);
	if(n_haystack==0 || n_needle==0 || n_needle > n_haystack)
		return;
	for(int i=0; i<=n_haystack-n_needle; i++) {
		for(int j=0; j<n_needle; j++) {
			if(haystack->data[i+j] != needle[j])
				break;
			if(j==n_needle-1) {
				/* found substring in range from i ... i+j */
				char *tmp = ALLOC(char, n_haystack - n_needle + n_replacement + 1);
				int l=0;
				for(int k=0; k<i; k++)
					tmp[l++] = haystack->data[k];
				for(int k=0; k<n_replacement; k++)
					tmp[l++] = replacement[k];
				for(int k=i+j+1; k<n_haystack; k++)
					tmp[l++] = haystack->data[k];
				tmp[l++] = '\0';
				ASSERT(l == n_haystack - n_needle + n_replacement + 1);
				mate__clear_string(haystack);
				mate__append_cstr_to_string(haystack, tmp);
				DEALLOC(tmp);
				return;
			}
		}
	}
}
