/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "common.h"

#include "la.h"

struct mate__Vector *mate__create_vector(const int N) {
	struct mate__Vector *vec = ALLOC(struct mate__Vector, 1);
	vec->N = N;
	vec->v = ALLOC(double, N);
	return vec;
}

void mate__set_vector(struct mate__Vector *vec, ...) {
	va_list l;
	va_start(l, vec);
	for (int i = 0; i < vec->N; i++)
		vec->v[i] = va_arg(l, double);
	va_end(l);
}

void mate__set_vector3(struct mate__Vector3 *vec, double x, double y,
					   double z) {
	vec->x = x;
	vec->y = y;
	vec->z = z;
}

void mate__delete_vector(struct mate__Vector *vec) {
	if (vec == NULL)
		return;
	free(vec->v);
	free(vec);
}

void mate__add_vectors(struct mate__Vector *res, struct mate__Vector *lhs,
					   struct mate__Vector *rhs) {
	ASSERT(res->N == lhs->N);
	ASSERT(res->N == rhs->N);
	for (int i = 0; i < res->N; i++)
		res->v[i] = lhs->v[i] + rhs->v[i];
}

void mate__sub_vectors(struct mate__Vector *res, struct mate__Vector *lhs,
					   struct mate__Vector *rhs) {
	ASSERT(res->N == lhs->N);
	ASSERT(res->N == rhs->N);
	for (int i = 0; i < res->N; i++)
		res->v[i] = lhs->v[i] - rhs->v[i];
}

double mate__dot_vectors(struct mate__Vector *lhs, struct mate__Vector *rhs) {
	ASSERT(lhs->N == rhs->N);
	double res = 0.0;
	for (int i = 0; i < lhs->N; i++)
		res += lhs->v[i] * rhs->v[i];
	return res;
}

bool mate__compare_vectors(struct mate__Vector *v0, struct mate__Vector *v1,
						   const double EPSILON) {
	ASSERT(v0->N == v1->N);
	double d = 0.0;
	for (int i = 0; i < v0->N; i++)
		d += (v0->v[i] - v1->v[i]) * (v0->v[i] - v1->v[i]);
	d = sqrt(d);
	return d <= EPSILON;
}

void mate__cross_vectors(struct mate__Vector *res, struct mate__Vector *lhs,
						 struct mate__Vector *rhs) {
	ASSERT(res->N == 3);
	ASSERT(lhs->N == 3);
	ASSERT(rhs->N == 3);
	res->v[0] = lhs->v[1] * rhs->v[2] - lhs->v[2] * rhs->v[1];
	res->v[1] = lhs->v[2] * rhs->v[0] - lhs->v[0] * rhs->v[2];
	res->v[2] = lhs->v[0] * rhs->v[1] - lhs->v[1] * rhs->v[0];
}

struct mate__Matrix *mate__create_matrix(const int M, const int N) {
	struct mate__Matrix *mat = ALLOC(struct mate__Matrix, 1);
	mat->M = M;
	mat->N = N;
	mat->v = ALLOC(double, M *N);
	return mat;
}

void mate__set_matrix(struct mate__Matrix *mat, ...) {
	const int n = mat->M * mat->N;
	va_list l;
	va_start(l, mat);
	int i, j;
	for (int k = 0; k < n; k++) {
		i = k / mat->N;
		j = k % mat->N;
		mat->v[i * mat->N + j] = va_arg(l, double);
	}
	va_end(l);
}

void mate__set_matrix_row(struct mate__Matrix *mat, int i, ...) {
	ASSERT(i >= 0 && i < mat->M);
	va_list l;
	va_start(l, i);
	for (int j = 0; j < mat->N; j++)
		mat->v[i * mat->N + j] = va_arg(l, double);
	va_end(l);
}

void mate__set_matrix_col(struct mate__Matrix *mat, int j, ...) {
	ASSERT(j >= 0 && j < mat->N);
	va_list l;
	va_start(l, j);
	for (int i = 0; i < mat->M; i++)
		mat->v[i * mat->N + j] = va_arg(l, double);
	va_end(l);
}

void mate__delete_matrix(struct mate__Matrix *mat) {
	if (mat == NULL)
		return;
	free(mat->v);
	free(mat);
}

void mate__set_zero_matrix(struct mate__Matrix *mat) {
	const int n = mat->M * mat->N;
	for (int i = 0; i < n; i++)
		mat->v[i] = 0.0;
}

void mate__set_ones_matrix(struct mate__Matrix *mat) {
	const int n = mat->M * mat->N;
	for (int i = 0; i < n; i++)
		mat->v[i] = 1.0;
}

void mate__set_identity_matrix(struct mate__Matrix *mat) {
	ASSERT(mat->M == mat->N);
	const int n = mat->M * mat->N;
	for (int i = 0; i < n; i++)
		mat->v[i] = i % (mat->M + 1) == 0 ? 1.0 : 0.0;
}

void mate__add_matrices(struct mate__Matrix *res, struct mate__Matrix *lhs,
						struct mate__Matrix *rhs) {
	ASSERT(res->M == lhs->M);
	ASSERT(res->M == rhs->M);
	ASSERT(res->N == lhs->N);
	ASSERT(res->N == rhs->N);
	for (int i = 0; i < res->M; i++)
		for (int j = 0; j < res->N; j++)
			res->v[mate__matrix_idx(res, i, j)] =
				lhs->v[mate__matrix_idx(lhs, i, j)] +
				rhs->v[mate__matrix_idx(rhs, i, j)];
}

void mate__cadd_matrices(struct mate__Matrix *lhs_res,
						 struct mate__Matrix *rhs) {
	ASSERT(lhs_res->M == rhs->M);
	ASSERT(lhs_res->N == rhs->N);
	for (int i = 0; i < lhs_res->M; i++)
		for (int j = 0; j < lhs_res->N; j++)
			lhs_res->v[mate__matrix_idx(lhs_res, i, j)] +=
				rhs->v[mate__matrix_idx(rhs, i, j)];
}

void mate__sub_matrices(struct mate__Matrix *res, struct mate__Matrix *lhs,
						struct mate__Matrix *rhs) {
	ASSERT(res->M == lhs->M);
	ASSERT(res->M == rhs->M);
	ASSERT(res->N == lhs->N);
	ASSERT(res->N == rhs->N);
	for (int i = 0; i < res->M; i++)
		for (int j = 0; j < res->N; j++)
			res->v[mate__matrix_idx(res, i, j)] =
				lhs->v[mate__matrix_idx(lhs, i, j)] -
				rhs->v[mate__matrix_idx(rhs, i, j)];
}

void mate__csub_matrices(struct mate__Matrix *lhs_res,
						 struct mate__Matrix *rhs) {
	ASSERT(lhs_res->M == rhs->M);
	ASSERT(lhs_res->N == rhs->N);
	for (int i = 0; i < lhs_res->M; i++)
		for (int j = 0; j < lhs_res->N; j++)
			lhs_res->v[mate__matrix_idx(lhs_res, i, j)] -=
				rhs->v[mate__matrix_idx(rhs, i, j)];
}

void mate__mul_matrices(struct mate__Matrix *res, struct mate__Matrix *lhs,
						struct mate__Matrix *rhs) {
	ASSERT(lhs->M == res->M);
	ASSERT(rhs->N == res->N);
	ASSERT(lhs->N == rhs->M);
	const int K = lhs->N;
	for (int i = 0; i < res->M; i++) {
		for (int j = 0; j < res->N; j++) {
			res->v[mate__matrix_idx(res, i, j)] = 0.0;
			for (int k = 0; k < K; k++)
				res->v[mate__matrix_idx(res, i, j)] +=
					lhs->v[mate__matrix_idx(lhs, i, k)] *
					rhs->v[mate__matrix_idx(rhs, k, j)];
		}
	}
}

void mate__cmul_matrices(struct mate__Matrix *lhs_res,
						 struct mate__Matrix *rhs) {
	ASSERT(lhs_res->N == rhs->N);
	ASSERT(lhs_res->N == rhs->M);
	const int K = lhs_res->N;
	const int res_size = lhs_res->M * lhs_res->N;
	double *res = ALLOC(double, res_size);
	for (int i = 0; i < lhs_res->M; i++) {
		for (int j = 0; j < lhs_res->N; j++) {
			res[mate__matrix_idx(lhs_res, i, j)] = 0.0;
			for (int k = 0; k < K; k++)
				res[mate__matrix_idx(lhs_res, i, j)] +=
					lhs_res->v[mate__matrix_idx(lhs_res, i, k)] *
					rhs->v[mate__matrix_idx(rhs, k, j)];
		}
	}
	memcpy(lhs_res->v, res, sizeof(double) * res_size);
	free(res);
}

void mate__mul_matrix_vec(struct mate__Vector *res, struct mate__Matrix *lhs,
						  struct mate__Vector *rhs) {
	ASSERT(lhs->M == res->N);
	ASSERT(lhs->N == rhs->N);
	const int K = lhs->N;
	for (int i = 0; i < lhs->M; i++) {
		res->v[i] = 0.0;
		for (int k = 0; k < K; k++)
			res->v[i] += lhs->v[mate__matrix_idx(lhs, i, k)] * rhs->v[k];
	}
}

void mate__cmul_matrix_vec(struct mate__Matrix *lhs,
						   struct mate__Vector *rhs_res) {
	ASSERT(lhs->M == lhs->N); /* square matrix */
	ASSERT(lhs->N == rhs_res->N);
	const int K = lhs->N;
	double *res = ALLOC(double, K);
	for (int i = 0; i < K; i++) {
		res[i] = 0.0;
		for (int k = 0; k < K; k++)
			res[i] += lhs->v[mate__matrix_idx(lhs, i, k)] * rhs_res->v[k];
	}
	memcpy(rhs_res->v, res, sizeof(double) * K);
	free(res);
}

void mate__transpose_matrix(struct mate__Matrix *res,
							struct mate__Matrix *mat) {
	ASSERT(res->M == mat->N);
	ASSERT(res->N == mat->M);
	for (int i = 0; i < res->M; i++)
		for (int j = 0; j < res->N; j++)
			res->v[mate__matrix_idx(res, i, j)] =
				mat->v[mate__matrix_idx(mat, j, i)];
}

double mate__trace(struct mate__Matrix *mat) {
	ASSERT(mat->M == mat->N);
	double trace = 0.0;
	for (int i = 0; i < mat->M; i++)
		trace += mate__get_matrix_element(mat, i, i);
	return trace;
}

double mate__determinant(struct mate__Matrix *mat) {
	ASSERT(mat->M == mat->N);
	const int n = mat->M;
	struct mate__Matrix *minor;
	int i, j, k, jm;
	double det, tmp;
	/* calculate small matrices non-recursively (faster) */
	switch (n) {
	case 1:
		return mat->v[0];
	case 2:
		return mat->v[0] * mat->v[3] - mat->v[1] * mat->v[2];
	case 3:
		return mat->v[0] * mat->v[4] * mat->v[8] +
			   mat->v[1] * mat->v[5] * mat->v[6] +
			   mat->v[2] * mat->v[3] * mat->v[7] -
			   mat->v[2] * mat->v[4] * mat->v[6] -
			   mat->v[1] * mat->v[3] * mat->v[8] -
			   mat->v[0] * mat->v[5] * mat->v[7];
		// TODO: case 4: (used in game engines!!)
	default:
		minor = mate__create_matrix(n - 1, n - 1);
		det = 0.0;
		for (k = 0; k < n; k++) {
			for (i = 1; i < n; i++) {
				jm = 0; /* minor column idx */
				for (j = 0; j < n; j++) {
					if (j != k) {
						mate__set_matrix_element(
							minor, i - 1, jm,
							mate__get_matrix_element(mat, i, j));
						jm++;
					}
				}
			}
			tmp =
				mate__get_matrix_element(mat, 0, k) * mate__determinant(minor);
			det = k % 2 == 0 ? det + tmp : det - tmp;
		}
		mate__delete_matrix(minor);
		return det;
	}
	return 0.0; /* dead code */
}

bool mate__inverse_matrix(struct mate__Matrix *res, struct mate__Matrix *mat) {
	ASSERT(mat->M == mat->N);
	ASSERT(res->M == res->N);
	ASSERT(mat->M == res->M);
	const int n = mat->M;
	double det;
	switch (n) {
	case 1:
		res->v[0] = 1.0 / mat->v[0];
		return true;
	case 2:
		det = fabs(mate__determinant(mat));
		if (mate__compare_scalars(det, 0.0, 1e-12)) // TODO: epsilon
			return false;
		res->v[0] = mat->v[3] / det;
		res->v[1] = -mat->v[1] / det;
		res->v[2] = -mat->v[2] / det;
		res->v[3] = mat->v[0] / det;
		return true;
	case 3:
		det = fabs(mate__determinant(mat));
		if (mate__compare_scalars(det, 0.0, 1e-12)) // TODO: epsilon
			return false;
		/* http://mathworld.wolfram.com/MatrixInverse.html
		  11 12 13      0  1  2
		  21 22 23  ->  3  4  5
		  31 32 33      6  7  8  */
		res->v[0] = (mat->v[4] * mat->v[8] - mat->v[5] * mat->v[7]) / det;
		res->v[1] = (mat->v[2] * mat->v[7] - mat->v[1] * mat->v[8]) / det;
		res->v[2] = (mat->v[1] * mat->v[5] - mat->v[2] * mat->v[4]) / det;
		res->v[3] = (mat->v[5] * mat->v[6] - mat->v[3] * mat->v[8]) / det;
		res->v[4] = (mat->v[0] * mat->v[8] - mat->v[2] * mat->v[6]) / det;
		res->v[5] = (mat->v[2] * mat->v[3] - mat->v[0] * mat->v[5]) / det;
		res->v[6] = (mat->v[3] * mat->v[7] - mat->v[4] * mat->v[6]) / det;
		res->v[7] = (mat->v[1] * mat->v[6] - mat->v[0] * mat->v[7]) / det;
		res->v[8] = (mat->v[0] * mat->v[4] - mat->v[1] * mat->v[3]) / det;
		return true;
	case 4:
		det = fabs(mate__determinant(mat));
		if (mate__compare_scalars(det, 0.0, 1e-12)) // TODO: epsilon
			return false;
		/* https://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/fourD/index.htm
		  00 01 02 03      0  1  2  3
		  10 11 12 13  ->  4  5  6  7
		  20 21 22 23      8  9 10 11
		  30 31 32 33     12 13 14 15  */
		res->v[0] = (mat->v[6] * mat->v[11] * mat->v[13] -
					 mat->v[7] * mat->v[10] * mat->v[13] +
					 mat->v[7] * mat->v[9] * mat->v[14] -
					 mat->v[5] * mat->v[11] * mat->v[14] -
					 mat->v[6] * mat->v[9] * mat->v[15] +
					 mat->v[5] * mat->v[10] * mat->v[15]) /
					det;
		res->v[1] = (mat->v[3] * mat->v[10] * mat->v[13] -
					 mat->v[2] * mat->v[11] * mat->v[13] -
					 mat->v[3] * mat->v[9] * mat->v[14] +
					 mat->v[1] * mat->v[11] * mat->v[14] +
					 mat->v[2] * mat->v[9] * mat->v[15] -
					 mat->v[1] * mat->v[10] * mat->v[15]) /
					det;
		res->v[2] = (mat->v[2] * mat->v[7] * mat->v[13] -
					 mat->v[3] * mat->v[6] * mat->v[13] +
					 mat->v[3] * mat->v[5] * mat->v[14] -
					 mat->v[1] * mat->v[7] * mat->v[14] -
					 mat->v[2] * mat->v[5] * mat->v[15] +
					 mat->v[1] * mat->v[6] * mat->v[15]) /
					det;
		res->v[3] = (mat->v[3] * mat->v[6] * mat->v[9] -
					 mat->v[2] * mat->v[7] * mat->v[9] -
					 mat->v[3] * mat->v[5] * mat->v[10] +
					 mat->v[1] * mat->v[7] * mat->v[10] +
					 mat->v[2] * mat->v[5] * mat->v[11] -
					 mat->v[1] * mat->v[6] * mat->v[11]) /
					det;
		res->v[4] = (mat->v[7] * mat->v[10] * mat->v[12] -
					 mat->v[6] * mat->v[11] * mat->v[12] -
					 mat->v[7] * mat->v[8] * mat->v[14] +
					 mat->v[4] * mat->v[11] * mat->v[14] +
					 mat->v[6] * mat->v[8] * mat->v[15] -
					 mat->v[4] * mat->v[10] * mat->v[15]) /
					det;
		res->v[5] = (mat->v[2] * mat->v[11] * mat->v[12] -
					 mat->v[3] * mat->v[10] * mat->v[12] +
					 mat->v[3] * mat->v[8] * mat->v[14] -
					 mat->v[0] * mat->v[11] * mat->v[14] -
					 mat->v[2] * mat->v[8] * mat->v[15] +
					 mat->v[0] * mat->v[10] * mat->v[15]) /
					det;
		res->v[6] = (mat->v[3] * mat->v[6] * mat->v[12] -
					 mat->v[2] * mat->v[7] * mat->v[12] -
					 mat->v[3] * mat->v[4] * mat->v[14] +
					 mat->v[0] * mat->v[7] * mat->v[14] +
					 mat->v[2] * mat->v[4] * mat->v[15] -
					 mat->v[0] * mat->v[6] * mat->v[15]) /
					det;
		res->v[7] = (mat->v[2] * mat->v[7] * mat->v[8] -
					 mat->v[3] * mat->v[6] * mat->v[8] +
					 mat->v[3] * mat->v[4] * mat->v[10] -
					 mat->v[0] * mat->v[7] * mat->v[10] -
					 mat->v[2] * mat->v[4] * mat->v[11] +
					 mat->v[0] * mat->v[6] * mat->v[11]) /
					det;
		res->v[8] = (mat->v[5] * mat->v[11] * mat->v[12] -
					 mat->v[7] * mat->v[9] * mat->v[12] +
					 mat->v[7] * mat->v[8] * mat->v[13] -
					 mat->v[4] * mat->v[11] * mat->v[13] -
					 mat->v[5] * mat->v[8] * mat->v[15] +
					 mat->v[4] * mat->v[9] * mat->v[15]) /
					det;
		res->v[9] = (mat->v[3] * mat->v[9] * mat->v[12] -
					 mat->v[1] * mat->v[11] * mat->v[12] -
					 mat->v[3] * mat->v[8] * mat->v[13] +
					 mat->v[0] * mat->v[11] * mat->v[13] +
					 mat->v[1] * mat->v[8] * mat->v[15] -
					 mat->v[0] * mat->v[9] * mat->v[15]) /
					det;
		res->v[10] = (mat->v[1] * mat->v[7] * mat->v[12] -
					  mat->v[3] * mat->v[5] * mat->v[12] +
					  mat->v[3] * mat->v[4] * mat->v[13] -
					  mat->v[0] * mat->v[7] * mat->v[13] -
					  mat->v[1] * mat->v[4] * mat->v[15] +
					  mat->v[0] * mat->v[5] * mat->v[15]) /
					 det;
		res->v[11] = (mat->v[3] * mat->v[5] * mat->v[8] -
					  mat->v[1] * mat->v[7] * mat->v[8] -
					  mat->v[3] * mat->v[4] * mat->v[9] +
					  mat->v[0] * mat->v[7] * mat->v[9] +
					  mat->v[1] * mat->v[4] * mat->v[11] -
					  mat->v[0] * mat->v[5] * mat->v[11]) /
					 det;
		res->v[12] = (mat->v[6] * mat->v[9] * mat->v[12] -
					  mat->v[5] * mat->v[10] * mat->v[12] -
					  mat->v[6] * mat->v[8] * mat->v[13] +
					  mat->v[4] * mat->v[10] * mat->v[13] +
					  mat->v[5] * mat->v[8] * mat->v[14] -
					  mat->v[4] * mat->v[9] * mat->v[14]) /
					 det;
		res->v[13] = (mat->v[1] * mat->v[10] * mat->v[12] -
					  mat->v[2] * mat->v[9] * mat->v[12] +
					  mat->v[2] * mat->v[8] * mat->v[13] -
					  mat->v[0] * mat->v[10] * mat->v[13] -
					  mat->v[1] * mat->v[8] * mat->v[14] +
					  mat->v[0] * mat->v[9] * mat->v[14]) /
					 det;
		res->v[14] = (mat->v[2] * mat->v[5] * mat->v[12] -
					  mat->v[1] * mat->v[6] * mat->v[12] -
					  mat->v[2] * mat->v[4] * mat->v[13] +
					  mat->v[0] * mat->v[6] * mat->v[13] +
					  mat->v[1] * mat->v[4] * mat->v[14] -
					  mat->v[0] * mat->v[5] * mat->v[14]) /
					 det;
		res->v[15] = (mat->v[1] * mat->v[6] * mat->v[8] -
					  mat->v[2] * mat->v[5] * mat->v[8] +
					  mat->v[2] * mat->v[4] * mat->v[9] -
					  mat->v[0] * mat->v[6] * mat->v[9] -
					  mat->v[1] * mat->v[4] * mat->v[10] +
					  mat->v[0] * mat->v[5] * mat->v[10]) /
					 det;
		return true;
	default:
		ASSERT(false); // unimplemented
	}
	return true;
}

bool mate__compare_matrices(struct mate__Matrix *m0, struct mate__Matrix *m1,
							const double EPSILON) {
	if (m0->M != m1->M)
		return false;
	if (m0->N != m1->N)
		return false;
	double dist = 0.0, tmp;
	for (int i = 0; i < m0->M; i++) {
		for (int j = 0; j < m0->N; j++) {
			tmp = (m0->v[mate__matrix_idx(m0, i, j)] -
				   m1->v[mate__matrix_idx(m1, i, j)]);
			// printf("%f %f %f\n", m0->v[mate__matrix_idx(m0,i,j)],
			// m1->v[mate__matrix_idx(m1,i,j)], tmp);
			dist += tmp * tmp;
		}
	}
	// printf("%f\n", sqrt(dist));
	return sqrt(dist) < EPSILON;
}

void mate__print_matrix(struct mate__Matrix *m) {
	for (int i = 0; i < m->M; i++) {
		printf("row %03d: ", i);
		for (int j = 0; j < m->M; j++) {
			printf("%f ", mate__get_matrix_element(m, i, j));
		}
		printf("\n");
	}
}
