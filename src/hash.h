/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== HASH =============================================================== */

#ifndef __HASH__H
#define __HASH__H

#include <stdbool.h>

#include "list.h"

/* ----- hash tables -------------------------------------------------------- */

// TODO: documentation
struct mate__Hashtable_Item {
	char *id;
	void *data;
};

// TODO: documentation
struct mate__Hashtable {
	int n; /* number of buckets */
	int (*hash_function)(char *str, int n);
	struct mate__List **buckets;
};

// TODO: documentation
struct mate__Hashtable_Iterator {
	bool first; /* indicates initial state */
	bool last;
	int bucket_idx;
	struct mate__Hashtable *hashtable;
	struct mate__List_iterator list_iterator;
};

// TODO
int mate__standard_hash_function(char *str, int n);

/**
 * mate__create_hashtable - creates a hash table
 * @return:        hash table (allocated on heap memeory)
 * @N:             number of buckets of the created hash tables
 * @hash_function: hash function; e.g. "mate__standard_hash_function(..)"
 */
struct mate__Hashtable *
mate__create_hashtable(const int n, int (*hash_function)(char *str, int n));

/**
 * mate__delete_hashtable - deletes a given hashtable
 * @ht:            hash table to be erased
 */
void mate__delete_hashtable(
	struct mate__Hashtable *ht); // TODO: erase data memory!

/**
 * mate__insert_into_hashtable - inserts data into hashtable
 * @return:        new allocated item
 * @ht:            destination hashtable
 * @id:            identifier of the item
 * @data:          item data
 */
struct mate__Hashtable_Item *
mate__insert_into_hashtable(struct mate__Hashtable *ht, char *id, void *data);

/**
 * mate__get_hashtable_item - gets item from hashtable
 * @return:        item or NULL, if not in list
 * @ht:            source hashtable
 * @id:            identifier of the item
 */
void *mate__get_hashtable_item(struct mate__Hashtable *ht, char *id);

// TODO
double mate__get_hashtable_load_factor(struct mate__Hashtable *ht);

// TODO
int mate__get_hashtable_length(struct mate__Hashtable *ht);

// TODO
struct mate__Hashtable_Iterator
mate__create_hashtable_iterator(struct mate__Hashtable *ht);

// TODO
void *mate__get_current_hashtable_iterator_item(
	struct mate__Hashtable_Iterator *iterator);

// TODO
void *mate__get_next_hashtable_iterator_item(
	struct mate__Hashtable_Iterator *iterator);

// TODO: mate__remove_hashtable_item(struct mate__Hashtable* ht, char *id); ->
// need to delete_list and set bucket to NULL, when empty

// TODO: handle same identifier for multiple items???

/* -------------------------------------------------------------------------- */

#endif
