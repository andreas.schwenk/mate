/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== LA := LINEAR ALGEBRA =============================================== */

#ifndef __LA__H
#define __LA__H

#include <stdbool.h>

/* ----- N dimensaional vectors --------------------------------------------- */

// TODO: documentation
struct mate__Vector {
	int N;
	double *v;
};

// TODO: documentation
struct mate__Vector2 {
	union {
		double v[2];
		struct {
			double x, y;
		};
	};
};

// TODO: documentation
struct mate__Vector3 {
	union {
		double v[3];
		struct {
			double x, y, z;
		};
	};
};

/**
 * mate__create_vector - creates a vector with N dimensions
 * @return:        vector (allocated on heap memeory)
 * @N:             number of elements of the created vector
 */
struct mate__Vector *mate__create_vector(const int N);

/**
 * mate__set_vector - sets values to elements of a vector
 * @vec:           vector
 * @...:           values of type double(!) (must match number of vector
 * elements)
 */
void mate__set_vector(struct mate__Vector *vec, ...);

// TODO
void mate__set_vector3(struct mate__Vector3 *vec, double x, double y, double z);

/**
 * mate__delete_vector - deletes a vector
 * @vec:           vector to be deleted
 */
void mate__delete_vector(struct mate__Vector *vec);

/**
 * mate__add_vector - adds two vectors of equal dimension
 * @res:           output vector (lhs + rhs)
 * @lhs:           left-hand side vector
 * @rhs:           right-hand side vector
 */
void mate__add_vectors(struct mate__Vector *res, struct mate__Vector *lhs,
					   struct mate__Vector *rhs);

/**
 * mate__sub_vector - subtracts two vectors of equal dimension
 * @res:           output vector (lhs - rhs)
 * @lhs:           left-hand side vector
 * @rhs:           right-hand side vector
 */
void mate__sub_vectors(struct mate__Vector *res, struct mate__Vector *lhs,
					   struct mate__Vector *rhs);

/**
 * mate__dot - calculates the dot product of two vectors of equal dimension
 * @return:        dot product of lhs and rhs
 * @lhs:           left-hand side vector
 * @rhs:           right-hand side vector
 */
double mate__dot_vectors(struct mate__Vector *lhs, struct mate__Vector *rhs);

/**
 * mate__compare_vector - compares two vectors by dimension and values
 * @return:        true, if  sqrt( sum_{i=0}^{N-1} (v0[i]-v1[i])^2 ) < EPSILON
 * @v0:            first vector
 * @v1:            second vector
 * @EPSILON:       max delta
 */
bool mate__compare_vectors(struct mate__Vector *v0, struct mate__Vector *v1,
						   const double EPSILON);

/* ----- 3 dimensional vectors ---------------------------------------------- */

/**
 * mate__cross - calculates the cross product of two 3-dimensional vector
 * @res:           result := lhs x rhs
 * @lhs:           left-hand side vector
 * @rhs:           right-hand side vector
 */
void mate__cross_vectors(struct mate__Vector *res, struct mate__Vector *lhs,
						 struct mate__Vector *rhs);

/* ----- MxN matrices ------------------------------------------------------- */

// TODO: documentation
struct mate__Matrix {
	int M, N;
	double *v;
};

// TODO: documentation
#define mate__matrix_idx(MAT, I, J) ((I) * (MAT->N) + (J))

// TODO: documentation
#define mate__get_matrix_element(MAT, I, J) MAT->v[mate__matrix_idx(MAT, I, J)]

// TODO: documentation
#define mate__set_matrix_element(MAT, I, J, VAL)                               \
	(MAT->v[mate__matrix_idx(MAT, I, J)] = (VAL))

/**
 * mate__create_matrix - creates a matrix with M rows and N columns
 * @return:        matrix (allocated on heap memeory)
 * @M:             number of rows of the created vector
 * @N:             number of columns of the created vector
 */
struct mate__Matrix *mate__create_matrix(const int M, const int N);

/**
 * mate__set_matrix - sets values to the elements of a matrix
 * @mat:           matrix
 * @...:           values of type double(!) (must match number of matrix
 * elements)
 */
void mate__set_matrix(struct mate__Matrix *mat, ...);

/**
 * mate__set_matrix_row - sets values to the elements of a specific row of a
 * matrix
 * @mat:           matrix
 * @i:             row to set
 * @...:           values of type double(!) (must match number of matrix
 * columns)
 */
void mate__set_matrix_row(struct mate__Matrix *mat, int i, ...);

/**
 * mate__set_matrix_col - sets values to the elements of specific column of a
 * matrix
 * @mat:           matrix
 * @j:             column to set
 * @...:           values of type double(!) (must match number of matrix rows)
 */
void mate__set_matrix_col(struct mate__Matrix *mat, int j, ...);

/**
 * mate__delete_matrix - deletes a matrix
 * @mat:           matrix to be deleted
 */
void mate__delete_matrix(struct mate__Matrix *mat);

/**
 * mate__set_zero_matrix - sets all elements of a matrix to zero
 * @mat:           matrix to be set to [0 ... 0]
 */
void mate__set_zero_matrix(struct mate__Matrix *mat);

/**
 * mate__set_ones_matrix - sets all elements of a matrix to one
 * @mat:           matrix to be set to [1 ... 1]
 */
void mate__set_ones_matrix(struct mate__Matrix *mat);

/**
 * mate__set_identity_matrix - sets the identity matrix
 * @mat:           matrix to be set
 */
void mate__set_identity_matrix(struct mate__Matrix *mat);

/**
 * mate__add_matrices - adds two matrices of equal dimension
 * @res:           output matrix (lhs + rhs)
 * @lhs:           left-hand side matrix
 * @rhs:           right-hand side matrix
 */
void mate__add_matrices(struct mate__Matrix *res, struct mate__Matrix *lhs,
						struct mate__Matrix *rhs);

/**
 * mate__cadd_matrices - compound addition of two matrices
 * @res_lhs:       left-hand side and output matrix (lhs_res += rhs)
 * @rhs:           right-hand side matrix
 */
void mate__cadd_matrices(struct mate__Matrix *lhs_res,
						 struct mate__Matrix *rhs);

/**
 * mate__sub_matrices - subtracts two matrices of equal dimension
 * @res:           output matrix (lhs - rhs)
 * @lhs:           left-hand side matrix
 * @rhs:           right-hand side matrix
 */
void mate__sub_matrices(struct mate__Matrix *res, struct mate__Matrix *lhs,
						struct mate__Matrix *rhs);

/**
 * mate__csub_matrices - compound subtraction of two matrices
 * @res_lhs:       left-hand side and output matrix (lhs_res -= rhs)
 * @rhs:           right-hand side matrix
 */
void mate__csub_matrices(struct mate__Matrix *lhs_res,
						 struct mate__Matrix *rhs);

/**
 * mate__mul_matrices - multiplies two matrices
 * @res:           output matrix (lhs * rhs)
 * @lhs:           left-hand side matrix
 * @rhs:           right-hand side matrix
 */
void mate__mul_matrices(struct mate__Matrix *res, struct mate__Matrix *lhs,
						struct mate__Matrix *rhs);

/**
 * mate__cmul_matrices - compound multiplication of two matrices
 * @res_lhs:       left-hand side and output matrix (lhs_res *= rhs)
 * @rhs:           right-hand side matrix
 */
void mate__cmul_matrices(struct mate__Matrix *lhs_res,
						 struct mate__Matrix *rhs);

/**
 * mate__mul_matrix_vec - multiplies a matrix by a vector
 * @res:           output vector (lhs * rhs)
 * @lhs:           left-hand side matrix
 * @rhs:           right-hand side vector
 */
void mate__mul_matrix_vec(struct mate__Vector *res, struct mate__Matrix *lhs,
						  struct mate__Vector *rhs);

/**
 * mate__cmul_matrix_vec - compund multiplication of a matrix and a vector
 * @lhs:           left-hand side matrix
 * @rhs_res:       right-hand side and output vector
 */
void mate__cmul_matrix_vec(struct mate__Matrix *lhs,
						   struct mate__Vector *rhs_res);

/**
 * mate__transpose_matrix - transposes a matrix, i.e. changes role of rows and
 * columns
 * @dst:           transposed matrix
 * @src:           source matrix
 */
void mate__transpose_matrix(struct mate__Matrix *res, struct mate__Matrix *mat);

// TODO: mat__ctranspose_matrix

/**
 * mate__trace - calculates the trace of a squared matrix (sum of its diagonal
 * elements)
 * @mat:           source matrix
 */
double mate__trace(struct mate__Matrix *mat);

/**
 * mate__determinant - calculates the determinant of a squared matrix
 * @mat:           source matrix
 */
double mate__determinant(struct mate__Matrix *mat);

/**
 * mate__inverse_matrix - calculates the inverse of a squared matrix
 * @res:           inverse matrix
 * @mat:           source matrix
 */
bool mate__inverse_matrix(struct mate__Matrix *res, struct mate__Matrix *mat);

/**
 * mate__compare_matrices - compares two matrices by dimension and values
 * @return:        true, if  sqrt( sum_{i=0}^{M-1} sum_{i=0}^{N-1}
 * (m0[i][j]-m1[i][j])^2 ) < EPSILON
 * @m0:            first matrix
 * @m1:            second matrix
 * @EPSILON:       max delta
 */
bool mate__compare_matrices(struct mate__Matrix *m0, struct mate__Matrix *m1,
							const double EPSILON);

/**
 * mate__print_matrix - print matrix to standard output
 * @m:             source matrix
 */
void mate__print_matrix(struct mate__Matrix *m);

/* -------------------------------------------------------------------------- */

#endif
