/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "list.h"
#include "parse.h"

#include "ca.h"

#define __DEBUG // TODO

char mate__ca_err_msg[2048];

char *mate__ca_expression_str_tmp = NULL;
int mate__ca_expression_str_tmp_size = 1024; // TODO: increase when demanded!!

const char mate__ca_expression_DELIMITERS[] = "+-*/()^";

enum mate__ca_sym_Type {
	mate__ca_sym_Type__VAR = 0,
	mate__ca_sym_Type__UNARY_OPERATOR = 1,
	mate__ca_sym_Type__BINARY_OPERATOR = 2,
	mate__ca_sym_Type__CONST_INT = 3,
	mate__ca_sym_Type__CONST_DOUBLE = 4,
	mate__ca_sym_Type__FUNCTION_CALL = 5
};

enum mate__ca_sym_Functioncode {
	mate__ca_sym_Functioncode__SIN,
	mate__ca_sym_Functioncode__COS,
	mate__ca_sym_Functioncode__EXP,
	mate__ca_sym_Functioncode__TAN,
	mate__ca_sym_Functioncode__COT,
	mate__ca_sym_Functioncode__SQRT,
	mate__ca_sym_Functioncode__ASIN,
	mate__ca_sym_Functioncode__ACOS,
	mate__ca_sym_Functioncode__ATAN,
	mate__ca_sym_Functioncode__ACOT,
	mate__ca_sym_Functioncode__LN,
	mate__ca_sym_Functioncode__LOG2,
	mate__ca_sym_Functioncode__LOG10
};

struct mate__ca_Parse_state {
	struct mate__List_iterator tk_iterator; /* token iterator */
	struct mate__List *ast_stack;			/* abstract syntax tree stack */
};

#define VALID_TK()                                                             \
	(mate__get_current_list_iterator_item(&ps->tk_iterator) != NULL)
#define TK()                                                                   \
	(((struct mate__Token *)mate__get_current_list_iterator_item(              \
		  &ps->tk_iterator))                                                   \
		 ->str)
#define TK___call_by_ref()                                                     \
	(((struct mate__Token *)mate__get_current_list_iterator_item(              \
		  &ps.tk_iterator))                                                    \
		 ->str)
#define CMP_TK(SYM) (VALID_TK() && strcmp(TK(), SYM) == 0)
#define NEXT_TK() mate__get_next_list_iterator_item(&ps->tk_iterator);

bool parse_operand(struct mate__ca_Parse_state *ps);
bool parse_mul(struct mate__ca_Parse_state *ps);
bool parse_add(struct mate__ca_Parse_state *ps);

struct mate__ca_Sym *create_sym(int tk_type, char *tk_str, int64_t *val_int,
								double *val_double) {
	struct mate__ca_Sym *new_sym = ALLOC(struct mate__ca_Sym, 1);
	new_sym->deriv = NULL;
	new_sym->type = tk_type;
	new_sym->str = ALLOC(char, strlen(tk_str + 1));
	strcpy(new_sym->str, tk_str);
	if (val_int != NULL)
		new_sym->val.val_int = *val_int;
	else if (val_double != NULL)
		new_sym->val.val_double = *val_double;
	new_sym->children = NULL;
	new_sym->num_children = 0;
	new_sym->flag = false;
	return new_sym;
}

void create_sym__alloc_children(struct mate__ca_Sym *sym,
								const int number_of_children) {
	sym->num_children = number_of_children;
	sym->children = ALLOC(struct mate__ca_Sym *, number_of_children);
}

void create_sym__set_child(struct mate__ca_Sym *sym, const int index,
						   struct mate__ca_Sym *child) {
	ASSERT(index >= 0);
	sym->children[index] = child;
}

bool delete_sym_nonrecursive(struct mate__ca_Sym *sym) {
	if (sym == NULL)
		return false;
	if (sym->str != NULL)
		free(sym->str);
	if (sym->children != NULL)
		free(sym->children);
	free(sym);
	return true;
}

bool parse_operand(struct mate__ca_Parse_state *ps) {
/* <operand> ::= TODO */
#define VARNAME_MAX_LEN 64
	char var_fct_name[VARNAME_MAX_LEN];

	if (CMP_TK("(")) {
		/* ----- bracketed expression ----- */
		NEXT_TK();
		if (parse_add(ps) == false)
			return false;
		if (CMP_TK(")")) {
			NEXT_TK();
		} else {
			sprintf(mate__ca_err_msg,
					"Error parsing expression: expected ')'\n");
			return false;
		}
	} else if (VALID_TK() && mate__is_integer(TK())) {
/* ----- integer constant ----- */
#ifdef __DEBUG
		printf("found integer constant %s\n", TK());
#endif
		int64_t val_int = atoi(TK());
		struct mate__ca_Sym *new_sym =
			create_sym(mate__ca_sym_Type__CONST_INT, TK(), &val_int, NULL);
		mate__push_to_list(ps->ast_stack, new_sym);
		NEXT_TK();
	} else if (VALID_TK() && mate__is_double(TK())) {
/* ----- double constant ----- */
#ifdef __DEBUG
		printf("found double constant %s\n", TK());
#endif
		double val_double = atof(TK());
		struct mate__ca_Sym *new_sym = create_sym(
			mate__ca_sym_Type__CONST_DOUBLE, TK(), NULL, &val_double);
		mate__push_to_list(ps->ast_stack, new_sym);
		NEXT_TK();
	} else if (VALID_TK() && mate__is_identifier(TK())) {
		/* ----- variable | function ----- */
		ASSERT(strlen(TK()) < VARNAME_MAX_LEN);
		strcpy(var_fct_name, TK());
		NEXT_TK();
		if (CMP_TK("(")) {
			NEXT_TK();
			/* --- function call --- */
			int64_t function_code;
			if (strcmp(var_fct_name, "sin") == 0)
				function_code = mate__ca_sym_Functioncode__SIN;
			else if (strcmp(var_fct_name, "cos") == 0)
				function_code = mate__ca_sym_Functioncode__COS;
			else if (strcmp(var_fct_name, "exp") == 0)
				function_code = mate__ca_sym_Functioncode__EXP;
			else if (strcmp(var_fct_name, "tan") == 0)
				function_code = mate__ca_sym_Functioncode__TAN;
			else if (strcmp(var_fct_name, "cot") == 0)
				function_code = mate__ca_sym_Functioncode__COT;
			else if (strcmp(var_fct_name, "sqrt") == 0)
				function_code = mate__ca_sym_Functioncode__SQRT;
			else if (strcmp(var_fct_name, "asin") == 0)
				function_code = mate__ca_sym_Functioncode__ASIN;
			else if (strcmp(var_fct_name, "acos") == 0)
				function_code = mate__ca_sym_Functioncode__ACOS;
			else if (strcmp(var_fct_name, "atan") == 0)
				function_code = mate__ca_sym_Functioncode__ATAN;
			else if (strcmp(var_fct_name, "acot") == 0)
				function_code = mate__ca_sym_Functioncode__ACOT;
			else if (strcmp(var_fct_name, "ln") == 0)
				function_code = mate__ca_sym_Functioncode__LN;
			else if (strcmp(var_fct_name, "log2") == 0)
				function_code = mate__ca_sym_Functioncode__LOG2;
			else if (strcmp(var_fct_name, "log10") == 0)
				function_code = mate__ca_sym_Functioncode__LOG10;
			else {
				sprintf(mate__ca_err_msg,
						"Error parsing expression: unknown function '%s'\n",
						TK());
				return false;
			}
#ifdef __DEBUG
			printf("found function %s\n", var_fct_name);
#endif
			if (parse_add(ps) == false)
				return false;
			struct mate__ca_Sym *tos = mate__pop_from_list(ps->ast_stack);
			struct mate__ca_Sym *new_sym =
				create_sym(mate__ca_sym_Type__FUNCTION_CALL, var_fct_name,
						   &function_code, NULL);
			create_sym__alloc_children(new_sym, 1);
			create_sym__set_child(new_sym, 0, tos);
			mate__push_to_list(ps->ast_stack, new_sym);
			if (CMP_TK(")")) {
				NEXT_TK()
			} else {
				sprintf(mate__ca_err_msg,
						"Error parsing expression: expected ')'\n");
				return false;
			}
		} else {
/* --- variable --- */
#ifdef __DEBUG
			printf("found variable %s\n", var_fct_name);
#endif
			struct mate__ca_Sym *new_sym =
				create_sym(mate__ca_sym_Type__VAR, var_fct_name, NULL, NULL);
			mate__push_to_list(ps->ast_stack, new_sym);
		}
	} else {
		/* ----- error ----- */
		if (VALID_TK())
			sprintf(mate__ca_err_msg,
					"Error parsing expression: unexpected token %s\n", TK());
		else
			sprintf(mate__ca_err_msg,
					"Error parsing expression: no token left %s\n", TK());
		return false;
	}

	return true;
}

bool parse_pow(struct mate__ca_Parse_state *ps) {
	/* <pow> ::= TODO */
	if (parse_operand(ps) == false)
		return false;
	while (CMP_TK("^")) {
#ifdef __DEBUG
		printf("found '1'\n");
#endif
		NEXT_TK();
		if (parse_operand(ps) == false)
			return false;

		struct mate__ca_Sym *rhs = mate__pop_from_list(ps->ast_stack);
		struct mate__ca_Sym *lhs = mate__pop_from_list(ps->ast_stack);

		struct mate__ca_Sym *new_sym =
			create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "^", NULL, NULL);
		create_sym__alloc_children(new_sym, 2);
		create_sym__set_child(new_sym, 0, lhs);
		create_sym__set_child(new_sym, 1, rhs);
		mate__push_to_list(ps->ast_stack, new_sym);
	}

	return true;
}

bool parse_mul(struct mate__ca_Parse_state *ps) {
	/* <mul> ::= TODO */
	if (parse_pow(ps) == false)
		return false;
	while (CMP_TK("*") || CMP_TK("/")) {
		char operator= TK()[0];
#ifdef __DEBUG
		printf("found '%c'\n", operator);
#endif
		NEXT_TK();
		if (parse_pow(ps) == false)
			return false;

		struct mate__ca_Sym *rhs = mate__pop_from_list(ps->ast_stack);
		struct mate__ca_Sym *lhs = mate__pop_from_list(ps->ast_stack);

		struct mate__ca_Sym *new_sym = create_sym(
			mate__ca_sym_Type__BINARY_OPERATOR, operator== '*' ? "*" : "/",
			NULL, NULL);
		create_sym__alloc_children(new_sym, 2);
		create_sym__set_child(new_sym, 0, lhs);
		create_sym__set_child(new_sym, 1, rhs);
		mate__push_to_list(ps->ast_stack, new_sym);
	}

	return true;
}

bool parse_add(struct mate__ca_Parse_state *ps) {
	/* <add> ::= TODO */
	if (parse_mul(ps) == false)
		return false;
	while (CMP_TK("+") || CMP_TK("-")) {
		char operator= TK()[0];
#ifdef __DEBUG
		printf("found '%c'\n", operator);
#endif
		NEXT_TK();
		if (parse_mul(ps) == false)
			return false;

		ASSERT(mate__get_list_length(ps->ast_stack) >= 2);

		struct mate__ca_Sym *rhs = mate__pop_from_list(ps->ast_stack);
		struct mate__ca_Sym *lhs = mate__pop_from_list(ps->ast_stack);

		struct mate__ca_Sym *new_sym = create_sym(
			mate__ca_sym_Type__BINARY_OPERATOR, operator== '+' ? "+" : "-",
			NULL, NULL);
		create_sym__alloc_children(new_sym, 2);
		create_sym__set_child(new_sym, 0, lhs);
		create_sym__set_child(new_sym, 1, rhs);
		mate__push_to_list(ps->ast_stack, new_sym);
	}

	return true;
}

int get_node_count(struct mate__ca_Sym *sym) {
	if (sym == NULL)
		return 0;
	int cnt = 1; /* this */
	for (int i = 0; i < sym->num_children; i++)
		cnt += get_node_count(sym->children[i]);
	return cnt;
}

void update_polish_notation_rec(struct mate__ca_Expression *expression,
								struct mate__ca_Sym *sym) {
	expression->polish[expression->polish_len++] = sym;
	for (int i = 0; i < sym->num_children; i++)
		update_polish_notation_rec(expression, sym->children[i]);
}

void update_polish_notation(struct mate__ca_Expression *expression) {
	if (expression->polish != NULL) {
		free(expression->polish);
		expression->polish = NULL;
		expression->polish_len = 0;
	}
	const int n = get_node_count(expression->tree);
	expression->polish = ALLOC(struct mate__ca_Sym *, n);
	expression->polish_len = 0;
	update_polish_notation_rec(expression, expression->tree);
}

struct mate__ca_Expression *mate__ca_create_expression(const char *exp_str) {

	struct mate__List *tk_list =
		mate__tokenize(exp_str, mate__ca_expression_DELIMITERS);

	mate__print_tokenlist(tk_list);

	struct mate__ca_Parse_state ps;
	ps.tk_iterator = mate__create_list_iterator(tk_list);
	mate__get_next_list_iterator_item(&ps.tk_iterator);
	ps.ast_stack = mate__create_list();

	if (parse_add(&ps) == false) {
		mate__delete_list(ps.ast_stack, false,
						  NULL); // TODO: pass delete function!
		return NULL;
	}

	if (mate__get_current_list_iterator_item(&ps.tk_iterator) != NULL ||
		mate__get_list_length(ps.ast_stack) != 1) {
		sprintf(mate__ca_err_msg,
				"Error parsing expression: unexpected symbol '%s'",
				TK___call_by_ref());
		mate__delete_list(ps.ast_stack, false,
						  NULL); // TODO: pass delete function!
		return NULL;
	}

	struct mate__ca_Sym *root =
		(struct mate__ca_Sym *)ps.ast_stack->first->data;

	mate__delete_list(ps.ast_stack, false, NULL); // TODO: pass delete function!

	struct mate__ca_Expression *expression =
		ALLOC(struct mate__ca_Expression, 1);
	expression->tree = root;
	expression->polish = NULL;
	expression->polish_len = 0;

	/*update_polish_notation(expression);*/
	mate__ca_optimize_expression(expression);

	return expression;
}

// TODO: remove:
/*void delete_unused_symbols_recursively(struct mate__ca_Sym *sym, const struct
mate__ca_Sym **polish, const int polish_len) { for(int i=0; i<sym->num_children;
i++) { struct mate__ca_Sym *child = sym->children[i];
	delete_unused_symbols_recursively(child, polish, polish_len);
  }
}

// TODO: remove:
/ **
 * frees all symbols from given polish notation list that are no longer in use
 * @sym:           current root symbol
 * @polish:        polish notation list
 * @polish_len:    number of nodes on polish notation list
 * /
void delete_unused_symbols(struct mate__ca_Sym *root, const struct mate__ca_Sym
**polish, const int polish_len) { ASSERT(sym != NULL);
  delete_unused_symbols_recursively(root, polish, polish_len);
  x
}*/

bool derivate_rec(struct mate__ca_Sym *sym, const char *var_name,
				  struct mate__List *all_syms) {

	// TODO: memory leaks, if created children or symbols from the original
	// expression are not mounted in the final derivated expression!!

	const int n = sym->num_children;
	int64_t const0 = 0;
	int64_t const1 = 1;
	int64_t const2 = 2;
	double const_double;
	int64_t fct_code;
	/* ----- no children ->  x' = 1  or  const' = 0  ----- */
	if (n == 0) {
		switch (sym->type) {
		case mate__ca_sym_Type__VAR:
			if (strcmp(sym->str, var_name) == 0) {
				sym->deriv = create_sym(mate__ca_sym_Type__CONST_INT, "1",
										&const1, NULL);
				mate__push_to_list(all_syms, sym->deriv);
			} else {
				sym->deriv = create_sym(mate__ca_sym_Type__CONST_INT, "0",
										&const0, NULL);
				mate__push_to_list(all_syms, sym->deriv);
			}
			break;
		case mate__ca_sym_Type__CONST_INT:
		case mate__ca_sym_Type__CONST_DOUBLE:
			sym->deriv =
				create_sym(mate__ca_sym_Type__CONST_INT, "0", &const0, NULL);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		default:
			ASSERT(false); /* unimplemented */
			break;
		}
		return true;
	}
	/* ----- derivate recursively ----- */
	for (int i = 0; i < n; i++) {
		struct mate__ca_Sym *child = sym->children[i];
		if (derivate_rec(child, var_name, all_syms) == false)
			return false;
	}
	struct mate__ca_Sym *c1, *c2;				/* child symbols */
	struct mate__ca_Sym *a, *b, *c, *d, *e, *f; /* temporary symbols */
	char operator;
	switch (sym->type) {
	case mate__ca_sym_Type__UNARY_OPERATOR:
		operator= sym->str[0];
		switch (operator) {
		case '-':
			/* ----- ( -c1 )' := -( c1' ) ----- */
			c1 = sym->children[0];
			sym->deriv =
				create_sym(mate__ca_sym_Type__UNARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 1);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		default:
			ASSERT(false); /* unimplemented */
		}
		break;
	case mate__ca_sym_Type__BINARY_OPERATOR:
		operator= sym->str[0];
		c1 = sym->children[0];
		c2 = sym->children[1];
		switch (operator) {
		case '+':
		case '-':
			/* ----- ( c1+c2 )' := c1' + c2' ----- */
			sym->deriv = create_sym(mate__ca_sym_Type__BINARY_OPERATOR,
									sym->str, NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			create_sym__set_child(sym->deriv, 1, c2->deriv);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case '*':
			/* ----- ( c1*c2 )' :=  c1' * c2  +  c1 * c2' ----- */
			/*                      <---a-->     <--b--->       */
			a = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(a, 2);
			create_sym__set_child(a, 0, c1->deriv);
			create_sym__set_child(a, 1, c2);
			b = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(b, 2);
			create_sym__set_child(b, 0, c1);
			create_sym__set_child(b, 1, c2->deriv);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "+", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, b);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case '/':
			/* ----- ( c1/c2 )' := (c1' * c2 - c1 * c2') / (c2 * c2) --- */
			/*                      <---a-->   <--b--->    <--c---->     */
			/*                      <--------d-------->                  */
			a = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(a, 2);
			create_sym__set_child(a, 0, c1->deriv);
			create_sym__set_child(a, 1, c2);
			b = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(b, 2);
			create_sym__set_child(b, 0, c1);
			create_sym__set_child(b, 1, c2->deriv);
			c = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(c, 2);
			create_sym__set_child(c, 0, c2);
			create_sym__set_child(c, 1, c2);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, a);
			create_sym__set_child(d, 1, b);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, d);
			create_sym__set_child(sym->deriv, 1, c);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case '^':
			/* ----- ( c1^c2 )' := c1' * c2 * c1^(c2 - 1) --- */
			/*                     <---a-->            b      */
			/*                                   <--c--->     */
			/*                                <----d---->     */
			a = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(a, 2);
			create_sym__set_child(a, 0, c1->deriv);
			create_sym__set_child(a, 1, c2);
			b = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			c = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(c, 2);
			create_sym__set_child(c, 0, c2);
			create_sym__set_child(c, 1, b);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "^", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, c1);
			create_sym__set_child(d, 1, c);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, d);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		default:
			ASSERT(false); /* unimplemented */
		}
		break;
	case mate__ca_sym_Type__FUNCTION_CALL:
		switch (sym->val.function_code) {
		case mate__ca_sym_Functioncode__SIN:
			/* ----- ( sin(c1) )' := c1' * cos( c1 ) --- */
			/*                             <--a---->     */
			c1 = sym->children[0];
			fct_code = mate__ca_sym_Functioncode__COS;
			a = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "cos", &fct_code,
						   NULL);
			create_sym__alloc_children(a, 1);
			create_sym__set_child(a, 0, c1);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			create_sym__set_child(sym->deriv, 1, a);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__COS:
			/* ----- ( cos(c1) )' := - c1' * sin( c1 ) --- */
			/*                       <-a->   <--b---->     */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__UNARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(a, 1);
			create_sym__set_child(a, 0, c1->deriv);
			fct_code = mate__ca_sym_Functioncode__SIN;
			b = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "sin", &fct_code,
						   NULL);
			create_sym__alloc_children(b, 1);
			create_sym__set_child(b, 0, c1);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, b);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__EXP:
			/* ----- ( exp(c1) )' := c1' * exp( c1 ) --- */
			/*                             <--a---->     */
			c1 = sym->children[0];
			fct_code = mate__ca_sym_Functioncode__EXP;
			a = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "exp", &fct_code,
						   NULL);
			create_sym__alloc_children(a, 1);
			create_sym__set_child(a, 0, c1);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			create_sym__set_child(sym->deriv, 1, a);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__TAN:
			/* ----- ( tan(c1) )' := 1 + tan(c1)^2 --- */
			/*                       a   <--b--> c     */
			/*                           <---d--->     */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			fct_code = mate__ca_sym_Functioncode__TAN;
			b = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "tan", &fct_code,
						   NULL);
			create_sym__alloc_children(b, 1);
			create_sym__set_child(b, 0, c1);
			c = create_sym(mate__ca_sym_Type__CONST_INT, "2", &const2, NULL);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "^", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "+", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, d);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__COT:
			/* ----- ( cot(c1) )' := - (1 + cot(c1)^2 ) --- */
			/*                          a   <--b--> c       */
			/*                              <---d--->       */
			/*                         <------e------->     */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			fct_code = mate__ca_sym_Functioncode__COT;
			b = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "cot", &fct_code,
						   NULL);
			create_sym__alloc_children(b, 1);
			create_sym__set_child(b, 0, c1);
			c = create_sym(mate__ca_sym_Type__CONST_INT, "2", &const2, NULL);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "^", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			e = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "+", NULL, NULL);
			create_sym__alloc_children(e, 2);
			create_sym__set_child(e, 0, a);
			create_sym__set_child(e, 1, d);
			sym->deriv =
				create_sym(mate__ca_sym_Type__UNARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 1);
			create_sym__set_child(sym->deriv, 0, e);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, e);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__SQRT:
			/* ----- ( sqrt(c1) )' := 1 / (2 * sqrt(c1)) --- */
			/*                        a    b   <--c--->      */
			/*                            <------d----->     */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			b = create_sym(mate__ca_sym_Type__CONST_INT, "2", &const2, NULL);
			fct_code = mate__ca_sym_Functioncode__SQRT;
			c = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "sqrt", &fct_code,
						   NULL);
			create_sym__alloc_children(c, 1);
			create_sym__set_child(c, 0, c1);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, d);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__ASIN:
			/* ----- ( asin(c1) )' := 1 / sqrt(1 - c1^2) --- */
			/*                        a        b   <-c>      */
			/*                                 <--d--->      */
			/*                            <-----e----->      */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			b = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			c = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(c, 2);
			create_sym__set_child(c, 0, c1);
			create_sym__set_child(c, 1, c1);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			fct_code = mate__ca_sym_Functioncode__SQRT;
			e = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "sqrt", &fct_code,
						   NULL);
			create_sym__alloc_children(e, 1);
			create_sym__set_child(e, 0, d);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, e);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, e);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__ACOS:
			/* ----- ( acos(c1) )' := -( 1 / sqrt(1 - c1^2) ) --- */
			/*                           a        b   <-c>        */
			/*                                    <---d-->        */
			/*                               <-----e------>       */
			/*                           <-------f-------->       */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			b = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			c = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(c, 2);
			create_sym__set_child(c, 0, c1);
			create_sym__set_child(c, 1, c1);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			fct_code = mate__ca_sym_Functioncode__SQRT;
			e = create_sym(mate__ca_sym_Type__FUNCTION_CALL, "sqrt", &fct_code,
						   NULL);
			create_sym__alloc_children(e, 1);
			create_sym__set_child(e, 0, d);
			f = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(f, 2);
			create_sym__set_child(f, 0, a);
			create_sym__set_child(f, 1, e);
			sym->deriv =
				create_sym(mate__ca_sym_Type__UNARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 1);
			create_sym__set_child(sym->deriv, 0, f);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, e);
			mate__push_to_list(all_syms, f);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__ATAN:
			/* ----- ( atan(c1) )' := 1 / (1 + c1^2) --- */
			/*                        a    b   <-c>      */
			/*                             <---d-->      */
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			b = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			c = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(c, 2);
			create_sym__set_child(c, 0, c1);
			create_sym__set_child(c, 1, c1);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "+", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, a);
			create_sym__set_child(sym->deriv, 1, d);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__ACOT:
			/* ----- ( acot(c1) )' := -( 1 / (1 + c1^2) ) --- */
			/*                           a    b   <-c>        */
			/*                                <---d-->        */
			/*                           <-----e----->        */
			c1 = sym->children[0];
			c1 = sym->children[0];
			a = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			b = create_sym(mate__ca_sym_Type__CONST_INT, "1", &const1, NULL);
			c = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(c, 2);
			create_sym__set_child(c, 0, c1);
			create_sym__set_child(c, 1, c1);
			d = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "+", NULL, NULL);
			create_sym__alloc_children(d, 2);
			create_sym__set_child(d, 0, b);
			create_sym__set_child(d, 1, c);
			e = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(e, 2);
			create_sym__set_child(e, 0, a);
			create_sym__set_child(e, 1, d);
			sym->deriv =
				create_sym(mate__ca_sym_Type__UNARY_OPERATOR, "-", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 1);
			create_sym__set_child(sym->deriv, 0, e);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, c);
			mate__push_to_list(all_syms, d);
			mate__push_to_list(all_syms, e);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__LN:
			/* ----- ( ln(c1) )' := c1' / c1 --- */
			c1 = sym->children[0];
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			create_sym__set_child(sym->deriv, 1, c1);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__LOG2:
			/* ----- ( log2(c1) )' := c1' / (c1 * ln(2)) --- */
			/*                                    <-a->      */
			/*                              <----b----->     */
			c1 = sym->children[0];
			const_double = 0.693147180559945; /* ln(2) */
			a = create_sym(mate__ca_sym_Type__CONST_DOUBLE, "0.693147180559945",
						   NULL, &const_double);
			b = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(b, 2);
			create_sym__set_child(b, 0, c1);
			create_sym__set_child(b, 1, a);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			create_sym__set_child(sym->deriv, 1, b);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		case mate__ca_sym_Functioncode__LOG10:
			/* ----- ( log10(c1) )' := c1' / (c1 * ln(10)) --- */
			/*                                     <-a-->      */
			/*                               <-----b----->     */
			c1 = sym->children[0];
			const_double = 2.302585092994046; /* ln(10) */
			a = create_sym(mate__ca_sym_Type__CONST_DOUBLE, "2.302585092994046",
						   NULL, &const_double);
			b = create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "*", NULL, NULL);
			create_sym__alloc_children(b, 2);
			create_sym__set_child(b, 0, c1);
			create_sym__set_child(b, 1, a);
			sym->deriv =
				create_sym(mate__ca_sym_Type__BINARY_OPERATOR, "/", NULL, NULL);
			create_sym__alloc_children(sym->deriv, 2);
			create_sym__set_child(sym->deriv, 0, c1->deriv);
			create_sym__set_child(sym->deriv, 1, b);
			mate__push_to_list(all_syms, a);
			mate__push_to_list(all_syms, b);
			mate__push_to_list(all_syms, sym->deriv);
			break;
		default:
			ASSERT(false); /* unimplemented */
		}
		break;
	default:
		ASSERT(false); /* unimplemented */
	}

	return true;
}

bool mate__ca_derivate_expression(struct mate__ca_Expression *expression,
								  const char *variable_name) {

	if (expression->tree == NULL)
		return NULL;

	/* ----- create list of all symbols that arise in the derivation process
	 * -----
	 */
	struct mate__List *all_syms = mate__create_list();

	/* ----- flag all original symbols as unused ----- */
	ASSERT(expression->polish != NULL);
	for (int i = 0; i < expression->polish_len; i++) {
		struct mate__ca_Sym *sym = expression->polish[i];
		sym->flag = false;
		mate__push_to_list(all_syms, sym);
	}

	/* ----- fast-track (esp. useful for sparse matrices): check, if
	 * variable_name is unused ----- */
	bool used = false;
	for (int i = 0; i < expression->polish_len; i++) {
		struct mate__ca_Sym *sym = expression->polish[i];
		if (sym->type == mate__ca_sym_Type__VAR &&
			strcmp(sym->str, variable_name) == 0) {
			used = true;
			break;
		}
	}
	if (used == false) {
		// TODO: free "tree" memory (children that become unused)!!
		int64_t const0 = 0;
		expression->tree =
			create_sym(mate__ca_sym_Type__CONST_INT, "0", &const0, NULL);
		update_polish_notation(expression);
		return true;
	}

	/* ----- derivation ----- */

	if (derivate_rec(expression->tree, variable_name, all_syms) == false)
		return false; // TODO: DEALLOC ANYTHING??

	expression->tree = expression->tree->deriv;

	/* ----- update polish notation ----- */
	update_polish_notation(expression);

	/* ----- free unused memory ----- */
	/* (a) flag current symbols as used ----- */
	ASSERT(expression->polish != NULL);
	for (int i = 0; i < expression->polish_len; i++) {
		struct mate__ca_Sym *sym = expression->polish[i];
		sym->flag = true;
	}
	/* (b) delete all unused symbols */
	struct mate__List_iterator it = mate__create_list_iterator(all_syms);
	struct mate__ca_Sym *sym;
	while ((sym = mate__get_next_list_iterator_item(&it)) != NULL) {
		if (sym->flag == false)
			delete_sym_nonrecursive(sym);
	}
	mate__delete_list(all_syms, false, NULL);

	/* ----- optimize expression ----- */
	mate__ca_optimize_expression(expression);

	return true;
}

void optimize_rec(struct mate__ca_Sym **sym) {
	if (mate__ca_expression_str_tmp == NULL)
		mate__ca_expression_str_tmp =
			ALLOC(char, mate__ca_expression_str_tmp_size);

	const int n = (*sym)->num_children;
	int64_t fct_code, res_int;
	double res_double, res_double_lhs, res_double_rhs;
	/* ----- no children -> nothing to optimize ----- */
	if (n == 0)
		return;
	/* ----- optimize recursively ----- */
	for (int i = 0; i < n; i++) {
		struct mate__ca_Sym *child = (*sym)->children[i];
		optimize_rec(&child);
		(*sym)->children[i] =
			child; /* optimize_rec(..) can mount another/new child */
	}

	struct mate__ca_Sym *c1, *c2;				/* child symbols */
	struct mate__ca_Sym *a, *b, *c, *d, *e, *f; /* temporary symbols */
	char operator;
	switch ((*sym)->type) {
	case mate__ca_sym_Type__UNARY_OPERATOR:
		operator=(*sym)->str[0];
		c1 = (*sym)->children[0];
		switch (operator) {
		case '-':
			/* ----- -(0) := 0 ----- */ // TODO: double approx 0
			if (c1->type == mate__ca_sym_Type__CONST_INT &&
				c1->val.val_int == 0) {
				delete_sym_nonrecursive(*sym);
				*sym = c1;
			}
			break;
		}
		break;
	case mate__ca_sym_Type__BINARY_OPERATOR:
		operator=(*sym)->str[0];
		c1 = (*sym)->children[0];
		c2 = (*sym)->children[1];
		switch (operator) {
		case '+':
		case '-':
			/* ----- const_int(c1) (+|-) const_int(c2) := const_int(c1 (+|-) c2)
			 * -----
			 */
			if (c1->type == mate__ca_sym_Type__CONST_INT &&
				c2->type == mate__ca_sym_Type__CONST_INT) {
				res_int = operator== '+' ? c1->val.val_int + c2->val.val_int
										 : c1->val.val_int - c2->val.val_int;
				sprintf(mate__ca_expression_str_tmp, "%lld", res_int);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = create_sym(mate__ca_sym_Type__CONST_INT,
								  mate__ca_expression_str_tmp, &res_int, NULL);
			}
			/* ----- const(c1) (+|-) const(c2) := const_double(c1 (+|-) c2)
			   ----- */
			else if (((c1->type == mate__ca_sym_Type__CONST_INT) ||
					  (c1->type == mate__ca_sym_Type__CONST_DOUBLE)) &&
					 ((c2->type == mate__ca_sym_Type__CONST_INT) ||
					  (c2->type == mate__ca_sym_Type__CONST_DOUBLE))) {
				res_double_lhs = c1->type == mate__ca_sym_Type__CONST_INT
									 ? c1->val.val_int
									 : c1->val.val_double;
				res_double_rhs = c2->type == mate__ca_sym_Type__CONST_INT
									 ? c2->val.val_int
									 : c2->val.val_double;
				res_double = operator== '+' ? res_double_lhs + res_double_rhs
											: res_double_lhs - res_double_rhs;
				sprintf(mate__ca_expression_str_tmp, "%f", res_double);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym =
					create_sym(mate__ca_sym_Type__CONST_DOUBLE,
							   mate__ca_expression_str_tmp, NULL, &res_double);
			}
			/* ----- c1 (+|-) 0 := c1 ----- */ // TODO: double approx 0
			else if (c2->type == mate__ca_sym_Type__CONST_INT &&
					 c2->val.val_int == 0) {
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = c1;
			}
			/* ----- 0 + c2 := c2 ----- */ // TODO: double approx 0
			else if (operator== '+' &&
					 c1->type == mate__ca_sym_Type__CONST_INT &&
					 c1->val.val_int == 0) {
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(*sym);
				*sym = c2;
			}
			break;
		case '*':
		case '/':
			/* ----- const_int(c1) * const_int(c2) := const_int(c1*c2) ----- */
			if (operator== '*' && c1->type == mate__ca_sym_Type__CONST_INT &&
				c2->type == mate__ca_sym_Type__CONST_INT) {
				res_int = c1->val.val_int * c2->val.val_int;
				sprintf(mate__ca_expression_str_tmp, "%lld", res_int);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = create_sym(mate__ca_sym_Type__CONST_INT,
								  mate__ca_expression_str_tmp, &res_int, NULL);
			}
			/* ----- const_int(c1) / const_int(c2) := const_int(c1/c2), if c1/c2
			   has no remainder ----- */
			else if (operator== '/' &&
					 c1->type == mate__ca_sym_Type__CONST_INT && c2->type ==
					 mate__ca_sym_Type__CONST_INT &&(c1->val.val_int %
													 c2->val.val_int) == 0) {
				res_int = c1->val.val_int / c2->val.val_int;
				sprintf(mate__ca_expression_str_tmp, "%lld", res_int);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = create_sym(mate__ca_sym_Type__CONST_INT,
								  mate__ca_expression_str_tmp, &res_int, NULL);
			}
			/* ----- const(c1) (*|/) const(c2) := const_double(c1 (*|/) c2)
			   ----- */
			else if (((c1->type == mate__ca_sym_Type__CONST_INT) ||
					  (c1->type == mate__ca_sym_Type__CONST_DOUBLE)) &&
					 ((c2->type == mate__ca_sym_Type__CONST_INT) ||
					  (c2->type == mate__ca_sym_Type__CONST_DOUBLE))) {
				res_double_lhs = c1->type == mate__ca_sym_Type__CONST_INT
									 ? c1->val.val_int
									 : c1->val.val_double;
				res_double_rhs = c2->type == mate__ca_sym_Type__CONST_INT
									 ? c2->val.val_int
									 : c2->val.val_double;
				res_double = operator== '*' ? res_double_lhs * res_double_rhs
											: res_double_lhs / res_double_rhs;
				sprintf(mate__ca_expression_str_tmp, "%f", res_double);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym =
					create_sym(mate__ca_sym_Type__CONST_DOUBLE,
							   mate__ca_expression_str_tmp, NULL, &res_double);
			}
			/* ----- c1 (*|/) 1 := c1 ----- */ // TODO: double approx 1
			else if (c2->type == mate__ca_sym_Type__CONST_INT &&
					 c2->val.val_int == 1) {
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = c1;
			}
			/* ----- 1 * c2 := c2 ----- */ // TODO: double approx 1
			else if (operator== '*' &&
					 c1->type == mate__ca_sym_Type__CONST_INT &&
					 c1->val.val_int == 1) {
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(*sym);
				*sym = c2;
			}
			/* ----- c1 * 0 := 0 ----- */ // TODO: double approx 0
			else if (c2->type == mate__ca_sym_Type__CONST_INT &&
					 c2->val.val_int == 0) {
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(*sym);
				*sym = c2;
			}
			/* ----- 0 (*|/) c2 := 0 ----- */ // TODO: double approx 0
			else if (c1->type == mate__ca_sym_Type__CONST_INT &&
					 c1->val.val_int == 0) {
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = c1;
			}
			break;
		case '^':
			/* ----- c1^1 := c1 ----- */ // TODO: double approx 1
			if (c2->type == mate__ca_sym_Type__CONST_INT &&
				c2->val.val_int == 1) {
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = c1;
			}
			/* ----- const_int(c1)^const_int(c2) := const_int(c1^c2) ----- */
			else if (c1->type == mate__ca_sym_Type__CONST_INT &&
					 c2->type == mate__ca_sym_Type__CONST_INT) {
				res_int = mate__pow_int(c1->val.val_int, c2->val.val_int);
				sprintf(mate__ca_expression_str_tmp, "%lld", res_int);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym = create_sym(mate__ca_sym_Type__CONST_INT,
								  mate__ca_expression_str_tmp, &res_int, NULL);
			}
			/* ----- const(c1)^const(c2) := const_double(c1^c2) ----- */
			else if (((c1->type == mate__ca_sym_Type__CONST_INT) ||
					  (c1->type == mate__ca_sym_Type__CONST_DOUBLE)) &&
					 ((c2->type == mate__ca_sym_Type__CONST_INT) ||
					  (c2->type == mate__ca_sym_Type__CONST_DOUBLE))) {
				res_double_lhs = c1->type == mate__ca_sym_Type__CONST_INT
									 ? c1->val.val_int
									 : c1->val.val_double;
				res_double_rhs = c2->type == mate__ca_sym_Type__CONST_INT
									 ? c2->val.val_int
									 : c2->val.val_double;
				res_double = pow(res_double_lhs, res_double_lhs);
				sprintf(mate__ca_expression_str_tmp, "%f", res_double);
				delete_sym_nonrecursive(c1);
				delete_sym_nonrecursive(c2);
				delete_sym_nonrecursive(*sym);
				*sym =
					create_sym(mate__ca_sym_Type__CONST_DOUBLE,
							   mate__ca_expression_str_tmp, NULL, &res_double);
			}
			break;
		default:
			break;
		}
		break;
	case mate__ca_sym_Type__FUNCTION_CALL:
		switch ((*sym)->val.function_code) {
		case mate__ca_sym_Functioncode__SIN:
			// TODO: sin(const) := const, ...
			break;
		default:
			break;
		}
		break;
	default:
		ASSERT(false); /* unimplemented */
	}
}

void mate__ca_optimize_expression(struct mate__ca_Expression *expression) {
	if (expression->tree == NULL)
		return;

	optimize_rec(&expression->tree);

	update_polish_notation(expression);
}

void mate__ca_delete_expression(struct mate__ca_Expression *expression) {
	// TODO
}

char *mate__ca_expression2str__infix(struct mate__ca_Expression *expression) {
	if (mate__ca_expression_str_tmp == NULL)
		mate__ca_expression_str_tmp =
			ALLOC(char, mate__ca_expression_str_tmp_size);
	mate__ca_expression_str_tmp[0] = '\0';

	if (expression == NULL)
		return mate__ca_expression_str_tmp;

	struct mate__List *stack = mate__create_list();

	char *str, *lhs, *rhs, *tos;

	const int n = expression->polish_len;
	for (int i = n - 1; i >= 0; i--) {
		struct mate__ca_Sym *sym = expression->polish[i];
		switch (sym->type) {
		case mate__ca_sym_Type__VAR:
		case mate__ca_sym_Type__CONST_INT:
		case mate__ca_sym_Type__CONST_DOUBLE:
			str = ALLOC(char, strlen(sym->str) + 1);
			strcpy(str, sym->str);
			mate__push_to_list(stack, str);
			break;
		case mate__ca_sym_Type__UNARY_OPERATOR:
			tos = mate__pop_from_list(stack);
			str = ALLOC(char, 1 /*minus*/ + strlen(tos) + 2 /*brackets*/ + 1);
			sprintf(str, "-(%s)", tos);
			mate__push_to_list(stack, str);
			break;
		case mate__ca_sym_Type__FUNCTION_CALL:
			tos = mate__pop_from_list(stack);
			str = ALLOC(char,
						strlen(sym->str) + strlen(tos) + 2 /*brackets*/ + 1);
			sprintf(str, "%s(%s)", sym->str, tos);
			mate__push_to_list(stack, str);
			break;
		case mate__ca_sym_Type__BINARY_OPERATOR:
			lhs = mate__pop_from_list(stack);
			rhs = mate__pop_from_list(stack);
			str = ALLOC(char, strlen(lhs) + strlen(sym->str) + strlen(rhs) +
								  2 /*brackets*/ + 1);
			sprintf(str, "(%s%s%s)", lhs, sym->str, rhs);
			free(lhs);
			free(rhs);
			mate__push_to_list(stack, str);
			break;
		default:
			ASSERT(false); /* unimplemented */
		}
	}

	ASSERT(mate__get_list_length(stack) == 1);

	int len = strlen((char *)stack->first->data) + 1;
	if (mate__ca_expression_str_tmp_size < len) {
		mate__ca_expression_str_tmp_size = len;
		mate__ca_expression_str_tmp =
			REALLOC(mate__ca_expression_str_tmp, char, len);
	}
	strcpy(mate__ca_expression_str_tmp, (char *)stack->first->data);

	mate__delete_list(stack, true, NULL);

	return mate__ca_expression_str_tmp;
}

char *mate__ca_expression2str__polish(struct mate__ca_Expression *expression) {
	/* reserve memory */
	if (mate__ca_expression_str_tmp == NULL)
		mate__ca_expression_str_tmp =
			ALLOC(char, mate__ca_expression_str_tmp_size);
	mate__ca_expression_str_tmp[0] = '\0';
	/* build string */
	for (int i = 0; i < expression->polish_len; i++) {
		if (i > 0)
			strcat(mate__ca_expression_str_tmp, " ");
		if (expression->polish[i]->type == mate__ca_sym_Type__UNARY_OPERATOR &&
			expression->polish[i]->str[0] == '-')
			strcat(mate__ca_expression_str_tmp, "~");
		else
			strcat(mate__ca_expression_str_tmp, expression->polish[i]->str);
	}
	return mate__ca_expression_str_tmp;
}

bool mate__ca_eval_expression(struct mate__ca_Expression *expression,
							  double *res) {

	if (expression == NULL)
		return false;

	double *stack = ALLOC(
		double,
		expression
			->polish_len); // TODO: allocale globally to reduce ALLOC-time???
	bool *stack_is_int =
		ALLOC(bool, expression->polish_len); /* TODO!!! distinguish int and
												double while calculating */
	int tos = -1;

	double lhs, rhs;

	const int n = expression->polish_len;
	for (int i = n - 1; i >= 0; i--) {
		struct mate__ca_Sym *sym = expression->polish[i];
		switch (sym->type) {
		case mate__ca_sym_Type__VAR:
			free(stack);
			return false;
		case mate__ca_sym_Type__CONST_INT:
			stack[++tos] = sym->val.val_int;
			break;
		case mate__ca_sym_Type__CONST_DOUBLE:
			stack[++tos] = sym->val.val_double;
			break;
		case mate__ca_sym_Type__FUNCTION_CALL:
			switch (sym->val.function_code) {
			case mate__ca_sym_Functioncode__SIN:
				stack[tos] = sin(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__COS:
				stack[tos] = cos(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__EXP:
				stack[tos] = exp(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__TAN:
				stack[tos] = tan(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__COT:
				stack[tos] = cos(stack[tos]) / sin(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__SQRT:
				stack[tos] = sqrt(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__ASIN:
				stack[tos] = asin(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__ACOS:
				stack[tos] = acos(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__ATAN:
				stack[tos] = atan(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__ACOT:
				stack[tos] = M_PI_2 - atan(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__LN:
				stack[tos] = log(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__LOG2:
				stack[tos] = log2(stack[tos]);
				break;
			case mate__ca_sym_Functioncode__LOG10:
				stack[tos] = log10(stack[tos]);
				break;
			default:
				ASSERT(false);
			}
			break;
		case mate__ca_sym_Type__UNARY_OPERATOR:
			stack[tos] = -stack[tos];
			break;
		case mate__ca_sym_Type__BINARY_OPERATOR:
			switch (sym->str[0]) {
			case '+':
				stack[tos - 1] = stack[tos] + stack[tos - 1];
				break;
			case '-':
				stack[tos - 1] = stack[tos] - stack[tos - 1];
				break;
			case '*':
				stack[tos - 1] = stack[tos] * stack[tos - 1];
				break;
			case '/':
				stack[tos - 1] = stack[tos] / stack[tos - 1];
				break;
			case '^':
				stack[tos - 1] = pow(stack[tos], stack[tos - 1]);
				break;
			default:
				ASSERT(false);
				break; // TODO
			}
			tos--;
			break;
		default:
			ASSERT(false); /* unimplemented */
		}
	}

	ASSERT(tos == 0);

	*res = stack[0];

	free(stack);
	free(stack_is_int);

	return true;
}

bool mate__ca_integrate_expression_numerically(
	struct mate__ca_Expression *expression, const char *variable_name,
	double *res) {

	/* ----- check that  variable_name  is the only variable used in expression
	 * ----- */

	ASSERT(false); // TODO

	return true;
}

bool mate__ca_set_variable(struct mate__ca_Expression *expression,
						   char *variable_name, double value) {
	const int n = expression->polish_len;
	bool occurrence = false;
	for (int i = 0; i < n; i++) {
		struct mate__ca_Sym *sym = expression->polish[i];
		if (sym->type == mate__ca_sym_Type__VAR &&
			strcmp(sym->str, variable_name) == 0) {
			occurrence = true;
			sym->type = mate__ca_sym_Type__CONST_DOUBLE; // TODO: double vs int
			sym->val.val_double = value;
		}
	}
	return occurrence;
}

char *mate__ca_get_error_msg(void) { return mate__ca_err_msg; }
