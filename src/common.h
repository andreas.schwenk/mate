/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== COMMON / SHARED DEFINITIONS ======================================== */

#ifndef __COMMON__H
#define __COMMON__H

#include <assert.h>
#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

/* ----- memory management -------------------------------------------------- */

#define ALLOC(TYPE, NUM_ELEM) (TYPE *)malloc(sizeof(TYPE) * NUM_ELEM)
#define REALLOC(PTR, TYPE, NUM_ELEM)                                           \
	(TYPE *)realloc(PTR, sizeof(TYPE) * NUM_ELEM)
#define DEALLOC(PTR) free(PTR)

/* ----- asserts------------------------------------------------------------- */

#define ASSERT(X) assert(X) /* TODO: must depend on "if debugging" */
#define UNIMPLEMENTED() { printf("unimplemented\n"); ASSERT(false); }

/* ----- constants ---------------------------------------------------------- */

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

#ifndef M_PI_2
#define M_PI_2 1.57079632679489661923132169163975144
#endif

#ifndef M_PI_4
#define M_PI_4 0.785398163397448309615660845819875721
#endif

/* ----- simple functions --------------------------------------------------- */

char *strdup_(const char *src);

/**
 * mate__pow_int - calculates base^exponent for integer input
 * @base:          base
 * @exponent:      exponent
 */
int64_t mate__pow_int(int64_t base, int64_t exponent);

#define mate__compare_scalars(x, y, epsilon) (fabs((x) - (y)) < epsilon)

void mate__error(const char *msg);

/* -------------------------------------------------------------------------- */

#endif
