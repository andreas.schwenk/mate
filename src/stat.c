/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#include "stat.h"

double mate__stat_sum(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double sum = 0.0;
	for (int i = 0; i < N; i++)
		sum += x[i];
	return sum;
}

double mate__stat_sum_abs(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double sum = 0.0;
	for (int i = 0; i < N; i++)
		sum += x[i] < 0.0 ? -x[i] : x[i];
	return sum;
}

double mate__stat_min(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double min = x[0];
	for (int i = 0; i < N; i++) {
		if (x[i] < min) {
			min = x[i];
		}
	}
	return min;
}

int mate__stat_min_idx(double *x, const int N) {
	if (N == 0)
		return 0;
	double min = x[0];
	double min_idx = 0;
	for (int i = 0; i < N; i++) {
		if (x[i] < min) {
			min = x[i];
			min_idx = i;
		}
	}
	return min_idx;
}

double mate__stat_max(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double max = x[0];
	for (int i = 0; i < N; i++) {
		if (x[i] > max) {
			max = x[i];
		}
	}
	return max;
}

int mate__stat_max_idx(double *x, const int N) {
	if (N == 0)
		return 0;
	double max = x[0];
	double max_idx = 0;
	for (int i = 0; i < N; i++) {
		if (x[i] > max) {
			max = x[i];
			max_idx = i;
		}
	}
	return max_idx;
}

double mate__stat_avrg(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double avrg = 0.0;
	for (int i = 0; i < N; i++)
		avrg += x[i];
	avrg /= (double)N;
	return avrg;
}

double mate__stat_stdev(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double avrg = mate__stat_avrg(x, N);
	double stdev = 0.0;
	for (int i = 0; i < N; i++)
		stdev += (x[i] - avrg) * (x[i] - avrg);
	stdev /= (double)(N - 1);
	return sqrt(stdev);
}

int cmp_double(const void *v1, const void *v2) {
	const double *v1d = (const double *)v1;
	const double *v2d = (const double *)v2;
	return (*v1d > *v2d) - (*v1d < *v2d);
}

double mate__stat_median(double *x, const int N) {
	if (N == 0)
		return 0.0;
	double *tmp = ALLOC(double, N);
	memcpy(tmp, x, sizeof(double) * N);
	qsort(tmp, N, sizeof(double), cmp_double);
	double res =
		(N % 2) == 1 ? tmp[N / 2] : (tmp[N / 2 - 1] + tmp[N / 2]) / 2.0;
	free(tmp);
	return res;
}

double mate__stat_covariance(double *x, double *y, const int N) {
	if (N == 0)
		return 0.0;
	double avrg_x = mate__stat_avrg(x, N);
	double avrg_y = mate__stat_avrg(y, N);
	double stdev_x = mate__stat_stdev(x, N);
	double stdev_y = mate__stat_stdev(y, N);
	double res = 0.0;
	for (int i = 0; i < N; i++)
		res += (x[i] - avrg_x) * (y[i] - avrg_y);
	res /= (double)(N - 1);
	return res;
}

double mate__stat_corr_coeff(double *x, double *y, const int N) {
	if (N == 0)
		return 0.0;
	double avrg_x = mate__stat_avrg(x, N);
	double avrg_y = mate__stat_avrg(y, N);
	double stdev_x = mate__stat_stdev(x, N);
	double stdev_y = mate__stat_stdev(y, N);
	double cov = 0.0;
	for (int i = 0; i < N; i++)
		cov += (x[i] - avrg_x) * (y[i] - avrg_y);
	cov /= (double)(N - 1);
	double divisor = stdev_x * stdev_y;
	if (fabs(divisor) < 1e-15)
		return 0.0;
	return cov / divisor;
}
