/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== CA := COMPUTER ALGEBRA ============================================= */

#ifndef __CA__H
#define __CA__H

#include <inttypes.h>

/* ----- expression --------------------------------------------------------- */

/**
 * TODO
 */
struct mate__ca_Sym {
	int type;
	char *str;
	union mate__ca_Sym_value {
		int64_t function_code;
		int64_t val_int;
		double val_double;
	} val;
	struct mate__ca_Sym **children;
	int num_children;
	struct mate__ca_Sym *deriv; /* derivation */
	bool flag; /* e.g. to indicate if the symbol is still in use (memory
				  management) */
};

/**
 * TODO
 */
struct mate__ca_Expression {
	struct mate__ca_Sym *tree;
	struct mate__ca_Sym *
		*polish;	/* polish notation (array of pointers to symbols) */
	int polish_len; /* number of symbols in polish notation */
};

/**
 * mate__ca_create_expression - create a symbol tree from expressoin string
 * @return:        expression or NULL, if exp_str cannot be parsed
 * @exp_str:       expression given as string
 */
struct mate__ca_Expression *mate__ca_create_expression(const char *exp_str);

/**
 * mate__ca_delete_expression - delete expression
 * @expression:    expression
 */
void mate__ca_delete_expression(struct mate__ca_Expression *expression);

/**
 * mate__ca_expression2str__infix - convert expression to string in infix
 * notation
 * @return:        string representation of expression. Volatile -> user MUST
 * create a copy
 * @expression:    expression
 */
char *mate__ca_expression2str__infix(struct mate__ca_Expression *expression);

/**
 * mate__ca_expression2str__polish - convert expression to string in polish
 * notation
 * @return:        string representation of expression. Volatile -> user MUST
 * create a copy
 * @expression:    expression. The unary operator "-" (negation) is formatted as
 * "~"
 */
char *mate__ca_expression2str__polish(struct mate__ca_Expression *expression);

/**
 * mate__ca_eval_expression - evaluates expression
 * @return:        success. Fails in case of unset variables
 * @expression:    expression
 * @res:           solution
 */
bool mate__ca_eval_expression(struct mate__ca_Expression *expression,
							  double *res);

/**
 * mate__ca_set_variable - substitute variable by constant
 * @return:        true := at least one occurence of the variable could be found
 * @variable_name: variable name to substitute
 * @value:         constant that substitutes variable
 */
bool mate__ca_set_variable(struct mate__ca_Expression *expression,
						   char *variable_name, double value);

/**
 * mate__ca_derivate_expression - derivate expression
 * @return:        success
 * @variable_name: derivation variable
 */
bool mate__ca_derivate_expression(struct mate__ca_Expression *expression,
								  const char *variable_name);

/**
 * mate__ca_optimize_expression - summarize expression
 * @expression:    expression to optimize
 */
void mate__ca_optimize_expression(struct mate__ca_Expression *expression);

/**
 * mate__ca_integrate_expression_numerically - integrates expression numerically
 * @return:        success
 * @variable_name: derivation variable
 * @res:           result
 */
bool mate__ca_integrate_expression_numerically(
	struct mate__ca_Expression *expression, const char *variable_name,
	double *res);

/**
 * mate__ca_get_error_msg - get message of last error. Result is volatile and
 * only valid until the next error occurs
 * @return:        error message
 */
char *mate__ca_get_error_msg(void);

/* -------------------------------------------------------------------------- */

#endif
