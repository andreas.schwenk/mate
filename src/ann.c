// TODO: switch to "C"

/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <cstdio>
#include <math.h>
#include <stdlib.h>

#include "ArtificialNeuralNetwork.h"

ArtificialNeuralNetwork::ArtificialNeuralNetwork(
	int numberInputs, int numberOutputs, int numberHiddenLayers,
	int numberNeuronsInHiddenLayer, bool weightsGiven, double *initialWeights) {
	if (numberInputs == 0 || numberOutputs == 0 || numberHiddenLayers == 0 ||
		numberNeuronsInHiddenLayer == 0) {
		printf("Error: ArtificialNeuralNetwork: each parameter must be greater "
			   "than zero!\n");
		return;
	}

	int i, j;

	this->numberInputs = numberInputs;
	this->numberOutputs = numberOutputs;
	this->numberHiddenLayers = numberHiddenLayers;
	this->numberNeuronsInHiddenLayer = numberNeuronsInHiddenLayer;

	// create a Multilayer Perceptron - ANN
	layers = new Layer[numberHiddenLayers + 2];

	// input layer
	layers[0].neurons = new Neuron[numberInputs];

	// output layer
	layers[numberHiddenLayers + 1].neurons = new Neuron[numberOutputs];
	for (i = 0; i < numberOutputs; i++)
		layers[numberHiddenLayers + 1].neurons[i].weights =
			new double[numberNeuronsInHiddenLayer + 1];

	// hidden layers
	for (i = 1; i <= numberHiddenLayers; i++) {
		layers[i].neurons = new Neuron[numberNeuronsInHiddenLayer];
		for (j = 0; j < numberNeuronsInHiddenLayer; j++) {
			if (i == 1)
				layers[i].neurons[j].weights = new double[numberInputs + 1];
			else
				layers[i].neurons[j].weights =
					new double[numberNeuronsInHiddenLayer + 1];
		}
	}

	initWeights(!weightsGiven, initialWeights);
}

ArtificialNeuralNetwork::~ArtificialNeuralNetwork() {
	// TODO
}

double ArtificialNeuralNetwork::getRandomNumber() {
	double res = double(rand()) / double(RAND_MAX); // [0..1]]
	res = res * 2.0 - 1.0;							// [-1,1]]
	// return res * 0.25; // output should be "very small". Here [-0.25,0.25]
	return 0.25; // !!!!!!!!! TODO
}

void ArtificialNeuralNetwork::initWeights(bool randomized,
										  double *startValues) {
	int i, j, k;

	int idx = 0;

	// set weights for first hidden layer
	for (j = 0; j < numberNeuronsInHiddenLayer; j++)
		for (k = 0; k < numberInputs + 1; k++)
			if (randomized)
				layers[1].neurons[j].weights[k] = getRandomNumber();
			else
				layers[1].neurons[j].weights[k] = startValues[idx++];

	// set weights for other hidden layers
	for (i = 2; i < numberHiddenLayers + 1; i++)
		for (j = 0; j < numberNeuronsInHiddenLayer; j++)
			for (k = 0; k < numberNeuronsInHiddenLayer + 1; k++)
				if (randomized)
					layers[i].neurons[j].weights[k] = getRandomNumber();
				else
					layers[i].neurons[j].weights[k] = startValues[idx++];

	// set weights for output layer
	for (j = 0; j < numberOutputs; j++)
		for (k = 0; k < numberNeuronsInHiddenLayer + 1; k++)
			if (randomized)
				layers[numberHiddenLayers + 1].neurons[j].weights[k] =
					getRandomNumber();
			else
				layers[numberHiddenLayers + 1].neurons[j].weights[k] =
					startValues[idx++];
}

void ArtificialNeuralNetwork::calculate() {
	int i, j, k;
	double net;
	for (i = 1; i < numberHiddenLayers + 2; i++) {
		for (j = 0;
			 j < (i != numberHiddenLayers + 1 ? numberNeuronsInHiddenLayer
											  : numberOutputs);
			 j++) {
			// weighted-sum (net-function)
			net = -layers[i].neurons[j].weights[0]; // w0
			for (k = 0;
				 k < (i == 1 ? numberInputs : numberNeuronsInHiddenLayer); k++)
				net += layers[i].neurons[j].weights[k + 1] *
					   layers[i - 1].neurons[k].y;
			// sigmoid (activation function)
			layers[i].neurons[j].y = 1.0 / (1.0 + exp(-net));
			// note: no output function due to sigmoid
		}
	}
}

void ArtificialNeuralNetwork::run(double *inputValues, double *outputValues) {
	int i;
	for (i = 0; i < numberInputs; i++)
		layers[0].neurons[i].y = inputValues[i];

	calculate();

	for (i = 0; i < numberOutputs; i++)
		outputValues[i] = layers[numberHiddenLayers + 1].neurons[i].y;
}

void ArtificialNeuralNetwork::backpropagationAlgorithm(
	double *trainingInput, double *trainingOutput, int numberOfTrainigsSets,
	double learningRate, double momentum, int numEpochs, bool showOutput) {
	if (showOutput)
		printf("%d epochs. Error development:\n\n", numEpochs);

#define PRINT_MOD 10000

	int i, j;
	int m, mStarIdx, n;
	int e;  // epoche index
	int ts; // trinings-set index

	double y; // real output value
	double Y; // output value given by current training set
	double error, totalEpocheError = 0.0;

	double wOld, wNew, wChange, x;
	double delta;

	Neuron *mNeuron, *mStarNeuron;

	// set wChange of all neurons to 0:
	for (i = 0; i < numberInputs; i++)
		layers[0].neurons[i].wChange = 0.0;
	for (i = 0; i < numberOutputs; i++)
		layers[numberHiddenLayers + 1].neurons[i].wChange = 0.0;
	for (i = 1; i < numberHiddenLayers + 1; i++)
		for (j = 0; j < numberNeuronsInHiddenLayer; j++)
			layers[i].neurons[j].wChange = 0.0;

	for (e = 0; e < numEpochs; e++) {
		totalEpocheError = 0.0;
		// for all training sets
		for (ts = 0; ts < numberOfTrainigsSets; ts++) {
			// apply input values of training set
			for (i = 0; i < numberInputs; i++)
				layers[0].neurons[i].y = trainingInput[ts * numberInputs + i];
			// calculate output
			calculate();
			// calculate error
			error = 0.0;
			for (i = 0; i < numberOutputs; i++) {
				y = layers[numberHiddenLayers + 1].neurons[i].y;
				Y = trainingOutput[ts * numberOutputs + i];
				error += (Y - y) * (Y - y);
			}
			error *= 0.5;
			totalEpocheError += error;

			if (showOutput && (e % PRINT_MOD == 0))
				printf("%.16f;", error);

			// adjust weights
			// (a) adjust weights of the output layer
			for (m = 0; m < numberOutputs; m++) {
				mNeuron = &(layers[numberHiddenLayers + 1].neurons[m]);
				y = mNeuron->y;
				Y = trainingOutput[ts * numberOutputs + m];
				// for all weights
				for (n = 0; n < numberNeuronsInHiddenLayer + 1;
					 n++) // "+1" for w0
				{
					x = n == 0 ? -1.0
							   : layers[numberHiddenLayers].neurons[n - 1].y;
					wOld = mNeuron->weights[n];
					delta = (Y - y);
					delta *= y * (1.0 - y);
					wChange =
						learningRate * delta * x + momentum * mNeuron->wChange;
					wNew = wOld + wChange;
					mNeuron->wChange = wChange;
					mNeuron->delta = delta;
					mNeuron->weights[n] = wNew;
				}
			}
			// (b) adjust weights of all other layers (except input layer)
			for (int k = numberHiddenLayers; k >= 1; k--) {
				for (m = 0; m < numberNeuronsInHiddenLayer; m++) {
					mNeuron = &(layers[k].neurons[m]);
					y = mNeuron->y;
					// for all weights
					for (n = 0; n < (k == 1 ? numberInputs + 1
											: numberNeuronsInHiddenLayer + 1);
						 n++) // "+1" for w0
					{
						x = n == 0 ? -1.0 : layers[k - 1].neurons[n - 1].y;
						wOld = mNeuron->weights[n];
						delta = 0.0;
						for (mStarIdx = 0;
							 mStarIdx < (k == numberHiddenLayers
											 ? numberOutputs
											 : numberNeuronsInHiddenLayer);
							 mStarIdx++) {
							mStarNeuron = &(layers[k + 1].neurons[mStarIdx]);
							delta += mStarNeuron->weights[m + 1] *
									 mStarNeuron->delta;
						}
						delta *= y * (1.0 - y);
						wChange = learningRate * delta * x +
								  momentum * mNeuron->wChange;
						wNew = wOld + wChange;
						mNeuron->wChange = wChange;
						mNeuron->delta = delta;
						mNeuron->weights[n] = wNew;
					}
				}
			}
		}
		if (showOutput && (e % PRINT_MOD == 0))
			//    printf("\n%.19f\n", totalEpocheError);
			printf("\n");
	}
	printf("%.19f\n", totalEpocheError);
}

void ArtificialNeuralNetwork::showWeights(bool showMetadata) {
	int k, n, w;
	int numNeurons, numWeights;

	for (k = 1; k < numberHiddenLayers + 2; k++) {
		if (k == numberHiddenLayers + 1)
			numNeurons = numberOutputs;
		else
			numNeurons = numberNeuronsInHiddenLayer;
		for (n = 0; n < numNeurons; n++) {
			if (k == 1)
				numWeights = numberInputs + 1;
			else
				numWeights = numberNeuronsInHiddenLayer + 1;
			for (w = 0; w < numWeights; w++)
				if (showMetadata)
					printf("w(%d) of neuron(%d,%d) = %.16f\n", w, n, k,
						   layers[k].neurons[n].weights[w]);
				else
					printf("%.16f\n", layers[k].neurons[n].weights[w]);
		}
	}
}

void main() {
	ArtificialNeuralNetwork ann(2, 1, 1, 5, false, NULL);
	double trainingInput[] = {0.2, 0.1, 0.3, 0.2, 0.8, 0.1};
	double trainingOutput[] = { 0.3, 0.5, 0.9 } int numberOfTrainigsSets = 3;
	double learningRate = 0.5;
	double momentum = 0.0;
	int numEpochs = 5;
	bool showOutput = true;
	void backpropagationAlgorithm(trainingInput, trainingOutput,
								  numberOfTrainigsSets, learningRate, momentum,
								  numEpochs, showOutput);
}
