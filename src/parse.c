/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "common.h"

#include "parse.h"

bool mate__is_identifier(const char *str) {
	const int n = strlen(str);
	for (int i = 0; i < n; i++) {
		char ch = str[i];
		if (i == 0 && ch != '_' && !IS_CHAR_LETTER(ch))
			return false;
		if (i > 0 && ch != '_' && !IS_CHAR_LETTER(ch) && !IS_CHAR_NUMBER(ch))
			return false;
	}
	return true;
}

bool mate__is_integer(const char *str) {
	/* <integer> ::= "0" | "1..9" { "0..9" } */
	const int n = strlen(str);
	if (n == 1 && str[0] == '0')
		return true;
	for (int i = 0; i < n; i++) {
		char ch = str[i];
		if (i == 0 && !IS_CHAR_NUMBER_1(ch))
			return false;
		if (i > 0 && !IS_CHAR_NUMBER(ch))
			return false;
	}
	return true;
}

bool mate__is_double(const char *str) {
	/* <double> ::= ( "0" | "1..9" { "0..9" } ) [ "." { "0..9" } ] */
	const int n = strlen(str);
	if (n == 0)
		return false;

	bool pre_comma = false;
	bool first_is_zero = str[0] == '0';
	if (first_is_zero && n == 1)
		return true;
	if (first_is_zero && n >= 2 && (str[1] != '.'))
		return false;

	for (int i = first_is_zero ? 1 : 0; i < n; i++) {
		char ch = str[i];
		if (ch == '.') {
			pre_comma = false;
			continue;
		}
		if (pre_comma) {
			if (i == 0 && !IS_CHAR_NUMBER_1(ch))
				return false;
			if (i > 0 && !IS_CHAR_NUMBER(ch))
				return false;
		} else {
			if (!IS_CHAR_NUMBER(ch))
				return false;
		}
	}

	return true;
}

#define EMPTY_CURRENT_TK_STR()                                                 \
	{ current_tk_str[0] = '\0'; }
#define PUSH_CHAR_TO_CURRENT_TK_STR(CH)                                        \
	{                                                                          \
		tmp = strlen(current_tk_str);                                          \
		current_tk_str[tmp] = CH;                                              \
		current_tk_str[tmp + 1] = '\0';                                        \
	}

#define PUSH_TOKEN(I)                                                          \
	{                                                                          \
		current = ALLOC(struct mate__Token, 1);                                \
		current->pos = current_tk_pos;                                         \
		current_tk_pos = I;                                                    \
		current->str = ALLOC(char, strlen(current_tk_str) + 1);                \
		strcpy(current->str, current_tk_str);                                  \
		EMPTY_CURRENT_TK_STR();                                                \
		mate__push_to_list(list, current);                                     \
	}

struct mate__List *mate__tokenize(const char *input, const char *delimiters) {

	const int n = strlen(input), m = strlen(delimiters);
	int tmp;

	struct mate__List *list = mate__create_list();
	// struct mate__Token *first = NULL;
	struct mate__Token *current = NULL;

	int current_tk_pos = 0;
	char current_tk_str[256]; // TODO: large enough? buffer overflow!
	EMPTY_CURRENT_TK_STR();

	for (int i = 0; i < n; i++) {
		char ch = input[i];
		bool is_delimiter = false;
		for (int j = 0; j < m; j++) {
			if (ch == delimiters[j]) {
				is_delimiter = true;
				break;
			}
		}

		if ((is_delimiter || ch == ' ' || ch == '\t') &&
			strlen(current_tk_str) > 0)
			/* push previous characters */
			PUSH_TOKEN(i);

		if (ch == ' ' || ch == '\t')
			current_tk_pos = i + 1;
		else
			PUSH_CHAR_TO_CURRENT_TK_STR(ch);

		if (is_delimiter) {
			/* push delimiter itself */
			PUSH_TOKEN(i);
			current_tk_pos = i + 1;
		}
	}

	if (strlen(current_tk_str) > 0)
		PUSH_TOKEN(0);

	return list;
}

void mate__del_tokenlist(struct mate__List *list) {
	mate__delete_list(
		list, true,
		NULL); // TODO: provide delete function to free string memory
}

void mate__print_tokenlist(struct mate__List *list) {
	struct mate__List_iterator iterator = mate__create_list_iterator(list);
	struct mate__Token *tk;
	while ((tk = mate__get_next_list_iterator_item(&iterator)) != NULL)
		printf("[%d]: %s\n", tk->pos, tk->str);
}
