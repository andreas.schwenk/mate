/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "../src/common.h"

#include "graph_tests.h"

bool mate__graph_tests() {

  printf("========== %s ==========\n", __FILE__);

  char *vertex_attr_labels[] = { "a" };
  char *edge_attr_labels[] = { "w" };
  struct mate__Graph *g = mate__create_graph("mygraph",
    0, NULL,
    1, vertex_attr_labels,
    0, NULL,
    1, edge_attr_labels
  );

  struct mate__Graph_Vertex *v0 = mate__add_vertex_to_graph(g, "v0");
  v0->coord.x = 50.0;
  v0->coord.y = 50.0;
  ASSERT( mate__set_vertex_attribute__double(v0, "a", 3.14) );

  struct mate__Graph_Vertex *v1 = mate__add_vertex_to_graph(g, "v1");
  v1->coord.x = 100.0;
  v1->coord.y =  50.0;
  ASSERT( mate__set_vertex_attribute__double(v1, "a", 2.71) );

  struct mate__Graph_Edge *e0 = mate__add_edge_to_graph(g, "e0", v0, v1);
  ASSERT( mate__set_edge_attribute__double(e0, "w", 2.0) != false );

  ASSERT( mate__get_graph_order(g) == 2 );
  ASSERT( mate__get_graph_size(g) == 1 );

  mate__add_complete_graph(g, 5, 150, 150, 75);

  mate__add_circle_graph(g, 5, 350, 150, 50);

  mate__graph_mst(g, "w");

  mate__print_graph_metadata(g);

  struct mate__Vector3 *coord = ALLOC(struct mate__Vector3, 3);
  mate__set_vector3(&coord[0],  50, 290, 0);
  mate__set_vector3(&coord[1], 150, 300, 0);
  mate__set_vector3(&coord[2], 250, 310, 0);
  struct mate__Matrix *adj_mat = mate__create_matrix(3, 3);
  mate__set_matrix(adj_mat,
    0.0, 1.0, 0.0,
    1.0, 0.0, 1.0,
    0.0, 1.0, 0.0
  );
  mate__add_graph_from_adjacency_matrix(g, coord, adj_mat, NULL);
  free(coord);

  mate__export_graph_as_html_canvas(g, "test.html");

  mate__export_graph(g, "graph.txt");

  struct mate__Graph *g2 = mate__import_graph("graph.txt");
  mate__export_graph(g2, "graph-2.txt");
  mate__delete_graph(g2);

  mate__delete_graph(g);

  return true;
}
