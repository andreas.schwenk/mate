/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "common_tests.h"

bool mate__common_tests() {

  printf("========== %s ==========\n", __FILE__);

  ASSERT(mate__pow_int(2,3) == 8);
  ASSERT(mate__pow_int(10,0) == 1);
  ASSERT(mate__pow_int(5,3) == 125);
  ASSERT(mate__pow_int(7,6) == 117649);

  return true;
}
