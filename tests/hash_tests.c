/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "../src/common.h"

#include "hash_tests.h"

bool mate__hash_tests() {

  printf("========== %s ==========\n", __FILE__);

  const int N = 10;
  const double EPSILON = 1e-9;

  struct mate__Hashtable *ht = mate__create_hashtable(N, mate__standard_hash_function) ;

  int *data10 = ALLOC(int, 1);
  *data10 = 10;
  mate__insert_into_hashtable(ht, "ten", data10);

  int *data10_2 = ALLOC(int, 1);
  *data10_2 = 10+2;
  mate__insert_into_hashtable(ht, "ten", data10_2);

  int *data11 = ALLOC(int, 1);
  *data11 = 11;
  mate__insert_into_hashtable(ht, "eleven", data11);

  int *data15 = ALLOC(int, 1);
  *data15 = 15;
  mate__insert_into_hashtable(ht, "fifteen", data15);

  ASSERT( mate__compare_scalars(mate__get_hashtable_load_factor(ht), 0.2, EPSILON) );

  data10 = mate__get_hashtable_item(ht, "ten");
  ASSERT(data10 != NULL);
  ASSERT(*data10 == 10);

  data11 = mate__get_hashtable_item(ht, "eleven");
  ASSERT(data11 != NULL);
  ASSERT(*data11 == 11);

  data15 = mate__get_hashtable_item(ht, "fifteen");
  ASSERT(data15 != NULL);
  ASSERT(*data15 == 15);

  ASSERT( mate__get_hashtable_length(ht) == 4 );

  printf("---- content ... ----\n");
  for(int i=0; i<N; i++) {
    if(ht->buckets[i] == NULL)
      printf("%d empty\n", i);
    else
      printf("%d #%d\n", i, mate__get_list_length(ht->buckets[i]));
  }
  printf("---- ... content ----\n");

  struct mate__Hashtable_Iterator it = mate__create_hashtable_iterator(ht);
  int *data;
  while((data=mate__get_next_hashtable_iterator_item(&it)) != NULL) {
    printf("----- %d\n", *data);
  }

  free(data10);
  free(data10_2);
  free(data11);
  free(data15);

  mate__delete_hashtable(ht);

  return true;
}
