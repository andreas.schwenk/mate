/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "../src/common.h"
#include "stat_tests.h"

bool mate__stat_tests() {

  printf("========== %s ==========\n", __FILE__);

  /* ----- basic functions ----------------------------------------------------*/

  const double EPSILON = 1e-10;

  #define N 7
  double x[N] = { -3.4, 2.3, 0.0, -10.8,  2.0,  1.234, -1.4 };
  double y[N] = {  1.0, 2.0, 4.3,  -1.0, -2.45, 7.2,   -8.1 };

  ASSERT( mate__compare_scalars(mate__stat_sum(x, N), -10.066, EPSILON) );
  ASSERT( mate__compare_scalars(mate__stat_sum_abs(x, N), 21.134, EPSILON) );

  ASSERT( mate__compare_scalars(mate__stat_min(x, N), -10.8, EPSILON) );
  ASSERT( mate__stat_min_idx(x, N) == 3 );
  ASSERT( mate__compare_scalars(mate__stat_max(x, N), 2.3, EPSILON) );
  ASSERT( mate__stat_max_idx(x, N) == 1 );
  ASSERT( mate__compare_scalars(mate__stat_avrg(x, N), -1.4380, EPSILON) );

  printf("%f\n", mate__stat_stdev(x, N));
  ASSERT( mate__compare_scalars(mate__stat_stdev(x, N), 4.59162004816020, EPSILON) );

  ASSERT( mate__compare_scalars(mate__stat_median(x, N), 0.0, EPSILON) );

  ASSERT( mate__compare_scalars(mate__stat_covariance(x, y, N),  5.26115000000000, EPSILON) );
  ASSERT( mate__compare_scalars(mate__stat_covariance(x, x, N), 21.0829746666667, EPSILON) );

  ASSERT( mate__compare_scalars(mate__stat_corr_coeff(x, x, N), 1.0, EPSILON) );
  ASSERT( mate__compare_scalars(mate__stat_corr_coeff(x, y, N), 0.231726854349846, EPSILON) );

  return true;
}
