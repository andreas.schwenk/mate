/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "../src/common.h"

#include "la_tests.h"

bool mate__la_tests() {

  printf("========== %s ==========\n", __FILE__);

  const double EPSILON = 1e-9;

  /* ----- vector tests ----------------------------------------------------- */

  struct mate__Vector *lhs = mate__create_vector(3);
  struct mate__Vector *rhs = mate__create_vector(3);
  struct mate__Vector *res = mate__create_vector(3);
  struct mate__Vector *solultion_add = mate__create_vector(3);
  struct mate__Vector *solultion_sub = mate__create_vector(3);
  struct mate__Vector *solultion_cross = mate__create_vector(3);

  mate__set_vector(lhs,              1.0,  2.0,  3.0);
  mate__set_vector(rhs,              4.0,  5.0,  6.0);
  mate__set_vector(solultion_add,    5.0,  7.0,  9.0);
  mate__set_vector(solultion_sub,   -3.0, -3.0, -3.0);
  mate__set_vector(solultion_cross, -3.0,  6.0, -3.0);

  mate__add_vectors(res, lhs, rhs);
  ASSERT( mate__compare_vectors(res, solultion_add, EPSILON) );

  mate__sub_vectors(res, lhs, rhs);
  ASSERT( mate__compare_vectors(res, solultion_sub, EPSILON) );

  ASSERT( mate__compare_scalars(mate__dot_vectors(lhs, rhs), 32.0, EPSILON) );

  mate__cross_vectors(res, lhs, rhs);
  ASSERT( mate__compare_vectors(res, solultion_cross, EPSILON) );

  mate__delete_vector(lhs);
  mate__delete_vector(rhs);
  mate__delete_vector(res);

  mate__delete_vector(solultion_add);
  mate__delete_vector(solultion_sub);
  mate__delete_vector(solultion_cross);

  /* ----- matrix tests ----------------------------------------------------- */

  struct mate__Matrix *m1 = mate__create_matrix(3, 3);
  struct mate__Matrix *m2 = mate__create_matrix(3, 3);
  struct mate__Matrix *m3 = mate__create_matrix(3, 3);

  struct mate__Matrix *m_sol = mate__create_matrix(3, 3);

  mate__set_matrix(m1,
    1.0, 2.0, 3.0,
    4.0, 5.0, 6.0,
    7.0, 8.0, 9.0
  );

  mate__print_matrix(m1);

  mate__set_matrix(m2,
    0.0, 1.0, 2.0,
    3.0, 4.0, 5.0,
    6.0, 7.0, 8.0
  );

  /* ----- add ----- */
  mate__add_matrices(m3, m1, m2);
  mate__set_matrix(m_sol,
     1.0,  3.0,  5.0,
     7.0,  9.0, 11.0,
    13.0, 15.0, 17.0
  );
  ASSERT( mate__compare_matrices(m3, m_sol, EPSILON) );

  /* ----- sub ----- */
  mate__sub_matrices(m3, m1, m2);
  for(int i=0; i<3; i++)
    mate__set_matrix_row(m_sol, i,  1.0, 1.0, 1.0);
  ASSERT( mate__compare_matrices(m3, m_sol, EPSILON) );

  /* ----- mul ----- */
  mate__mul_matrices(m3, m1, m2);
  mate__set_matrix_col(m_sol, 0,  24.0, 51.0,  78.0);
  mate__set_matrix_col(m_sol, 1,  30.0, 66.0, 102.0);
  mate__set_matrix_col(m_sol, 2,  36.0, 81.0, 126.0);
  ASSERT( mate__compare_matrices(m3, m_sol, EPSILON) );

  struct mate__Vector *vec_multest = mate__create_vector(3);
  struct mate__Vector *vec_multest_res = mate__create_vector(3);
  struct mate__Vector *vec_multest_sol = mate__create_vector(3);
  mate__set_vector(vec_multest, 1.0, 2.0, 3.0);
  mate__set_vector(vec_multest_sol, 14.0, 32.0, 50.0);
  mate__mul_matrix_vec(vec_multest_res, m1, vec_multest);
  ASSERT( mate__compare_vectors(vec_multest_res, vec_multest_sol, EPSILON) );
  mate__delete_vector(vec_multest);
  mate__delete_vector(vec_multest_res);
  mate__delete_vector(vec_multest_sol);

  /* ----- cadd ----- */
  // TODO

  /* ----- csub ----- */
  // TODO

  /* ----- cmul ----- */
  // TODO

  /* ----- mate__cmul_matrix_vec ----- */
  // TODO

  /* ----- transpose ----- */
  mate__transpose_matrix(m3, m1);
  mate__set_matrix(m_sol,
    1.0, 4.0, 7.0,
    2.0, 5.0, 8.0,
    3.0, 6.0, 9.0
  );
  ASSERT( mate__compare_matrices(m3, m_sol, EPSILON) );

  /* ----- zero matrix ----- */
  mate__set_zero_matrix(m3);
  mate__set_matrix(m_sol,
    0.0, 0.0, 0.0,
    0.0, 0.0, 0.0,
    0.0, 0.0, 0.0
  );
  ASSERT( mate__compare_matrices(m3, m_sol, EPSILON) );

  /* ----- ones matrix ----- */
  mate__set_ones_matrix(m3);
  mate__set_matrix(m_sol,
    1.0, 1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0, 1.0, 1.0
  );
  ASSERT( mate__compare_matrices(m3, m_sol, EPSILON) );

  /* ----- trace ----- */
  ASSERT( mate__compare_scalars(mate__trace(m1), 15.0, EPSILON) );

  /* ---- determinant ---- */
  struct mate__Matrix *det_test_1x1 = mate__create_matrix(1, 1);
  struct mate__Matrix *det_test_2x2 = mate__create_matrix(2, 2);
  struct mate__Matrix *det_test_3x3 = mate__create_matrix(3, 3);
  struct mate__Matrix *det_test_4x4 = mate__create_matrix(4, 4);
  struct mate__Matrix *det_test_5x5 = mate__create_matrix(5, 5);

  mate__set_matrix(det_test_1x1,
    2.0
  );
  ASSERT( mate__compare_scalars(mate__determinant(det_test_1x1), 2.0, EPSILON) );

  mate__set_matrix(det_test_2x2,
      2.3,  -6.8,
      1.34, 99.0
  );
  ASSERT( mate__compare_scalars(mate__determinant(det_test_2x2), 236.812, EPSILON) );

  mate__set_matrix(det_test_3x3,
    3.0, -5.0,  6.0,
    -2.0, 6.0, -3.0,
    -7.0, 3.0,  5.0
  );
  ASSERT( mate__compare_scalars(mate__determinant(det_test_3x3), 178.0, EPSILON) );

  mate__set_matrix(det_test_4x4,
     3.0,  -5.0,  6.0, -8.0,
    -2.0,   6.0, -3.0,  2.0,
    -7.0,   3.0,  5.0,  1.0,
     6.0,  13.0,  6.0, -3.0
  );
  ASSERT( mate__compare_scalars(mate__determinant(det_test_4x4), 4097.0, EPSILON) );

  mate__set_matrix(det_test_5x5,
     3.0,  -5.0,  6.0, -8.0,  4.0,
    -2.0,   6.0, -3.0,  2.0, -6.0,
    -7.0,   3.0,  5.0,  1.0, -1.0,
     6.0,  13.0,  6.0, -3.0,  8.0,
     3.0,  -6.0 , 2.0 , 7.0, -3.0
  );
  ASSERT( mate__compare_scalars(mate__determinant(det_test_5x5), -79409.0, EPSILON) );

  /* ---- inverse matrix ---- */
  struct mate__Matrix *inv_res_1x1 = mate__create_matrix(1, 1);
  struct mate__Matrix *inv_res_2x2 = mate__create_matrix(2, 2);
  struct mate__Matrix *inv_res_3x3 = mate__create_matrix(3, 3);
  struct mate__Matrix *inv_res_4x4 = mate__create_matrix(4, 4);
  //struct mate__Matrix *inv_res_5x5 = mate__create_matrix(5, 5); // TODO

  struct mate__Matrix *inv_sol_1x1 = mate__create_matrix(1, 1);
  struct mate__Matrix *inv_sol_2x2 = mate__create_matrix(2, 2);
  struct mate__Matrix *inv_sol_3x3 = mate__create_matrix(3, 3);
  struct mate__Matrix *inv_sol_4x4 = mate__create_matrix(4, 4);
  //struct mate__Matrix *inv_sol_5x5 = mate__create_matrix(5, 5); // TODO

  mate__set_matrix(inv_sol_1x1,
    0.5
  );

  mate__set_matrix(inv_sol_2x2,
     0.41805313919902709, 0.02871476107629681,
    -0.00565849703562320, 0.00971234565815922
  );

  mate__set_matrix(inv_sol_3x3,
    0.2191011235955056, 0.2415730337078651, -0.1179775280898876,
    0.1741573033707865, 0.3202247191011236, -0.0168539325842697,
    0.2022471910112360, 0.1460674157303371,  0.0449438202247191
  );

  mate__set_matrix(inv_sol_4x4,
    -0.06980717598242617, -0.14962167439589943, -0.07835001220405174,  0.06028801562118623,
     0.01391261898950450,  0.10324627776421770,  0.00512570173297536,  0.03343910178179157,
    -0.06443739321454722, -0.21503539175006103,  0.08152306565779840,  0.05565047595801807,
    -0.20820112277276059, -0.28191359531364418,  0.02855748108371980,  0.04344642421283867
  );

  /*mate__set_matrix(inv_sol_5x5,
    TODO
  );*/

  ASSERT( mate__inverse_matrix(inv_res_1x1, det_test_1x1) );
  ASSERT( mate__compare_matrices(inv_res_1x1, inv_sol_1x1, EPSILON) );

  ASSERT( mate__inverse_matrix(inv_res_2x2, det_test_2x2) );
  ASSERT( mate__compare_matrices(inv_res_2x2, inv_sol_2x2, EPSILON) );

  ASSERT( mate__inverse_matrix(inv_res_3x3, det_test_3x3) );
  ASSERT( mate__compare_matrices(inv_res_3x3, inv_sol_3x3, EPSILON) );

  ASSERT( mate__inverse_matrix(inv_res_4x4, det_test_4x4) );
  ASSERT( mate__compare_matrices(inv_res_4x4, inv_sol_4x4, EPSILON) );

  //ASSERT( mate__inverse_matrix(inv_res_5x5, det_test_5x5) ); TODO
  //ASSERT( mate__compare_matrices(inv_res_5x5, inv_sol_5x5, EPSILON) ); TODO

  mate__delete_matrix(inv_res_1x1);
  mate__delete_matrix(inv_res_2x2);
  mate__delete_matrix(inv_res_3x3);
  mate__delete_matrix(inv_res_4x4);
  //mate__delete_matrix(inv_res_5x5); // TODO

  mate__delete_matrix(inv_sol_1x1);
  mate__delete_matrix(inv_sol_2x2);
  mate__delete_matrix(inv_sol_3x3);
  mate__delete_matrix(inv_sol_4x4);
  //mate__delete_matrix(inv_sol_5x5); // TODO

  mate__delete_matrix(det_test_1x1);
  mate__delete_matrix(det_test_2x2);
  mate__delete_matrix(det_test_3x3);
  mate__delete_matrix(det_test_4x4);
  mate__delete_matrix(det_test_5x5);

  mate__delete_matrix(m1);
  mate__delete_matrix(m2);
  mate__delete_matrix(m3);
  mate__delete_matrix(m_sol);

  return true;
}
