/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../src/common.h"

#include "ca_tests.h"

bool mate__ca_tests() {

  printf("========== %s ==========\n", __FILE__);

  struct mate__ca_Expression* expression;
  double res, expected;

  const double EPSILON = 1e-9;

  /* ----- integer test ----------------------------------------------------- */

  expression = mate__ca_create_expression("2 - 24/8");
  expected = 2-24/8;
  if(expression == NULL)
    printf("%s\n", mate__ca_get_error_msg());
  ASSERT(expression != NULL);

  printf("expression (polish): '%s'\n", mate__ca_expression2str__polish(expression));
  printf("expression (infix): '%s'\n", mate__ca_expression2str__infix(expression));

  ASSERT(mate__ca_eval_expression(expression, &res) == true);
  printf("eval expression: '%f'\n", res);
  ASSERT(fabs(res-expected) < EPSILON);

  mate__ca_delete_expression(expression);

  /* ----- double test ------------------------------------------------------ */

  expression = mate__ca_create_expression("exp(2.5) - 24/8 * (2+3)^3");
  expected = exp(2.5) - 24/8 * pow((2+3),3);
  if(expression == NULL)
    printf("%s\n", mate__ca_get_error_msg());
  ASSERT(expression != NULL);

  printf("expression (polish): '%s'\n", mate__ca_expression2str__polish(expression));
  printf("expression (infix): '%s'\n", mate__ca_expression2str__infix(expression));

  ASSERT(mate__ca_eval_expression(expression, &res) == true);
  printf("eval expression: '%f'\n", res);
  ASSERT(fabs(res-(expected)) < EPSILON);

  mate__ca_delete_expression(expression);

  /* ----- variable tests --------------------------------------------------- */

  expression = mate__ca_create_expression("2*x");
  if(expression == NULL)
    printf("%s\n", mate__ca_get_error_msg());
  ASSERT(expression != NULL);

  printf("expression (polish): '%s'\n", mate__ca_expression2str__polish(expression));
  printf("expression (infix): '%s'\n", mate__ca_expression2str__infix(expression));

  ASSERT(mate__ca_eval_expression(expression, &res) == false);

  mate__ca_set_variable(expression, "x", 3.5);
  expected = 2*3.5;

  ASSERT(mate__ca_eval_expression(expression, &res) == true);

  printf("eval expression: '%f'\n", res);
  ASSERT(fabs(res-expected) < EPSILON);

  mate__ca_delete_expression(expression);

  /* ----- optimization tests ------------------------------------------------- */

  expression = mate__ca_create_expression("(0+((0*x)+(3*1)))");
  if(expression == NULL)
    printf("%s\n", mate__ca_get_error_msg());
  ASSERT(expression != NULL);

  printf("-----opt test-----\n");
  printf("expression (polish): '%s'\n", mate__ca_expression2str__polish(expression));
  printf("expression (infix): '%s'\n", mate__ca_expression2str__infix(expression));

  mate__ca_delete_expression(expression);

  // TODO: add more optimization tests

  /* ----- derivation tests ------------------------------------------------- */

  expression = mate__ca_create_expression("2 + 3*x");
  if(expression == NULL)
    printf("%s\n", mate__ca_get_error_msg());
  ASSERT(expression != NULL);

  printf("expression (polish): '%s'\n", mate__ca_expression2str__polish(expression));
  printf("expression (infix): '%s'\n", mate__ca_expression2str__infix(expression));

  ASSERT(mate__ca_derivate_expression(expression, "x") == true);

  printf("derivated expression (polish): '%s'\n", mate__ca_expression2str__polish(expression));
  printf("derivated expression (infix): '%s'\n", mate__ca_expression2str__infix(expression));

  mate__ca_delete_expression(expression);

  // TODO: add more derivation tests (e.g. for all function types)

  /* ----- numerical integration tests -------------------------------------- */

  // TODO

  return true;
}
