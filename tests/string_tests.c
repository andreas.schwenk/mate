/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "../src/common.h"
#include "string_tests.h"

bool mate__string_tests() {

  printf("========== %s ==========\n", __FILE__);

  /* ----- C string function tests -------------------------------------------*/

  struct mate__List* tokens = mate__split_c_string("  This     is\n a\t \t(test)  \t", "\t \n()", "()");
  //printf("%d\n", mate__get_list_length(tokens));
  ASSERT(mate__get_list_length(tokens) == 6);
  ASSERT(strcmp((char*)mate__get_list_item(tokens, 0), "This") == 0);
  ASSERT(strcmp((char*)mate__get_list_item(tokens, 1), "is") == 0);
  ASSERT(strcmp((char*)mate__get_list_item(tokens, 2), "a") == 0);
  ASSERT(strcmp((char*)mate__get_list_item(tokens, 3), "(") == 0);
  ASSERT(strcmp((char*)mate__get_list_item(tokens, 4), "test") == 0);
  ASSERT(strcmp((char*)mate__get_list_item(tokens, 5), ")") == 0);
  mate__delete_list(tokens, true, NULL);

  /* ----- mate string function tests ----------------------------------------*/

  struct mate__String* s = mate__create_string();

  mate__append_cstr_to_string(s, "Hello, world!");
  printf("##%s##\n", s->data);
  ASSERT(s->n_used == 14);
  ASSERT(mate__get_string_length(s) == 13);
  ASSERT(s->n_used <= s->n_allocated);
  ASSERT(strcmp(s->data, "Hello, world!")==0);

  mate__append_int_to_string(s, 314);
  printf("%s\n", s->data);
  ASSERT(s->n_used == 17);
  ASSERT(mate__get_string_length(s) == 16);
  ASSERT(s->n_used <= s->n_allocated);
  ASSERT(strcmp(s->data, "Hello, world!314")==0);

  mate__append_double_to_string(s, 123.456, 3);
  ASSERT(s->n_used == 24);
  ASSERT(mate__get_string_length(s) == 23);
  ASSERT(s->n_used <= s->n_allocated);
  ASSERT(strcmp(s->data, "Hello, world!314123.456")==0);

  mate__substring_with_end(s, 2, 10);
  ASSERT(strcmp(s->data, "llo, wor")==0);

  mate__clear_string(s);
  ASSERT(mate__get_string_length(s) == 0);

  mate__append_cstr_to_string(s, "this is a test\nblub\nbla bla\n");
  mate__indent_string(s, 2);
  ASSERT(strcmp(s->data, "\t\tthis is a test\n\t\tblub\n\t\tbla bla\n")==0);

  mate__clear_string(s);
  mate__append_cstr_to_string(s, "");
  int line_count = mate__get_string_line_count(s);
  ASSERT(line_count == 1);

  mate__clear_string(s);
  mate__append_cstr_to_string(s, "1\n2\n3\n");
  line_count = mate__get_string_line_count(s);
  ASSERT(line_count == 4);

  mate__clear_string(s);
  mate__append_cstr_to_string(s, " \n");
  mate__indent_string(s, 5);
  ASSERT(strcmp(s->data, "\t\t\t\t\t \n")==0);

  mate__clear_string(s);
  mate__append_cstr_to_string(s, "bla blub");
  mate__append_indent_to_string(s, 4);
  ASSERT(strcmp(s->data, "bla blub\t\t\t\t")==0);

  mate__delete_string(s);

  struct mate__String* s1 = mate__create_string();
  mate__append_cstr_to_string(s1, "Hello, world!");

  ASSERT(mate__string_starts_with(s1, "Hell"));
  ASSERT(mate__string_starts_with(s1, "") == false);
  ASSERT(mate__string_starts_with(s1, "ello") == false);
  ASSERT(mate__string_starts_with(s1, "this is a test 1 2 3") == false);

  ASSERT(mate__string_ends_with(s1, "orld!"));
  ASSERT(mate__string_ends_with(s1, "") == false);
  ASSERT(mate__string_ends_with(s1, "orld") == false);
  ASSERT(mate__string_ends_with(s1, "this is a test 1 2 3") == false);

  ASSERT(mate__string_contains(s1, "Hell"));
  ASSERT(mate__string_contains(s1, "o, wor"));
  ASSERT(mate__string_contains(s1, "rld!"));
  ASSERT(mate__string_contains(s1, "") == false);
  ASSERT(mate__string_contains(s1, "abc") == false);
  ASSERT(mate__string_contains(s1, "this is a test 1 2 3") == false);

  mate__substitute_substring(s1, "ello", "xyzw");
  ASSERT(strcmp(s1->data, "Hxyzw, world!")==0);

  mate__substitute_substring(s1, "Hxy", "ABC");
  ASSERT(strcmp(s1->data, "ABCzw, world!")==0);

  mate__substitute_substring(s1, "world!", "DEF");
  ASSERT(strcmp(s1->data, "ABCzw, DEF")==0);

  mate__substitute_substring(s1, "w, DE", "");
  ASSERT(strcmp(s1->data, "ABCzF")==0);

  mate__clear_string(s1);
  struct mate__List *int_list = mate__create_list();
  int *v;
  v = ALLOC(int, 1);
  *v = 13;
  mate__push_to_list(int_list, v);
  v = ALLOC(int, 1);
  *v = 31;
  mate__push_to_list(int_list, v);
  mate__append_int_list_to_string(s1, int_list);
  ASSERT(strcmp(s1->data, "[13, 31]")==0);
  mate__delete_list(int_list, true, NULL);

  mate__clear_string(s1);
  struct mate__List *double_list = mate__create_list();
  double *v2;
  v2 = ALLOC(double, 1);
  *v2 = 1.3;
  mate__push_to_list(double_list, v2);
  v2 = ALLOC(double, 1);
  *v2 = 3.1;
  mate__push_to_list(double_list, v2);
  mate__append_double_list_to_string(s1, double_list, 3);
  ASSERT(strcmp(s1->data, "[1.300, 3.100]")==0);
  mate__delete_list(double_list, true, NULL);

	// TODO: mate__string_equalto_string
	// TODO: mate__string_equalto_c_string

  return true;
}
