/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../src/common.h"

#include "parse_tests.h"

bool mate__parse_tests() {

  printf("========== %s ==========\n", __FILE__);

  /* ----- 1-token parser ----------------------------------------------------*/

  ASSERT(mate__is_identifier("test") == true);
  ASSERT(mate__is_identifier("test123") == true);
  ASSERT(mate__is_identifier("123test") == false);

  ASSERT(mate__is_integer("0") == true);
  ASSERT(mate__is_integer("123") == true);
  ASSERT(mate__is_integer("0123") == false);
  ASSERT(mate__is_integer("a123") == false);

  ASSERT(mate__is_double("0") == true);
  ASSERT(mate__is_double("0.123") == true);
  ASSERT(mate__is_double("01") == false);
  ASSERT(mate__is_double("123") == true);
  ASSERT(mate__is_double("123.456") == true);
  ASSERT(mate__is_double("123.01") == true);
  ASSERT(mate__is_double("124.") == true);
  ASSERT(mate__is_double(".123") == true);
  ASSERT(mate__is_double("a") == false);
  ASSERT(mate__is_double("a.0") == false);
  ASSERT(mate__is_double("0.a") == false);

  /* ----- tokenizer ---------------------------------------------------------*/

  char str[] = "   1 + 3*  42";
  char delimiters[] = "+*";

  struct mate__List *tokens = mate__tokenize(str, delimiters);
  ASSERT(mate__get_list_length(tokens) == 5);

  struct mate__Token *tk;
  struct mate__List_iterator iterator = mate__create_list_iterator(tokens);

  ASSERT(mate__get_list_length(tokens) == 5);

  int i = 0;
  while((tk=mate__get_next_list_iterator_item(&iterator)) != NULL) {
    switch(i) {
      case 0: ASSERT(strcmp(tk->str, "1")==0); ASSERT(tk->pos ==  3); break;
      case 1: ASSERT(strcmp(tk->str, "+")==0); ASSERT(tk->pos ==  5); break;
      case 2: ASSERT(strcmp(tk->str, "3")==0); ASSERT(tk->pos ==  7); break;
      case 3: ASSERT(strcmp(tk->str, "*")==0); ASSERT(tk->pos ==  8); break;
      case 4: ASSERT(strcmp(tk->str,"42")==0); ASSERT(tk->pos == 11); break;
    }
    i ++;
  }

  mate__del_tokenlist(tokens);

  return true;
}
