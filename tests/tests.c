/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

//#include "ann_test.h" TODO
#include "ca_tests.h"
#include "cluster_tests.h"
#include "common_tests.h"
#include "graph_tests.h"
#include "hash_tests.h"
#include "la_tests.h"
#include "list_tests.h"
#include "parse_tests.h"
#include "stat_tests.h"
#include "string_tests.h"

int main(int argc, char *argv[]) {

  /* note on order: first test low-level functions (where feasible) */
  mate__common_tests();
  mate__string_tests();
  mate__stat_tests();
  mate__la_tests();
  mate__list_tests();
  mate__hash_tests();
  mate__parse_tests();
  mate__ca_tests();
  //mate__graph_tests(); TODO

  //mate__ann_tests(); TODO
  //mate__cluster_tests(); TODO

  printf("========== ... end of tests ==========\n");

  exit(EXIT_SUCCESS);
}
