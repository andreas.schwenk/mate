/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "../src/common.h"

#include "cluster_tests.h"

bool mate__cluster_tests() {

  printf("========== %s ==========\n", __FILE__);

  #define n 8

  double x[n] = { 1, 2, 3, 10, 11, 12, 13, 14 };
  double y[n] = { 4, 5, 6, 30, 31, 32, 33, 34 };

  const int k = 2;

  double kx[k];
  double ky[k];

  const int MAX_IT = 1000000;
  const double EPSILON = 1e-6;

  int iterations = mate__cluster_kmeans_2d(n, x, y, k, kx, ky, MAX_IT, EPSILON);

  ASSERT(iterations <= MAX_IT);

  // TODO

  return true;
}
