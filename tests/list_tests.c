/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "../src/common.h"

#include "list_tests.h"

bool mate__list_tests() {

  printf("========== %s ==========\n", __FILE__);

  struct mate__List *list = mate__create_list();

  ASSERT(list->first == NULL);
  ASSERT(list->last == NULL);
  ASSERT(mate__get_list_length(list) == 0);
  //ASSERT(mate__get_list_item(list,0) == NULL);

  int *data;

  data = ALLOC(int, 1);
  *data = 5;
  mate__push_to_list(list, (void*)data);

  ASSERT(list->last != NULL);
  ASSERT(list->last == list->first);
  ASSERT(mate__get_list_length(list) == 1);
  ASSERT(mate__get_list_length(list) == list->len);
  ASSERT(mate__get_list_item(list,0) != NULL);
  ASSERT( *((int*)mate__get_list_item(list,0)) == 5);
  //ASSERT(mate__get_list_item(list,1) == NULL);
  //ASSERT(mate__get_list_item(list,-1) == NULL);

  data = ALLOC(int, 1);
  *data = 7;
  mate__push_to_list(list, (void*)data);

  ASSERT(list->last != NULL);
  ASSERT(list->last != list->first);
  ASSERT(mate__get_list_length(list) == 2);
  ASSERT(mate__get_list_length(list) == list->len);
  ASSERT( *((int*)mate__get_list_item(list,0)) == 5);
  ASSERT( *((int*)mate__get_list_item(list,1)) == 7);
  //ASSERT(mate__get_list_item(list,2) == NULL);

  struct mate__List_iterator iterator = mate__create_list_iterator(list);
  int i = 0;
  while( (data=mate__get_next_list_iterator_item(&iterator)) != NULL ) {
    switch(i) {
      case 0: ASSERT(*data == 5); break;
      case 1: ASSERT(*data == 7); break;
    }
    i ++;
  }

  // TODO: mate__iterate_list

	mate__set_list_iterator_to_back(&iterator);
	i = 0;
    while( (data=mate__get_prev_list_iterator_item(&iterator)) != NULL ) {
      switch(i) {
        case 0: ASSERT(*data == 7); break;
        case 1: ASSERT(*data == 5); break;
      }
      i ++;
    }

  data = mate__pop_from_list(list);
  ASSERT(*data == 7);
  free(data);

  ASSERT(list->last != NULL);
  ASSERT(list->first != NULL);
  ASSERT(mate__get_list_length(list) == 1);
  ASSERT(mate__get_list_length(list) == list->len);
  //ASSERT(mate__get_list_item(list,1) == NULL);

  data = mate__pop_from_list(list);
  ASSERT(*data == 5);
  free(data);

  ASSERT(list->last == NULL);
  ASSERT(list->first == NULL);
  ASSERT(mate__get_list_length(list) == 0);
  ASSERT(mate__get_list_length(list) == list->len);
  //ASSERT(mate__get_list_item(list,0) == NULL);

  mate__delete_list(list, true, NULL);

  // TODO: tests function pointer in mate__delete_list
  // TODO: tests for "mate__list_delete_item" -> (a) delete first (b) delte "middle" (c) delete last item

  return true;
}
