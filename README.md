```
/******************************************************************************
 *                            MATE - Math Library                             *
 *                  _                                                         *
 *       _ __  __ _| |_ ___                                                   *
 *      | '  \/ _` |  _/ -_)                                                  *
 *      |_|_|_\__,_|\__\___|                                                  *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/
```

Mate (Esperanto for "math") is a small math library written in the C programming
language. The library does not use any external dependencies and thus can be
used on a wide range of devices from embedded systems to high performance
computers.

Components:
- ann:      artificial neural networks (TODO)
- approx:   approximations (TODO)
- ca:       computer algebra / symbolic calculation
- co:       combinatorial optimization (TODO)
- cluster:  clustering
- graph:    graphs (TODO)
- la:       linear algebra
- list:     lists
- hash:     hashtables
- mh:       meta heuristics (TODO)
- parse:    parser
- stat:     statistics
