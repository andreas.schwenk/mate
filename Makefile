#******************************************************************************
#*                            MATE - Math Library                             *
#*                  _                                                         *
#*       _ __  __ _| |_ ___                                                   *
#*      | '  \/ _` |  _/ -_)                                                  *
#*      |_|_|_\__,_|\__\___|                                                  *
#*                                                                            *
#* Copyright (c) 2018, Andreas Schwenk, contact@arts-and-sciences.com         *
#*                                                                            *
#* GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
#*                                                                            *
#* This library is licensed as described in LICENSE, which you should have    *
#* received as part of this distribution.                                     *
#*                                                                            *
#* This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
#* KIND, either impressed or implied.                                         *
#******************************************************************************/

PREFIX = /usr/local/

PROG = mate
CC = gcc
BUILD_DIR = build/

INSTALL_LIB_DIR = $(PREFIX)lib/
INSTALL_HEADERS_DIR = $(PREFIX)include/

DIR_SRC = src/
DIR_TST = tests/

INC =
LIB = -lm
#HEADERS  = $(DIR_SRC)ann.h
HEADERS =
HEADERS += $(DIR_SRC)ca.h $(DIR_SRC)cluster.h $(DIR_SRC)common.h $(DIR_SRC)graph.h $(DIR_SRC)hash.h $(DIR_SRC)la.h $(DIR_SRC)list.h $(DIR_SRC)parse.h $(DIR_SRC)stat.h $(DIR_SRC)string.h
#HEADERS += $(DIR_TST)ann_tests.h
HEADERS += $(DIR_TST)ca_tests.h $(DIR_TST)cluster_tests.h $(DIR_TST)common_tests.h $(DIR_TST)graph_tests.h $(DIR_TST)hash_tests.h $(DIR_TST)la_tests.h $(DIR_TST)list_tests.h $(DIR_TST)parse_tests.h $(DIR_TST)stat_tests.h $(DIR_TST)string_tests.h

#CFLAGS = -std=gnu99 -O3
CFLAGS = -std=gnu99 -g3   # TODO
#CFLAGS += -fPIC         # PIC := Position Independent Code (only for shared libraries)

LIB_OBJS =
#LIB_OBJS = ann.o
LIB_OBJS += $(BUILD_DIR)ca.o $(BUILD_DIR)cluster.o $(BUILD_DIR)common.o $(BUILD_DIR)graph.o $(BUILD_DIR)graph_io.o $(BUILD_DIR)hash.o $(BUILD_DIR)la.o $(BUILD_DIR)list.o $(BUILD_DIR)parse.o $(BUILD_DIR)stat.o $(BUILD_DIR)string.o
#TEST_OBJS += ann_tests.o
TEST_OBJS += $(BUILD_DIR)ca_tests.o $(BUILD_DIR)cluster_tests.o $(BUILD_DIR)common_tests.o $(BUILD_DIR)graph_tests.o $(BUILD_DIR)hash_tests.o $(BUILD_DIR)la_tests.o $(BUILD_DIR)list_tests.o $(BUILD_DIR)parse_tests.o $(BUILD_DIR)stat_tests.o $(BUILD_DIR)string_tests.o $(BUILD_DIR)tests.o

OBJS = $(LIB_OBJS) $(TEST_OBJS)


$(shell mkdir -p $(BUILD_DIR))

$(PROG): $(OBJS)
	#make clean
	$(CC) $(INC) $(LIB) $(OBJS) -o $(BUILD_DIR)$(PROG)



#ann.o: $(DIR_SRC)ann.c $(HEADERS)
#	$(CC) $(INC) $(CFLAGS) -c $(DIR_SRC)ann.c -o ann.o

$(BUILD_DIR)ca.o: $(DIR_SRC)ca.c $(HEADERS)
	$(CC) -static $(DIR_SRC)ca.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)ca.o

$(BUILD_DIR)cluster.o: $(DIR_SRC)cluster.c $(HEADERS)
	$(CC) -static $(DIR_SRC)cluster.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)cluster.o

$(BUILD_DIR)common.o: $(DIR_SRC)common.c $(HEADERS)
	$(CC) -static $(DIR_SRC)common.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)common.o

$(BUILD_DIR)graph.o: $(DIR_SRC)graph.c $(HEADERS)
	$(CC) -static $(DIR_SRC)graph.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)graph.o

$(BUILD_DIR)graph_io.o: $(DIR_SRC)graph_io.c $(HEADERS)
	$(CC) -static $(DIR_SRC)graph_io.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)graph_io.o

$(BUILD_DIR)hash.o: $(DIR_SRC)hash.c $(HEADERS)
	$(CC) -static $(DIR_SRC)hash.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)hash.o

$(BUILD_DIR)la.o: $(DIR_SRC)la.c $(HEADERS)
	$(CC) -static $(DIR_SRC)la.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)la.o

$(BUILD_DIR)list.o: $(DIR_SRC)list.c $(HEADERS)
	$(CC) -static $(DIR_SRC)list.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)list.o

$(BUILD_DIR)parse.o: $(DIR_SRC)parse.c $(HEADERS)
	$(CC) -static $(DIR_SRC)parse.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)parse.o

$(BUILD_DIR)stat.o: $(DIR_SRC)stat.c $(HEADERS)
	$(CC) -static $(DIR_SRC)stat.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)stat.o

$(BUILD_DIR)string.o: $(DIR_SRC)string.c $(HEADERS)
	$(CC) -static $(DIR_SRC)string.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)string.o

#ann_tests.o: $(DIR_TST)ann_tests.c $(HEADERS)
#		$(CC) -static $(DIR_TST)ann_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)ann_tests.o

$(BUILD_DIR)ca_tests.o: $(DIR_TST)ca_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)ca_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)ca_tests.o

$(BUILD_DIR)cluster_tests.o: $(DIR_TST)cluster_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)cluster_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)cluster_tests.o

$(BUILD_DIR)common_tests.o: $(DIR_TST)common_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)common_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)common_tests.o

$(BUILD_DIR)graph_tests.o: $(DIR_TST)graph_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)graph_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)graph_tests.o

$(BUILD_DIR)hash_tests.o: $(DIR_TST)hash_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)hash_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)hash_tests.o

$(BUILD_DIR)la_tests.o: $(DIR_TST)la_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)la_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)la_tests.o

$(BUILD_DIR)list_tests.o: $(DIR_TST)list_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)list_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)list_tests.o

$(BUILD_DIR)parse_tests.o: $(DIR_TST)parse_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)parse_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)parse_tests.o

$(BUILD_DIR)stat_tests.o: $(DIR_TST)stat_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)stat_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)stat_tests.o

$(BUILD_DIR)string_tests.o: $(DIR_TST)string_tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)string_tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)string_tests.o

$(BUILD_DIR)tests.o: $(DIR_TST)tests.c $(HEADERS)
	$(CC) -static $(DIR_TST)tests.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)tests.o

clean:
	rm -f $(PROG) $(OBJS)

.PHONY: install
install: $(PROG)
	ar rcs $(BUILD_DIR)lib$(PROG).a $(LIB_OBJS)
	mkdir -p $(INSTALL_HEADERS_DIR)$(PROG)/
	cp $(HEADERS) $(INSTALL_HEADERS_DIR)$(PROG)/
	cp $(BUILD_DIR)lib$(PROG).a $(INSTALL_LIB_DIR)lib$(PROG).a

.PHONY: uninstall
uninstall:
	rm -r $(INSTALL_HEADERS_DIR)$(PROG)/
	rm $(INSTALL_LIB_DIR)lib$(PROG).a
